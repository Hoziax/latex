Für den Strom $I$ durch einen Kondensator mit der Kapazität $C$ gilt im Allgemeinen
$$ i(t) = C \cdot \frac{\mathrm{d}u(t)}{\mathrm{d}t} $$
Der Strom entspricht also der Spannungsänderung pro Zeit. Folglich lässt sich der Strom
bei einer sinusförmigen Wechselspannung
$$u(t)=\hat{U}\cdot\sin(\omega t)$$
als
\begin{align*}
    i(t) &= C \cdot \frac{\mathrm{d}}{\mathrm{d}t}u(t)\\
    &=C\cdot\hat{U}\frac{\mathrm{d}}{\mathrm{d}t}\sin(\omega t)\\
    &=C\cdot\hat{U}\cdot\omega\cdot\cos(\omega t)\\
    &=C\cdot\hat{U}\cdot\omega\cdot\sin(\omega t+\frac{\pi}{2})
\end{align*}

beschreiben.\\

Daraus ergibt sich eine Phasenverschiebung $\varphi$ zwischen $u(t)$ und $i(t)$ von
$\varphi=\frac{\pi}{2}$. Man sagt auch, der Strom sei um $90^{\circ}$ voreilend zur Spannung.

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        xtick={0,pi/4,pi/2,3*pi/4,pi,5*pi/4,3*pi/2,7*pi/4,2*pi},
                        ytick={-1,0,1},
                        xticklabels={0,$\frac{\pi}{4}$,$\frac{\pi}{2}$,$\frac{3\pi}{4}$,$\pi$,$\frac{5\pi}{4}$,$\frac{3\pi}{2}$,$\frac{7\pi}{4}$,$2\pi$},
                        yticklabels={,,},
                        xlabel={$t[\mathrm{s}]$},
                        ylabel={$u[\mathrm{V}]/i[\mathrm{A}]$},
                        width=10cm,
                        height=8cm,
                        xmin=0,
                        ymin=-1.5,
                        xmax=7,
                        ymax=1.5
                        ]
            
            \addplot[domain=0:6.28,blue] {sin(x*57.32)};
            \addplot[domain=0:6.28,red] {cos(x*57.32)*0.75};
        \end{axis}

        \foreach \x/\y in {0/$0$,45/$\frac{\pi}{4}$,90/$\frac{\pi}{2}$,135/$\frac{3\pi}{4}$,180/$\pi$,225/$\frac{5\pi}{4}$,270/$\frac{3\pi}{2}$,315/$\frac{7\pi}{4}$}{
            \draw[black!50,rotate around={\x:(-3,3.21)}] (-0.76,3.21)--(-0.96,3.21);
            \node at ({-3+cos(\x)*2.64},{3.21+sin(\x)*2.64}) {\y};
        }

        \draw[black!50] (-3,3.21) circle (2.14cm);
        \fill (-3,3.21) circle (0.5mm);
        \fill[blue,rotate around={45:(-3,3.21)}] (-0.86,3.21) circle (0.5mm);
        \fill[blue] (0.94,4.72) circle (0.5mm);
        \fill[red] (0.94,4.34) circle (0.5mm);
        \draw[-Latex,blue,rotate around={45:(-3,3.21)}] (-3,3.21)--(-0.86,3.21);
        \draw[-Latex,red,rotate around={135:(-3,3.21)}] (-3,3.21)--(-1.4,3.21);
        \fill[red,rotate around={135:(-3,3.21)}] (-1.4,3.21) circle (0.5mm);
        \draw[dashed,blue] (-1.48,4.72)--(0.94,4.72);
        \draw[dashed,red] (-4.1,4.34)--(0.94,4.34);
        \node[blue] at (1.4,4.7) {$u(t)$};
        \node[red] at (1.4,4.3) {$i(t)$};
        \draw[dotted] (-3,5.35)--(1.9,5.35);
        \draw[dotted] (-3,1.07)--(5.7,1.07);
        \draw[dotted] (-3,3.21)--(3,3.21);

        \node at (-2.4,3.45) {$t$};
        \node[blue] at (-2.4,4.1) {$\vec{u}$};
        \node[red] at (-3.8,3.6) {$\vec{i}$};
        \draw[-Latex] (-2,3.21) arc (0:45:10mm);

        \draw[rotate around={45:(-3,3.21)}] (-2.2,3.21) arc (0:90:8mm);
        \node at (-3,3.6) {$\varphi$};

    \end{tikzpicture}
\end{center}

Im Zeigerdiagramm muss man sich weiterhin vorstellen, dass die beiden Zeiger
(mit konstantem Winkel zwischeneinander) mit der Zeit gegen den Uhrzeigersinn 
rotieren und so die Spannungs- und Stromkurven \glqq zeichnen\grqq{}.\\

Der Wechselstromwiderstand $X_C$ eines Kondensator berechnet sich aus 
$$X_C=\frac{1}{\omega C}$$