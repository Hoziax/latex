Der Zusammenhang zwischen Strom $I$ und Spannung $U$ eines ohmschen Widerstandes $R$ ist gegeben durch das ohmsche Gesetz.
$$ U=R \cdot I $$

Es ist ersichtlich, dass gilt $ U \propto I $. Folglich sind Strom und Spannung in Phase ($\varphi = 0$). 
Die Zeiger $\vec{u}$ und $\vec{i}$ liegen aufeinander.

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[   samples=100,
                        inner axis line style={thick},
                        axis lines=center,
                        axis line style={-Latex},
                        xtick={0,pi/4,pi/2,3*pi/4,pi,5*pi/4,3*pi/2,7*pi/4,2*pi},
                        ytick={-1,0,1},
                        xticklabels={0,$\frac{\pi}{4}$,$\frac{\pi}{2}$,$\frac{3\pi}{4}$,$\pi$,$\frac{5\pi}{4}$,$\frac{3\pi}{2}$,$\frac{7\pi}{4}$,$2\pi$},
                        yticklabels={,,},
                        xlabel={$t[\mathrm{s}]$},
                        ylabel={$u[\mathrm{V}]/i[\mathrm{A}]$},
                        width=10cm,
                        height=8cm,
                        xmin=0,
                        ymin=-1.5,
                        xmax=7,
                        ymax=1.5
                        ]
            
            \addplot[domain=0:6.28,blue] {sin(x*57.32)};
            \addplot[domain=0:6.28,red] {sin(x*57.32)*0.75};
        \end{axis}

        \foreach \x/\y in {0/$0$,45/$\frac{\pi}{4}$,90/$\frac{\pi}{2}$,135/$\frac{3\pi}{4}$,180/$\pi$,225/$\frac{5\pi}{4}$,270/$\frac{3\pi}{2}$,315/$\frac{7\pi}{4}$}{
            \draw[black!50,rotate around={\x:(-3,3.21)}] (-0.76,3.21)--(-0.96,3.21);
            \node at ({-3+cos(\x)*2.64},{3.21+sin(\x)*2.64}) {\y};
        }

        \draw[black!50] (-3,3.21) circle (2.14cm);
        \fill (-3,3.21) circle (0.5mm);
        \fill[blue,rotate around={45:(-3,3.21)}] (-0.86,3.21) circle (0.5mm);
        \fill[blue] (0.94,4.72) circle (0.5mm);
        \fill[red] (0.94,4.34) circle (0.5mm);
        \draw[-Latex,blue,thick,rotate around={45:(-3,3.21)}] (-3,3.21)--(-0.86,3.21);
        \draw[-Latex,red,rotate around={45:(-3,3.21)}] (-3,3.21)--(-1.4,3.21);
        \fill[red,rotate around={45:(-3,3.21)}] (-1.4,3.21) circle (0.5mm);
        \draw[dashed,blue] (-1.48,4.72)--(0.94,4.72);
        \draw[dashed,red] (-1.87,4.34)--(0.94,4.34);
        \node[blue] at (1.4,4.7) {$u(t)$};
        \node[red] at (1.4,4.3) {$i(t)$};
        \draw[dotted] (-3,5.35)--(1.9,5.35);
        \draw[dotted] (-3,1.07)--(5.7,1.07);
        \draw[dotted] (-3,3.21)--(3,3.21);

        \node at (-2.4,3.45) {$t$};
        \node[blue] at (-1.9,4.6) {$\vec{u}$};
        \node[red] at (-2.5,4) {$\vec{i}$};
        \draw[-Latex] (-2,3.21) arc (0:45:10mm);

    \end{tikzpicture}
\end{center}

Möchte man nun Parallel- oder Serienschaltungen von ohmschen Widerständen betrachten, so ist es nachvollziehbar, dass
sich die Phasenlage zwischen den einzelnen Strömen und Spannungen nicht ändern kann. Es kann also nach wie vor nur mit 
den Beträgen von $\vec{u}$ und $\vec{i}$ gerechnet werden, wie das auch in den bekannten Formeln zur Reihen- und Parallelschaltungen
der Fall ist.

\paragraph{Beispiel.}
    \begin{center}
        \begin{tikzpicture}
            \draw (-0.5,0.25) -- (0,0.25);
            \draw (0,0) rectangle (1.5,0.5);
            \draw (1.5,0.25) -- (2.5,0.25);
            \draw (2.5,0) rectangle (4,0.5);
            \draw (4,0.25) -- (4.5,0.25);
            \node at (0.75,0.75) {$R_1=1 \;\mathrm{k\Omega}$};
            \node at (3.25,0.75) {$R_2=2\;\mathrm{k\Omega}$};
            \draw[red,-Latex,thick] (-0.1,0.25) -- (-0.09,0.25);
            \draw[blue,-Latex,thick] (0,-0.25) -- (1.5,-0.25);
            \draw[blue,-Latex,thick] (2.5,-0.25) -- (4,-0.25);
            \node[blue] at (0.75,-0.75) {$U_1=1\;\mathrm{V}$};
            \node[blue] at (3.25,-0.75) {$U_2=2\;\mathrm{V}$};
            \node[red] at (-1,0.6) {$I=1\;\mathrm{mA}$};
        \end{tikzpicture}
    \end{center}

    Die Betrachtung der skalaren Grössen unter Berücksichtigung der Regeln der Parallelschaltung 
    \begin{align*}
        U_{\mathrm{ges}}&=U_1+U_2\\
        R_{\mathrm{ges}}&=R_1+R_2\\
        I_1&=I_2
    \end{align*}

    entspricht der Betrachtung der vektoriellen \glqq Zeigergrössen\grqq{}, da $\varphi=0$:

    \begin{equation*}
        \vec{u}_{\mathrm{ges}}=\vec{u}_1+\vec{u}_2
    \end{equation*}

    \begin{center}
        \begin{tikzpicture}
            \draw[black!50] (0,0) circle (1cm);
            \foreach \x in {0,45,...,315}{
                \draw[black!50,rotate=\x] (0.9,0)--(1.1,0);
            }
            \fill[black] (0,0) circle (0.5mm);
            \fill[black] (1,0) circle (0.5mm);
            \draw[-Latex,blue] (0,0) -- (1,0);
            \draw[-Latex,blue] (1,0) -- (3,0);

            \node[blue] at (0.5,0.25) {$\vec{u}_1$};
            \node[blue] at (1.75,0.25) {$\vec{u}_2$};
            
        \end{tikzpicture}
    \end{center}




