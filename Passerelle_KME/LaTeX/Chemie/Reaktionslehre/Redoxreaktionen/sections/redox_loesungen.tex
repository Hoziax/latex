\section{Redoxvorgänge in wässrigen Lösungen}
\subsection{Reaktionen von Salzlösungen mit Metallen}
Metalle sind Reduktionsmittel, ihre Atome bilden durch Abgabe von Valenzelektronen Kationen, die in wässrigen Lösungen hydratisiert werden.\\
\textbf{Unedle Metalle} sind \textbf{starke Reduktionsmittel}, sie lassen sich leicht oxidieren und in Lösung bringen. Ihre Atome geben leicht Elektronen ab und die Ionen werden hydratisiert.\\ 
\begin{center}
    \ch{Na -> Na+ (aq) + e-}
\end{center}
\textbf{Edle Metalle} sind \textbf{schwache Reduktionsmittel}. Ihre Atome geben die Valenzelektronen nur schwer ab. Entsprechend leicht nehmen ihre Ionen Elektronen auf, haben also eine stark oxidierende Wirkung. Edelmetalle lassen sich leicht aus Lösungen abscheiden (reduzieren).\\
\begin{center}
    \ch{Ag+ (aq) + e- -> Ag}
\end{center}
Ein Metall kann seine Elektronen auf die Ionen eines anderen Metalls übertragen, wenn dieses ein schwächeres Reduktionsmittel (edleres Metall) ist. Das unedlere Metall verdrängt das edlere aus Lösungen seiner Salze.\\
\begin{center}
    \ch{Zn (s) + 2 Ag+ (aq) -> 2 Ag (s) + Zn^{2+}}
\end{center}

\paragraph*{Aufstellen einer Redoxgleichung} anhand eines Zinkblechs in \textit{Kupfer(\rom{2})-chlorid}-Lösung. \\
Die \textit{Kupfer(\rom{2})-chlorid}-Lösung besitzt eine für Kupfer(\rom{2})-salze typische Lösung. Wird das Zinkblech in die Lösung getaucht, verliert sie langsam ihre bläuliche Farbe und auf dem Zinkblech bildet sich ein dunkler Belag, der immer dicker wird und sich irgendwann ablöst.\\
Ausgangsgleichung: \qquad \ch{Zn + \color{darkblue}{Cu^{2+} (aq)} + 2 Cl- (aq) -> \color{darkblue}{Cu} + ? + ?}\\
Die Abscheidung von Kupfer (dunkler Belag) und das Verschwinden der Farbe der Lösung zeigen, dass Kupfer(\rom{2})-IOnen zu Kupfer-Atomen reduziert wurden. Die dazu erforderlichen Elektronen liefert das Zink. Zink-Atome wurden zu Zink-Ionen oxidiert.
\begin{center}
    \begin{tikzpicture}
        \node(zn1) at (1,1) {\ch{Zn}};
        \node[right = 0.5cm of zn1] (cu1) {\ch{\color{darkblue}{Cu^{2+} (aq)}}};
        \node[right = 2.7cm of zn1] (cl1) {\ch{2 Cl- (aq)}};
        \node[right = 4.2cm of zn1] (arrow) {\charrow{->}};
        \node[right = 5.2cm of zn1] (cu2) {\ch{\color{darkblue}{Cu}}};
        \node[right = 6.5cm of zn1] (zn2) {\ch{Zn^{2+} (aq)}};
        \node[right = 8.7cm of zn1] (cl2) {\ch{2 Cl- (aq)}};

        \node at ($(zn1)!0.35!(cu1)$) {$+$};
        \node at ($(cu1)!0.5!(cl1)$) {$+$};
        \node at ($(cu2)!0.35!(zn2)$) {$+$};
        \node at ($(zn2)!0.5!(cl2)$) {$+$};

        \node[below = 0.08cm of zn1] (rm) {{\scriptsize RM}};
        \coordinate[below = 0.1cm of rm] (belowRm);
        \node[below = 0cm of cu1, text = darkblue] (om) {{\scriptsize OM}};

        \coordinate[above = 0.38cm of zn1] (aboveZn1);
        \coordinate[above = 0.3cm of cu1] (aboveCu1);

        \draw[->, >=stealth, darkblue] (zn1) -- (aboveZn1) -- (aboveCu1) -- (cu1);
        \path (aboveZn1) -- (aboveCu1) node [midway, fill = white] {\color{darkblue}{2} \ch{\color{darkblue}{e-}}};

        \draw[->, >=stealth, dashed] (rm) -- (belowRm) -| (zn2);
        \node[right = 3cm of belowRm, fill = white] {{\scriptsize wird oxidiert}};

        \draw[->, >=stealth, dashed, darkblue] (om) -| (cu2);
        \node[right = 0.95cm of om, fill = white, text = darkblue] {{\scriptsize wird reduziert}};
    \end{tikzpicture}
\end{center}
Nun kann man die Oxidation und Reduktion einzeln aufschreiben und daraus die Redox-Gleichung ableiten.
\begin{center}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{1.8cm} p{7.5cm} p{2cm}}
        Oxidation: & \ch{Zn \qquad \qquad \qquad \quad -> Zn^{2+} + 2 e-} & \\
        \hline
        Reduktion: & \ch{~~~~~~~~ \color{darkblue}{Cu^{2+}} + 2 \color{darkblue}{e-} -> \color{darkblue}{Cu}} & \\
        \hline
        Redox: & \ch{Zn + \color{darkblue}{Cu^{2+}} + 2 \color{darkblue}{e-} -> Zn^{2+} + 2 e- + \color{darkblue}{Cu}} & \ch{2 e-} kürzen\\
        Redox: & \ch{Zn + \color{darkblue}{Cu^{2+}} \qquad \quad ~ -> Zn^{2+} + \qquad \quad \color{darkblue}{Cu}} & \\
    \end{tabular}
\end{center}

\subsection{Korrespondierende Redoxpaare}
Die reduzierte und die oxidierte Form eines Metalls bilden ein korrespondierendes Redoxpaar \ch{Me}/\ch{Me^{n+}}. Ein Redoxpaar wird nach dem Schema \textit{Reduktionsmittel}/\textit{Oxidationsmittel} geschrieben. Das $n$ gibt den Unterschied in der Elektronenzahl an.\\
Das Metall-Atom (reduzierte Form) ist der Elektronendonator (Reduktionsmittel) und wird durch Elektronenabgabe zum korrespondierenden Oxidationsmittel (Metall-Ion $=$ oxidierte Form).
Das \textbf{Oxidationsmittel} ist umso \textbf{schwächer}, je \textbf{stärker} das \textbf{korrespondierende Reduktionsmittel} ist.

\subsection{Die Redoxreihe}
\begin{itemize}
    \item In der Redoxreihe sind die Redoxpaare so angeordnet, dass das \textbf{stärkste Reduktionsmittel links oben} und das \textbf{stärkste Oxidationsmittel rechts unten} stehen.
    \item Ein \textbf{Reduktionsmittel} kann \textbf{Elektronen} an die \textbf{Oxidationsmittel abgeben}, die in der Redoxreihe weiter \textbf{unten} stehen.
    \item \textbf{Metalle} stehen als Reduktionsmittel (Elektronendonatoren) in der linken Reihe, ihre \textbf{reduzierende Wirkung nimmt nach unten ab}. \textbf{Metall-Ionen} stehen als Elektronenakzeptoren rechts, ihre \textbf{oxidierende Wirkung nimmt nach unten zu}.
    \item \textbf{Nichtmetall-Atome} stehen in der rechten Reihe (Elektronenakzeptoren). Ihre \textbf{oxidierende Wirkung nimmt nach unten zu}. \textbf{Nichtmetall-Anionen} stehen in der linken Reihe (Elektronendonatoren). Ihre \textbf{reduzierende Wirkung} ist eher \textbf{schwach} und \textbf{nimmt nach unten ab}.
\end{itemize}

\renewcommand{\arraystretch}{1.5}
\begin{tabular}{p{6cm} p{1cm} p{1cm} p{6cm}} 
    \multicolumn{4}{c}{\textbf{Metalle}} \\
    \hline
    Metalle, die leicht Elektronen abgeben, stehen in der Redoxreihe oben. Man bezeichnet sie als unedel, weil sie leicht oxidierbar sind. & & & Die Ionen unedler Metalle haben eine geringe Neigung, Elektronen aufzunehmen.\par Sie sind schwache Oxidationsmittel.\\
    Metalle, deren Atome nur schwer Elektronen abgeben, stehen in der Redoxreihe weit unten. Man bezeichnet sie als edel. Sie sind schwache Reduktionsmittel. & \multirow{-2}{*}{\rotatebox[origin=c]{90}{\parbox{2.3cm}{$\overrightarrow{\text{reduzierende Wirkung}}$}}} & \multirow{-2}{*}{\rotatebox[origin=c]{90}{\parbox{2.2cm}{$\overleftarrow{\text{oxidierende Wirkung}}$}}} & Die Ionen edler Metalle nehmen leicht Elektronen auf. Sie sind gute Oxidationsmittel.
\end{tabular}

\begin{center}
    \begin{tikzpicture}
        \matrix[column sep=0.5cm, row sep=0.5cm]  (m) {
            \node[text width = 6cm] {};
            & \node (rm1) {RM};
            & \node (om1) {OM};
            & \node[text width = 6cm] {{\scriptsize Ein OM kann von einem unterhalb stehenden RM keine Elektronen aufnehemen}};\\

            \node[text width = 6cm] {{\scriptsize Ein RM kann (nur) an die unterhalb stehenden OM Elektronen abgeben.}};
            & \node (rm2) {RM};
            & \node (om2) {OM};
            & \node[text width = 6cm] {}; \\

            \node[text width = 6cm] {};
            & \node (rm3) {RM};
            & \node (om3) {OM};
            & \node[text width = 6cm] {{\scriptsize Ein OM kann von einem oberhalb stehenden RM Elektronen aufnehemen}};\\
        };

        \draw[->, >= stealth, dashed, darkblue] (om1) -- (rm1);
        \path (om1) -- (rm1) node [midway, fill = white, text = darkblue] {X};

        
        \draw[->, >= stealth, dashed, darkblue] (rm2) -- (om1);
        \path (rm2) -- (om1) node [midway, fill = white, text = darkblue] {X};
        \draw[->, >= stealth, darkblue] (rm2) -- (om2);
        \draw[->, >= stealth, darkblue] (rm2) -- (om3);
        \path (rm2) -- (om3) node [midway, fill = white, text = darkblue] {\ch{e-}};

        \draw[->, >= stealth, darkblue] (om3) -- (rm3);
    \end{tikzpicture}
\end{center}

Entscheidend für die Reduktionswirkung ist neben einer möglichst \textbf{kleinen Ionisierungsenergie} auch eine möglichst \textbf{grosse Hydrationsenergie}.

\subsection{Reaktion von Salzlösungen mit Nichtmetallen}
\subsubsection*{Nichtmetalle als Oxidationsmittel}
\textbf{Nichtmetall-Atome nehmen} relativ \textbf{leicht Elektronen auf}. Sie sind Oxidationsmittel. Da aber die meisten Nichtmetalle aus Molekülen bestehen, müssen die Bindungen bei der Reaktion getrennt werden.\\
Die Nichtmetall-Ionen können Elektronen abgeben, die Reaktion ist umkehrbar.\\
\begin{center}
    \ch{!(OM)( X2 ) + 2 e- <> !(RM)( 2 X- )}
\end{center}

\subsubsection*{Oxidation von Nichtmetall-Ionen durch Nichtmetalle}
Ein \textbf{Nichtmetall} kann ein \textbf{Ion oxidieren}, wenn es die \textbf{grössere Elektronegativität} besitzt, also das stärkere Oxidationsmittel ist.

\paragraph*{Beispiel Iod-Bildung}
Leitet man Chlorgas in eine Kaliumiodid-Lösung, bildet sich Iod.

\begin{center}
    \begin{tikzpicture}
        \node(k1) at (1,1) {\ch{2 K+}};
        \node[above right = -0.55cm and 0.7cm of k1] (i1) {\ch{2 I-}};
        \node[above right = -0.6 and 2.2cm of k1] (cl1) {\ch{Cl2}};
        \node[right = 2.8cm of k1] (arrow) {\charrow{->}};
        \node[right = 3.8cm of k1] (k2) {\ch{2 K+}};
        \node[right = 5.4cm of k1] (cl2) {\color{darkblue}{2} \ch{\color{darkblue}{Cl^{-}}}};
        \node[above right = -0.6cm and 7.1cm of k1] (i2) {\ch{I2}};

        \node at ($(k1)!0.5!(i1)$) {$+$};
        \node at ($(i1)!0.5!(cl1)$) {$+$};
        \node at ($(k2)!0.5!(cl2)$) {$+$};
        \node at ($(cl2)!0.5!(i2)$) {$+$};

        \node[below = 0.08cm of i1] (rm) {{\scriptsize RM}};
        \coordinate[below = 0.1cm of rm] (belowRm);
        \node[below = 0cm of cl1, text = darkblue] (om) {{\scriptsize OM}};

        \coordinate[above = 0.32cm of i1] (aboveI1);
        \coordinate[above = 0.3cm of cl1] (aboveCl1);

        \draw[->, >=stealth, darkblue] (i1) -- (aboveI1) -- (aboveCl1) -- (cl1);
        \path (aboveI1) -- (aboveCl1) node [midway, fill = white] {\color{darkblue}{2} \ch{\color{darkblue}{e-}}};

        \draw[->, >=stealth, dashed] (rm) -- (belowRm) -| (i2);
        \node[right = 2.4cm of belowRm, fill = white] {{\scriptsize wird oxidiert}};

        \draw[->, >=stealth, dashed, darkblue] (om) -| (cl2);
        \node[right = 0.5cm of om, fill = white, text = darkblue] {{\scriptsize wird reduziert}};
    \end{tikzpicture}
\end{center}

Die Iodid-Ionen werden durch Chlor oxidiert. Das \ch{Cl2}-Molekül wird gespalten und jedes \ch{Cl}-Atom nimmt ein \ch{I-}-Ion auf. Es entstehen Chlorid-Ionen und Iod-Atome bzw. Iod-Moleküle.\\
Die Iodid-Ionen sind das stärkere Reduktionsmittel, sie geben je ein Elektron an die beiden Chlor-Atome eines \ch{Cl2}-Moleküls ab. Weil Chlor eine grössere Elektronegativität besitzt, kann es einem Iodid-Ion ein Elektron abnehmen.

\subsection{Reaktionen von Metallen mit Säuren}
\hbox{
    \hspace{-1cm}
\begin{tabular} {p{3cm} | p{6cm} | p{6cm}}
    & \textbf{Unedle Metalle} & \textbf{Edle Metalle} \\
    Reduktionswirkung & stark & schwach \\
    Elektronenabgabe, Ionenbildung & Atome können relativ \textbf{leicht} Elektronen abgeben und als Ionen in Lösung gehen. & Atome können nur \textbf{schwer} Elektronen abgeben und als Ionen in Lösung gehen.\\
    Oxidierbarkeit, Reaktionsfreudigkeit & Sind \textbf{leicht oxidierbar}, reagieren leicht und heftig mit fast allen Nichtmetallen. & Sind \textbf{schwer oxidierbar}, reagieren nur mit reaktionsfreudigen Nichtmetallen.\\
    Oxidation an der Luft & Laufen rasch an. & Laufen nicht oder sehr langsam an.\\
    Reaktion mit Säuren & Lösen sich unter Wasserstoffbildung auf, übertragen ihr(e) Valenzelektronen auf \ch{H+}-Ionen, & Reagieren nicht mit \glqq gewöhnlichen\grqq{} Säuren. Sie können ihre Valenzelektronen nicht auf \ch{H+}-Ionen übertragen, reagieren aber mit \glqq oxidierenden\grqq{} Säuren.\\
    Redoxreihe & \textbf{Oberhalb} des Paars \textbf{\ch{H2}/\ch{2 H+}}. & \textbf{Unterhalb} des Paars \textbf{\ch{H2}/\ch{2 H+}}.\\
    Kationen & Ionen haben eine geringe Tendenz Elektronen aufzunehmen und sind deswegen \textbf{schwer reduzierbar}. & Ionen nehmen leicht Elektronen auf und sind deswegen \textbf{leicht reduzierbar}.\\
    Vorkommen & Überwiegend in oxidierter Form. & Auch Elementar.\\
\end{tabular}
}
\vspace{0.5cm}
Unedle Metalle reagieren mit Säuren unter der Bildung von Wasserstoff zu Salzen:
\begin{center}
    \ch{Me + 2 HA -> MeA2 + H2}
\end{center}
Bei der Reaktion geben die Metall-Atome ihre Valenzelektronen an die hydratisierten Wasserstoff-Ionen der sauren Lösung ab. In der Reaktionsgleichung kann man der einfachheitshalber \ch{H+ (aq)} verwenden, man darf aber nicht vergessen, dass das \ch{H+}-Ion an ein \ch{H2O}-Molekül zu Oxonium (\ch{H3O+}) gebunden ist.\\
Edel- und Halbmetalle stehen in der Redoxreihe unterhalb von Wasserstoff und reagieren nicht mit \glqq gewöhnlichen\grqq{} Säuren, weil sie nicht durch \ch{H+}-Ionen oxidiert werden. Einige reagieren jedoch mit oxidierenden Säuren. Diese sind stärkere Oxidationsmittel als die \ch{H+}-Ionen, weswegen nie Wasserstoff dabei entsteht.