\section{Weltbevölkerung gestern, heute und morgen}
\subsection{Bevölkerungsverteilung}
Bevölkerungszahlen ohne Angaben zur räumlichen Verteilung sagen wenig aus. Deshalb wird die Bevölkerungsdichte, das Verhältnis zwischen Bevölkerung und Fläche, zu verschiedenen Bezugsflächen gebildet. Die arithmetische Dichte bezieht sich auf die Gesamtfläche; die physiologische Dichte auf die landwirtschaftlich produktive Fläche einer Region.\\
Die vier am dichtesten besiedelten Ballungsräume der Erde liegen in Süd- und Ostasien, in Westeuropa und im Nordosten der USA.
\begin{center}
    \begin{equation*}
        \text{arithmetische Dichte:} \qquad \frac{\text{Einwohnerzahl}}{\text{Fläche (in } km^2 \text{)}}
    \end{equation*}
    \begin{equation*}
        \text{physiologische Dichte:} \qquad \frac{\text{Einwohnerzahl}}{\text{landwirtschaftlich produktive Fläche (in } km^2 \text{)}}
    \end{equation*}    
\end{center}

\begin{flushleft}
    \renewcommand{\arraystretch}{1.5}
    \begin{tabular} {p{2.6cm} p{11.4cm}}
        \textbf{Entwicklung eines Landes} 
        & Der Human Development Report der UN definiert die Entwicklung als: \emph{\glqq die Erweiterung der Möglichkeiten der Menschen, ein ihnen lebenswert erscheinendes Dasein zu führen\grqq{}}. Auch wenn Kerngrössen wie BIP und Pro-Kopf-Einkommen hilfreich sein können, geben sie noch keinen Rückschluss auf die Entwicklung eines Landes.\par
        Auch wenn es keine eindeutige Definition der für Entwicklungsfaktoren gibt, treffen folgende Merkmale auf die Mehrzahl der Entwicklungsländer zu:
        \begin{itemize}
            \item Tiefes Pro-Kopf-Einkommen
            \item Hoher Beschäftigungsanteil im primären Sektor, kaum Beschäftigung in der restlichen Sektoren
            \item Hohe Arbeitslosenrate
            \item Kapitalmangel
            \item Hohes Bevölkerungswachstum
            \item Mangelhafte medizinische Versorgung
            \item Schwaches Bildungswesen und hohe Analphabetenquote
        \end{itemize}\\
        
        \textbf{Human Development Index} 
        & Der HDI ist ein Mass für die Entwicklung eines Landes. Er berücksichtigt die Lebenserwartung bei Geburt, Bildung und Lebensstandard.\\

        \textbf{Wirtschaftliche Einteilung}
        & Sind die Entwicklungsumstände eines Landes tief, spricht man von einem Entwicklungsland. Gelangt ein Land durch wirtschaftlichen Aufschwung an die Schwelle zu den Industriestaaten, wird es Schwellenland genannt.\\

        \textbf{Industrieländer}
        & Industrieländer stehen am oberen Ende der Wohlstandsskala. Sie zeichnen sich durch hohes Pro-Kopf-Einkommen, hohen Beschäftigungsanteil im zweiten bis vierten Sektor, grosses Verkehrsaufkommen, usw. aus.\\

        \textbf{Dritte Welt} 
        & Ursprünglich bezeichnete der Begriff \emph{Dritte Welt} die blockfreien Staaten im Kalten Krieg. Heutzutage verwendet man diesen Begriff aber nur noch für Entwicklungsländer.
    \end{tabular}
\end{flushleft}

\newpage

\subsection{Bevölkerungsexplosion}
Die Bevölkerungsexplosion in den Industriestaaten im 19. Jahrhundert verlief nach den fünf Phasen des demografischen Übergangs. Der Übergang besteht aus einer Senkung sowohl der Sterbe- als auch der Geburtenziffer von einem hohes auf ein tiefes Niveau. Durch das zeitliche Nachhinken des Geburtenzifferabfalls stieg die Bevölkerung explosionsartig an. Seit ca. 1970 liegt die Geburtenziffer in vielen Industriestaaten sogar unter der Sterbeziffer, was zu einer Bevölkerungsschrumpfung führt.
\begin{figure}[h!]
    \centering
    \includegraphics[width = 13cm]{images/demografischer_uebergang.jpeg}
    \caption{Demografischer Übergang}
    \source[0.3cm]{\emph{Bevölkerung und Raum}, S. 32, Compendio-Verlag}
    \label{fig:Demografischer Übergang}
\end{figure}

Die gesellschaftlichen, wirtschaftlichen und politischen Rahmenbedingungen in den Entwicklungsländern sind grundsätzlich verschieden von den Zuständen in Europa zur Jahrhundertwende. Insbesondere die Bevölkerungsschere\footnote{Auseinanderklaffen der Geburten- und Sterberate} öffnet sich weit stärker als jemals in Europa. Während die durchschnittliche Fertilitätsrate in der EU bei $1.56$ liegt, haben \emph{südeuropäische} Staaten eine \emph{tiefere Geburtenrate}, als nordeuropäische. Die Bevölkerungsentwicklung in den Entwicklungsländern folgt nicht dem klassischen Modell des demografischen Übergangs. Die zukünftige Entwicklung wird bestimmt von der enorm zahlreichen Jugend, die erst noch ins fortpflanzungsfähige Alter kommt. Daher wird die Bevölkerung der Entwicklungsländer träge weiterwachsen, selbst wenn die Wachstumsraten erfolgreich bekämpft werden können.\\
Sowohl die Zuwachsraten als auch die Fertilitätsraten sinken seit den 1980er-Jahren. Szenarios der Fertilitätsentwicklung zeigen, wie sich diese Trendwende auf das Weltbevölkerungswachstum auswirken wird. Bei hohen Kinderzahlen pro Frau steigt die Weltbevölkerung weiterhin bedrohlich an, bei tiefen Werten könnte sie sogar sinken.
\newpage
Aids trifft hauptsächlich die erwerbstätige Altersgruppe und bürdet dem Gesundheitswesen hohe Kosten auf. In den stark betroffenen Gebieten destabilisiert die hohe Sterblichkeitsrate die Gesellschaft und dämpft das Bevölkerungswachstum.

\subsubsection*{Demografische Dividende}
Die demografische Dividende beschreibt den Zustand, wenn der \textbf{Grossteil} der \textbf{Bevölkerung} im \textbf{erwerbsfähigen Alter} ist. Somit ist der Anteil der Erwerbstätigen grösser als der Anteil der Nicht-Erwerbstätigen, welche auf eine Versorgung durch Erwerbstätige angewiesen ist.

\subsection{Bewältigung der Bevölkerungskriese}
Die Familiengruppe ist abhängig vom kulturellen, wirtschaftlichen und politischen Umfeld einer Gesellschaft sowie von der Einsicht und den Verhaltensweisen des Einzelnen. Die Reduktion des Bevölkerungswachstums ist somit keine technische, sondern eine kulturelle Aufgabe. Heute baut man auf zwei Pfeiler zur Verminderung des Bevölkerungswachstums:
\begin{itemize}
    \item \textbf{Empowerment der Frau}: Die Gleichberechtigung und Gleichstellung der Frau innerhalb der Gesellschaft und die Durchsetzung des Menschenrechts auf reproduktive Gesundheit führt zu selbstständigen Frauen, die weniger Kinder wollen.
    \item \textbf{Familienplanung}: Die sexuelle Aufklärung und die Verfügbarkeit von Verhütungsmitteln sind die Eckpfeiler effizienter Familienplanung.
\end{itemize}

\begin{figure}[h!]
    \centering
    \includegraphics[width = 14cm]{images/interventionsmoeglichkeiten_demografic.PNG}
    \caption{Interventionsmöglichkeiten auf dem demografischen Entwicklungspfad}
    \source{Prezi Bevölkerung und Raum; Anita Diener - Berliner Institut für Bevölkerung und Entwicklung}
    \label{fig:demographic_intervention}
\end{figure}