\section{Stadtstrukturen: Modelle und Theorien}
Da unterschiedliche Räume einer Stadt oder Agglomeration mit der Zeit \textbf{unterschiedliche Funktionen} annehmen, spricht man von \textbf{funktionaler Differenzierung}. In der Stadtgeografie gibt es verschiedene Modelle um diese Merkmale zu ordnen und Regelmässigkeiten zu entdecken und ordnen.

\subsection{Kreise, Sektoren und Kerne}
\subsubsection*{Kreis-Modell}
\setlength\intextsep{0pt}
\begin{wrapfigure}[11] {r} {4cm}
    \includegraphics[width = 4cm]{images/modell_kreis.jpg}
    \caption{Kreis-Modell}
\end{wrapfigure}
Das Kreis-Modell geht davon aus, dass sich Zonen verschiedener Merkmale \textbf{ringförmig} anordnen. Die Stadtmerkmale verändern sich vom \textbf{Zentrum} zur \textbf{Peripherie}.\\
Ein Beispiel für die für das Kreis-Modell ist die Anzahl Stockwerke in amerikanischen Städten. Während sich im \textit{CBD}\footnote{Central Business-District} Wolkenkratzer gruppieren nimmt die Anzahl Stockwerke nach aussen hin stetig ab.

\paragraph*{Citybildung und Central Business-District}
Die \textbf{starke Konzentration} von \textbf{Büro- und Verwaltungsgebäuden} im \textbf{Stadtzentrum} wird als Citybildung bezeichnet. Eine Folge der Citybildung ist die Bildung eines Geschäftszentrums in Grossstädten (\textit{CBD}). Der \textit{CBD} zeichnet sich durch hohe Gebäude, grosse Tagesbevölkerung, hohe Verkehrsdichte und hohe Preise aus.

\subsubsection*{Sektoren-Modell}
\begin{wrapfigure}[10] {l} {4cm}
    \includegraphics[width = 4cm]{images/modell_sektoren.jpg}
    \caption{Sektoren-Modell}
\end{wrapfigure}
Das Sektoren-Modell unterteilt eine Stadt in \textbf{kuchenstückartige Sektoren}. Diese Modelle sind häufig im Zusammenhang mit \textbf{Bevölkerungs-} und \textbf{Wirtschaftsstrukturen} anzutreffen.\\
Ethnische Gruppen sind in (nordamerikanischen) Grossstädten oft sektorenartig verteilt. Dies lässt sich mit dem Bedürfnis der Einzelnen unter Menschen mit gleichen Erfahrungen und Hintergrund zu leben erklären. Auch herrschen zwischen den einzelnen Sektoren meist Unterschiede in Einkommen und Lebensbedingungen. Kommt es zu einer wirtschaftlichen Depression können so ganze Quartiere verwahrlosen und zu Slums/Gettos verkommen.

\vspace{2cm}

\subsection*{Mehr-Kerne-Modell}
\begin{wrapfigure}[5] {r} {4cm}
    \includegraphics[width = 4cm]{images/modell_mehrkern.jpg}
    \caption{Mehr-Kerne-Modell}
\end{wrapfigure}    
Das Mehr-Kerne-Modell geht davon aus, dass mit der \textbf{Grösse einer Stadt} auch die \textbf{Anzahl Kerne zunimmt}. Dabei wird nicht nur die Stadtmitte als Kern angesehen, sondern auch periphere Geschäfts- oder Kulturzentren, sowie Parks. Die verschiedenen Kerne zeichnen sich durch unterschiedliche Nutzung aus und variieren in ihrer Grösse und Bedeutung.

\subsection*{Definitionen}
\begin{figure}[h]
    \centering
    \includegraphics[width = 14cm]{images/dynamik_urbane_bevoelkerung.PNG}
    \caption{Urbanisierung, Suburbanisierung und Reurbanisierung}
    \source{\textit{Bevölkerung und Raum}, S. 80, Compendio-Verlag}
    \label{fig:dynamic_cities}
\end{figure}

\subsubsection*{Urbanisierung (Verstädterung)}
Zunahme der Stadtbevölkerung vor allem durch Landbevölkerung. Dieses Phänomen prägte europäische Städte während der Industrialisierung und ist heute hauptsächlich in Entwicklungsländern zu beobachten.

\subsubsection*{Suburbanisierung}
Zuwanderer ziehen nicht mehr in die Kernstadt, sondern siedeln sich in der Peripherie in neu entstandene Wohnquartiere an. Auch Bewohner der Kernstadt und der Arbeiterquartiere zieht es in die Peripherie der Stadt.\\
Durch Suburbanisierung entsteht aus einer Stadt eine grossflächige Agglomeration.
\begin{figure}[h]
    \centering
    \includegraphics[width = 14cm]{images/herausforderungen_suburbanisierung.PNG}
    \caption{Herausforderungen der Suburbanisierung}
    \source{Hasler/Egli (2016): Geographie - wissen und verstehen. Hep. Bern. S. 292.}
    \label{fig:challanges_suburbanisierung}
\end{figure}

\subsubsection*{Reurbanisierung}
Reurbanisierung bezeichnet die gezielte Stadterneuerungsmassnahmen, wie die Wiederbelebung der Innenstädte oder zentraler Stadtviertel.

\subsubsection*{Gentrification}
Gentrification bezeichnet den Austausch einkommensschwacher Bevölkerungsgruppen durch besser Verdienende. Dies geschieht oft durch Sanierung und Modernisierung.\\
Reurbanisierung und Gentrification gehen oft Hand in Hand.

\newpage

\subsection{Daseinsgrundfunktionen}
Daseinsgrundfunktionen sind grundlegende \textbf{Bedürfnisse} und \textbf{Aktivitäten}, welche durch ihre Ausübung den Raum prägen.\\
Einige Daseinsgrundfunktionen sind:
\begin{multicols} {2}
    \begin{itemize}
        \item Wohnen
        \item Arbeiten
        \item Bildung (\glqq Sich-Bilden\grqq{})
        \item Versorgung (\glqq Sich-Versorgen\grqq{})
        \columnbreak
        \item Erholung (\glqq Sich-Erholen\grqq{})
        \item Kommunizieren
        \item Teilnahme am Verkehr (\glqq Am-Verkehr-Teilnehmen\grqq{})
    \end{itemize}
\end{multicols}

\subsection{Vom Funktionenmix zur regionalen Ordnung}
Mit der Industrialisierung wurde das althergebrachte Muster der ineinander verwobenen Funktionen von Wohnen und Arbeiten untragbar. Die Nähe ihrer Wohnorte zu den Fabriken schadete der Gesundheit der Arbeiter. Als Reaktion darauf reagierten Stadtplaner in den 20er und 30er Jahren des 20. Jahrhunderts mit \textbf{funktionaler Entmischung}. Bei funktionaler Entmischung werden die \textbf{Daseinsgrundfunktionen} klar und deutlich voneinander \textbf{getrennt}. Die Entwicklung der individuellen Mobilität begünstigte diese Entwicklung.\\
Die neuen Wohnquartiere in der Peripherie führten zu einem Imageverlust für die Innenstadt als Wohnquartier. Auch fehlt es in den peripheren Wohnquartieren an Einrichtungen für Freizeit und öffentliches Leben. Dadurch degenerieren diese zu Schlafstädten.