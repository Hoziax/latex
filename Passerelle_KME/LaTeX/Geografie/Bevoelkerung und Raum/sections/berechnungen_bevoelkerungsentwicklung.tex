\section{Berechnungen zur Bevölkerungsentwicklung}
\subsection{Messung der Bevölkerungsentwicklung}
Als Teilgebiet der Demografie untersucht die Bevölkerungsgeografie die räumlichen Unterschiede in Dichte und Aufbau der Bevölkerung. Die Gesamtbevölkerung eines Gebiets ist abhängig von der Natürlichen Bevölkerungsentwicklung und der Migration. Geburten- und Sterberate bestimmen die natürliche Bevölkerungsentwicklung. Ein- und Auswanderungsrate die Migration.\\
Die Kinderzahl pro Frau (Fertilitätsrate) einer Bevölkerung gibt an, wie viele Kinder pro Frau geboren werden. Liegt die Kinderzahl pro Frau bei 2.13, so wird langfristig jedes Paar durch ein neues ersetzt. Die Bevölkerung pflanzt sich auf dem Ersatzniveau der Fertilität fort.

\subsection{Gesetzmässigkeiten des Bevölkerungswachstums}
Die Differenz aus Geburten- und Sterbeziffer heisst Wachstumsrate. Sie bezeichnet den prozentualen jährlichen Anstieg der Bevölkerung. Industrieländer haben tiefe oder gar negative Wachstumsraten, in Entwicklungsländern sind die Werte hoch.\\
Die Verdopplungszeit gibt die benötigte Zeit an, die benötigt wird, damit sich eine Bevölkerung bei konstantem Zuwachs verdoppelt hat. Sie ergibt sich aus der Formel für exponentielles Wachstum.
\begin{equation*}
    2N_0 = N_0(1+r)^t \longrightarrow t = \frac{\log(2)}{\log(1+r)} \qquad r = \text{Wachstumsrate} \quad [t]=\text{Jahre}
\end{equation*}

\subsection{Prognosen zur Welternährung}
\setlength\intextsep{0pt}
\begin{wrapfigure}[10] {r} {4cm}
    \resizebox{4cm}{4cm}{
    \begin{tikzpicture}
        \begin{axis}[   
            samples=50,
            inner axis line style={thick},
            axis lines=center,
            ticks=none,
            xticklabels={,,},
            yticklabels={,,},
            xlabel={Zeit},
            x label style={at={(axis description cs:0.5,0)},anchor=north},
            xmin=0,
            xmax=1.2,
            ymin=0,
            ymax=1.2,
            legend style={legend cell align=left,align=left,draw=white, anchor=north west, at={(axis cs: 0,-0.1)}}
        ]
        \addplot[domain=0:1.2, ultra thick, orangeyellow] {0.5*x + 0.4};
        \addplot[domain=0:1.2, ultra thick, orangeyellow, dashed] {(x^3) +0.1};
        \node[fill=white] at (axis cs: 0.91,0.855) {{\LARGE \Lightning}}; 
        \addlegendentry {{\small Wachstum der \\landwirtschaftlichen Produktion}};
        \addlegendentry {{\small Bevölkerungszuwachs}}; 
        \end{axis}
    \end{tikzpicture}
    }
    \caption{{\scriptsize Bevölkerung und Nahrungsmittelversorgung}}
    \label{fig:Bevölkerung und Nahrungsmittelversorgung}
\end{wrapfigure}
Die umstrittene These des englischen Demografen Malthus besagt, dass die Bevölkerung exponentiell zunehme, während die Nahrungsmittelversorgung nur linear wachse. Ab dem unvermeidlichen Schnittpunkt der Kurven würde die Bevölkerung durch Hunger, Seuchen und Krieg wieder vermindert.

Dieser Idee entspricht das s-förmige Wachstum, bei dem sich die Wachstumsrate nach einer anfänglich exponentiellen Phase, dem Sättigungsniveau der Bevölkerung annähert und die Grenze der Tragfähigkeit erreicht.\\
Von Überbevölkerung ist dann die Rede, wenn ein Raum seine Bevölkerung nicht mehr versorgen kann.

\vspace{0.8cm}

\begin{figure}[h!]
    \centering
    \resizebox{10.5cm}{6.25cm}{
    \begin{tikzpicture}
        \node (graph) at (0,0) {\includegraphics[width=12cm]{images/s_growth.jpeg}};
        
        \node (xLabel1) at (-3.5,-2.7) {\textbf{Zeit}};
        \node (xLabel2) at (3.6,-2.7) {\textbf{Zeit}};

        \node[rotate=90] (yLabel1) at (-6,0) {\textbf{Bevölkerungswachstum}};
        \node[rotate=90] (yLabel1) at (1.1,0) {\textbf{Bevölkerungswachstum}};

        \node[fill=white] at (-3.5,1.7) {Sättigungsniveau};

        \node[text width=12cm] at (0,-3.8) {{\scriptsize Die begrenzten Ressourcen der Umwelt schränken das Wachstum ein. Die exponentielle Wachstumskurve nähert sich bei Vernunftsverhalten dem Sättigungsniveau an. Die Kurve erhält dadurch die Form eines abgescherten s.\\Nach Malthus müsste die Kurve zuerst das Sättigungsniveau überschreiten, danach durch Krieg und Elend sinken und mit der Zeit sich am Sättigungsniveau einpendeln.\\}};
    \end{tikzpicture}
    }
    \caption{S-förmiges Wachstum der Bevölkerungszahl}
    \label{fig:S-förmiges Wachstum der Bevölkerungszahl}
\end{figure}

\subsubsection*{Hatte Malthus recht?}
Ob Malthus recht hatte, ist bis heute umstritten. Kritiker argumentieren, dass das Argument des linearen Nahrungsmittelwachstums nie bewiesen wurde. Vielmehr hätten historische Nahrungsengpässe, durch Erfindergeist, zu Fortschritten in der Agrartechnik geführt.\\
Jedoch bleiben die Schlüsselprobleme bestehen. Die landwirtschaftliche Nutzfläche ist endlich und die Weltbevölkerung wächst weiterhin. Auch kann die maximale Produktionsvolumen an Nahrung einer Region schnell überschritten werden (Sahelzone).\\
Summa summarum scheint an Malthus These etwas dran zu sein, wenn auch nicht im Bezug auf das lineare Wachstum der Nahrungsmittelversorgung.
