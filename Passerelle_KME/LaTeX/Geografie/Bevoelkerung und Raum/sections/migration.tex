\section{Disparitäten und Migration}
\subsection{Disparitäten}
Disparitäten sind Ungleichheiten in der Bevölkerung. In den Disparitäten unterscheidet man drei Hauptgruppen:
\begin{itemize}
    \item soziale Disparitäten
    \item wirtschaftliche Disparitäten
    \item räumliche Disparitäten
\end{itemize}

\textbf{Räumliche Disparitäten} sind die Ursache für Migration. Zwischen verschiedenen Gebieten (lokal, national oder global) bestehen Unterschiede in Lebensbedingungen und Entwicklungsmöglichkeiten. Sie entstehen durch die ungleiche Verteilung von Ressourcen und verstärken sich mit der Zeit durch ungleiche wirtschaftliche Entwicklung.

\subsubsection*{Ausgleich sozialer und räumlicher Disparitäten}
Es liegt im Interesse eines Staates, die bestehenden Disparitäten möglichst gering zu halten. Sonst können soziale Spannungen auftreten.\\
Möglichkeiten des sozialen Ausgleichs sind:
\begin{itemize}
    \item Umverteilungsmassnahmen über das Steuersystem
    \item gut ausgebautes Sozialsystem
    \item Förderung der Chancengleichheit über das Bildungssystem
    \item ausgleichende Regionalpolitik
\end{itemize}

\subsection{Migration - ein Phänomen mit vielen Gesichtern}
Neben der \textbf{räumlichen} Mobilität, muss man auch die \textbf{soziale Mobilität} in Betracht ziehen. Die soziale Mobilität bezeichnet die Veränderung der Position innerhalb einer Gesellschaft (Beförderung, Heirat, Adoption, etc.).
\subsubsection*{Dimensionen der Migration}
Das Spektrum der \textbf{Motivation} für Migration liegt zwischen dem \emph{\glqq Luxus der freien Wahl\grqq{}} und dem \emph{\glqq Terror des Zwangs\grqq{}}. Während die Einen eine frei Wahl wohin und für was sie migrieren. Werden Andere gezwungen zu emigrieren oder müssen fliehen um ihr eigenes Leben zu schützen. Die \textbf{Flucht vor Bürgerkriegen} ist die häufigste Form der \textbf{Zwangsmigration} im heutigen Zeitalter.\\
In \textbf{räumlichen Dimensionen} unterscheidet man zwei Arten von Migration.
\begin{itemize}
    \item \textbf{Binnenmigration} bezeichnet die Migration innerhalb eines Landes oder einer Region.
    \item \textbf{Transnationale Migration} bezeichnet eine länderübergreifende Migration.
    \begin{itemize}
        \item internationale Migration
        \item interkontinentale Migration
    \end{itemize}
\end{itemize}
In den \textbf{zeitlichen Aspekten} kann man unterscheiden, ob eine Migration \textbf{andauernd} oder \textbf{vorübergehend} ist. So lassen sich die Bewegungen von Nomaden (andauernd) klar von der irischen Auswanderung nach Amerika abgrenzen (vorübergehend).\\
Auch lässt sich die Migration nach ihrem \textbf{Umfang} kategorisieren. Dabei unterscheidet man zwischen \textbf{Einzel-}, \textbf{Gruppen-} und \textbf{Massenmigration}.

\subsection{Gründe der Migration}
Während in der Zeit vor der Industrialisierung natürliche Veränderung der Umwelt und die Erschliessung von neuen Gebieten die Hauptgründe für Migrationen waren, sind die heutigen Beweggründe von sozialer und wirtschaftlicher Natur. Auch kommt es nach dem Zusammenbruch des Ostblock durch ausbrechende Bürgerkriege immer mehr zu Flüchtlingsströmen.\\
Die Intensität des Migrationsstroms zwischen zwei Gebieten ist von folgenden Faktoren abhängig:
\begin{itemize}
    \item \textbf{Räumliche Disparität} zwischen dem Herkunfts- und Zielgebiet.
    \item Stärke des \textbf{Informationsflusses} vom Ziel- zum Herkunftsgebiet.
    \item \textbf{Distanz} zwischen dem Herkunfts- und Zielgebiet.
\end{itemize}
Die abstossenden Gesichtspunkte des Herkunftsgebiets (\textbf{Push-Faktoren}) werden gegen die anziehenden Aspekte der Zielregion (\textbf{Pull-Faktoren}) abgewogen. Dabei verläuft die Abwägung der Push- und Pull-Faktoren meist nicht objektiv und rational, sondern steht unter starkem Einfluss von idealisierten Wunschvorstellungen. Auch werden die verschiedenen Faktoren nicht von allen Individuen gleich gewichtet.\\
Die meisten Migranten folgen ihnen bereits bekannten Wegen. Oftmals leben Verwandte von ihnen schon im Zielgebiet und bieten ihnen so Sicherheit und erste Anhaltspunkte in der Fremde.

\subsection{Geografische Bedeutung der europäischen Ausbreitung nach Übersee}
Durch das Bevölkerungswachstum in Europa entstand im 18. und 19 Jahrhundert ein hoher Bevölkerungsdruck. Dieser konnte, in Kombination mit den beschränkten landwirtschaftlichen Arbeitsmöglichkeiten, nur durch Abwanderung verringert werden. Man schätzt, dass bei der europäischen Ausbreitung rund 55 Mio. Menschen den Kontinent verlassen und sich in Nord- und Südamerika angesiedelt haben.\\
Das kulturelle und politische Erbe dieser Völkerwanderung ist heute im politischen und kulturellen Mosaik der Völker, Sprachen und Religionen erkennbar.

\subsection{Nomaden}
Nomaden sind Dauerwanderer. Ihre Lebens- und Wirtschaftsform basiert auf räumlicher Mobilität. Sie betreiben Wanderwirtschaft, wobei die Hirten den Tieren auf der Futtersuche folgen und dabei weite Distanzen zurücklegen. Jedoch werden die dafür notwendigen, weiträumigen Lebensräume immer knapper.\\
Ein Fallbeispiel dafür sind die Nomaden der Sahelregion\footnote{Übergangszone zwischen Sahara und der südlich angrenzenden Savanne.}. Durch den Bau von Tiefbrunnen wuchsen die Herden der Nomaden. Die Überweidung durch die grösseren Herden und den sinkenden Grundwasserspiegel, aus der daraus resultierenden Desertifikation vermindert sich die Weidefläche der Nomaden. Als Reaktion darauf treiben sie ihre Herden immer weiter südlich in das Gebiet von sesshaften Bauern mit denen sie dadurch in Konflikt kommen.

\newpage

\subsection{Landflucht und Verstädterung}
Weltweit konzentrieren sich die Menschen in städtischen Siedlungen und verlassen die ländlichen Gebiete. Die \textbf{Verstädterung} bezeichnet die Ansammlung von Menschen in Städten; während \textbf{Landflucht} das Verlassen der ländlichen Gebiete betitelt. \\
\begin{multicols} {2}
    \textbf{Push-Faktoren}
    \begin{itemize}
        \item Wachsende Bevölkerung auf dem Land.
        \item Grund und Boden sind begrenzt und unterliegen starren Besitzverhältnissen.
        \item Entwicklung der Agrartechnik $\rightarrow$ Es werden weniger Arbeitskräfte benötigt.
        \item Durch änderndes Konsumverhalten erzielen Agrarprodukte immer tiefere Preise.
    \end{itemize}
    
    \columnbreak

    \textbf{Pull-Faktoren}
    \begin{itemize}
        \item Durch den Informationsaustausch steigt die Wertschätzung der (vermeintlichen) Annehmlichkeiten der Stadt.
        \item Industrialisierung lässt auf Arbeitsplätze in den Städten hoffen.
        \item Bessere Versorgungslage im Gesundheits- und Sozialwesen.
        \item Bessere Bildungsmöglichkeiten.
    \end{itemize}
\end{multicols}

Die Landflucht und Verstädterung haben einen verstärkenden Einfluss aufeinander (positive Rückkopplung).

\subsection{Flüchtlinge}
\textbf{Politische Flüchtlinge} fliehen vor Gewalt und Verfolgung aus ihrem Heimatland. \textbf{Wirtschaftliche Flüchtlinge} wandern aus wirtschaftlichen Gründen oder wegen einer Natur- oder ökologischen Katastrophe aus. Nur politische Flüchtlinge erhalten Asyl; wirtschaftliche Flüchtlinge müssen das Land mehr oder weniger freiwillig wieder verlassen.

\subsection{Trends und Prognosen}
Die internationale Migration wird auch weiterhin auf den bestehenden Bahnen verlaufen. Die begehrtesten Ziele wie Nordamerika, Europa oder die Golfstaaten sind meist nur Fachkräften offen. Die grossen Migrationsströme verlaufen deshalb ausserhalb dieser Gebiete. Die unterschiedliche Entwicklung der Entwicklungsländer wird die Ströme innerhalb dieser Gebiete noch verstärken.\\
Zwar verursachen Einwanderer im Zielgebiet wirtschaftliche und politische Kosten, jedoch bieten sie auch eine ganze Reihe von Vorteilen. Diese Vorteile sind unter Anderem das Hinzukommen von ausländischen Fachkräften und die Verzögerung der Überalterung der Bevölkerung.\\
Genaue Prognosen über die Entwicklung der Migrationsströme sind aufgrund der komplexen Natur nicht möglich.

\newpage

\subsection{Mali - traditionelle Landwirtschaft versus Marktfruchtbau}
Das Land Mali liegt in einem klimatisch schwierigen Gebiet: der Wüste Sahara und der Sahelzone. Entsprechend liegt ein grosser Teil der Landfläche im Bereich der tropischen Trocken- oder Wüstenklimate mit unregelmässigen Niederschlägen und einer wiederkehrenden Dürregefahr.\\
Wanderackerbau und Nomadismus sind die traditionellen Bewirtschaftungsmethoden dieser Klimazone. Durch die Kolonialisierung wurde jedoch der Anbau von Marktfrüchten für den Export, vor allem Baumwolle, vorangetrieben. Auch versuchte man, den knappen Wasservorräten durch das Anlegen von Tiefenbrunnen zu begegnen.\\
Die Nomaden (z.B. Tuareg), hiessen diese ganzjährigen Wasserstellen willkommen und blieben in der Nähe. Dadurch übernutzen ihre Herden die dünne Grasnarbe. Das starke Bevölkerungswachstum (Kinderreichtum ist ein wichtiger Faktor für den Bestand der Familie), der Bewässerungsfeldbau und die Überweidung sind der Grund für die weitere Ausbreitung der Wüste.\\
Heute wird das Land durch einen schweren Bürgerkrieg erschüttert, der auf dem Unabhängigkeitskrieg der Tuareg und den islamistischen Angriffen gegen die Regierung beruht.