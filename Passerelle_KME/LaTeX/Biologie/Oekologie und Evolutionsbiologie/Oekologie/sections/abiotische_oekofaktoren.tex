\section{Abiotische Ökofaktoren}
\subsection{Ökologische Potenz}
\subsubsection*{Toleranzpunkte und Kardinalpunkte}
Die direkte Wirkung eines Ökofaktors auf ein Lebewesen bzw. eine Population wird experimentell ermittelt. Dabei wird untersucht, wie sich eine bestimmte Leistung verändert, wenn sich der Wert/Intensität des Ökofaktors verändert. Diese Messresultate werden in einer \emph{Toleranzkurve} dargestellt.
\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
            xmin = 0,
            xmax = 10,
            samples=100,
            yticklabels={,,},
            ytick style = {draw = none},
            xticklabels={,,},
            xtick style = {draw = none},
            inner axis line style={thick},
            axis lines=center,
            axis line style={thick},
            ylabel = {{\small Aktivität}},
            xlabel = {{\small Intensität des Umweltfaktors}},
            x label style={at={(axis description cs:0.5,0)},anchor=north},
            y label style={at={(axis description cs:0,.5)},rotate=90,anchor=south},
            clip=false,
            height=5cm,
            width=10cm
        ]
            \addplot[very thick, darkred, domain = 1:9] {gauss(5,1)};
    
            \node[draw=white, shape=circle, fill=darkblue, scale=0.3, thick] (minMarker) at (axis cs: 1,0) {};
            \node[darkblue, text centered] (minText) at (axis cs: 1, 0.6) {Minimum};
            \draw[darkblue] (minMarker) -- (minText);
    
            \node[draw=white, shape=circle, fill=darkblue, scale=0.3, thick] (maxMarker) at (axis cs: 9,0) {};
            \node[darkblue, text centered] (maxText) at (axis cs: 9, 0.6) {Maximum};
            \draw[darkblue] (maxMarker) -- (maxText);
    
            \node[draw=white, shape=circle, fill=darkblue, scale=0.3, thick] (optMarker) at (axis cs: 5, 0.4) {};
            \node[darkblue, text centered, above = 0cm of optMarker] (optText) {Optimum};
    
            \node[darkblue, text centered, text width = 5cm] (label1) at (axis cs: 5, 0.53) {{\small Überlebensbereich/Toleranzbereich}};
            \draw[<-, >=stealth, darkblue] (axis cs: 1.1, 0.53) -- (label1);
            \draw[->, >=stealth, darkblue] (label1) -- (axis cs: 8.9, 0.53);
    
            \draw[darkgreen] (axis cs: 2.5, 0) -- (axis cs: 2.5, 0.4);
            \draw[darkgreen] (axis cs: 7.5, 0) -- (axis cs: 7.5, 0.4);
            \node[darkgreen, text centered, text width = 1.5cm] (label2) at (axis cs: 5, 0.1) {{\small Volle Aktivität}};
            \draw[->, >=stealth, darkgreen] (label2) -- (axis cs: 2.6, 0.1);
            \draw[->, >=stealth, darkgreen] (label2) -- (axis cs: 7.4, 0.1);
        \end{axis}
    \end{tikzpicture}
    \caption{Toleranzkurve und Kardinalpunkte}
    \label{}
\end{figure}
Der \emph{Toleranzbereich} ist der Bereich, in dem ein Organismus \emph{überleben} kann.\\
Innerhalb des Toleranzbereichs liegt das \emph{Optimum}, bei dem das \emph{Wachstum am stärksten} ist. Die Fähigkeit, \emph{Schwankungen des Umweltfaktors} innerhalb des Toleranzbereichs zu \emph{ertragen}, nennt sich \emph{ökologische Potenz}. Innerhalb des Toleranzbereichs liegt der Bereich in dem der Organismus vollständig aktiv ist und sich auf fortpflanzt.\\
Minimum, Maximum und Optimum sind \emph{Kardinalpunkte}.\\
Da Ökofaktoren voneinander Abhängig sind können sich die Kardinalpunkte eines Faktors ändern, wenn sich ein anderer Ökofaktor ändert. Beispielsweise ist die Optimaltemperatur oft von der Luftfeuchtigkeit abhängig. Will man mehrere, in Verbindung stehende Ökofaktoren visualisieren, stellt man diese in einem mehrdimensionalen Koordinatensystem dar. Dabei werden die Ökofaktoren als Achsen verwendet.

\subsubsection*{Limitierender Faktor}
Je \emph{weiter} ein \emph{Faktor vom Optimum entfernt} ist, desto \emph{stärker} fällt er für einen \emph{Organismus ins Gewicht} und desto wichtiger ist seine Optimierung. \\
Nach dem Konzept des \emph{limitierenden Faktors} ist die Aktivität einer Population von dem Faktor abhängig, der \emph{am weitesten} von seinem \emph{Optimum entfernt} ist.

\subsubsection*{Unterschiede in der ökologischen Potenz}
Der Toleranzbereich für einen Ökofaktor ist bei verschiedenen Arten sehr unterschiedlich. Arten mit \emph{grosser ökologischer Potenz} nennt man \emph{euryök}. Arten mit \emph{kleiner ökologischer Potenz} nennt man \emph{stenök}.\\
Gilt eine solche Aussage nur für einen Faktor, wird die Vorsilbe euryo- oder steno- verwendent. Bsp: euryotherm und stenotherm

\subsubsection*{Verbreitung, Zeigerarten}
Die ökologische Potenz bestimmt die \emph{mögliche Verbreitung}. \emph{Euryöke} Arten haben ein \emph{grosses potenzielles Verbreitungsgebiet}. \emph{Stenöke} Arten haben ein \emph{kleines potenzielles Verbreitungsgebiet}. Das potenzielle Verbreitungsgebiet darf aber nicht mit der effektiven Verbreitung verwechselt werden. Die effektive Verbreitung ist das Resultat davon, wie gut sich eine Art gegen ihre Konkurrenten durchsetzen kann.\\

\paragraph*{Zeigerarten} sind Arten mit geringer Toleranz gegenüber einem spezifischen Ökofaktor und ermöglichen so Rückschlüsse über einen Standort.
\begin{itemize}
    \item \emph{Zeigerpflanzen} weisen auf bestimmte \emph{Eigenschaften des Bodens} hin:
    \begin{itemize}
        \item Brennnesseln und Blacke\footnote{Wiesenampfer} sind Stickstoffzeiger.
        \item Heidekraut und Heidelbeere gedeihen auf sauren Böden.
        \item Salbei und Huflattich bevorzugen alkalische Böden.
    \end{itemize}
    \item \emph{Flechten} geben Aufschluss über die \emph{Luftverschmutzung}.
    \item Gewisse \emph{Wasserbewohner} geben Hinweise auf die \emph{Wasserqualität}.
\end{itemize}

\subsubsection*{Zeitliche Schwankungen}
Die meisten Ökofaktoren schwanken im Verlauf der Zeit.
\begin{itemize}
    \item Die \emph{tageszeitliche Periodik} kann die \emph{Aktivitäten} eines Lebewesen bestimmen. Bsp: Tag-Nacht-Rhythmus
    \item Die \emph{jahreszeitliche Periodik} kann die \emph{Entwicklung} mitbestimmen. Bsp: Blütezeit bei Pflanzen oder Paarungsbereitschaft bei Tieren.
\end{itemize}

\subsubsection*{Dauerstadien}
Lebewesen können \emph{ungünstige Perioden} mit sog. Dauerstadien überstehen, indem sie ihre \emph{Aktivitäten vermindern}. So können Organismen (extreme) Kälteperioden (Winterschlaf oder -starre) oder extreme Dürreperioden überstehen.

\subsubsection*{Indirekte Wirkung}
Ökofaktoren können auch eine \emph{indirekte Wirkung} haben, indem sie Feinde, Beute oder Konkurrenten beeinflussen.

\subsubsection*{Anpassung der Lebewesen an abiotische Faktoren}
Organismen können auf ungünstige Bedingungen reagieren, indem sie sich kurzfristig individuell oder langfristig durch Mutation oder Selektion anpassen.\\
\emph{Individuelle Anpassung} geschieht durch das Auftreten von \emph{Modifikationen}. Solche Modifikationen können die Anpassung der Gestalt bei Pflanzen oder Akklimatisierung und die Zahl der roten Blutkörperchen bei Tieren sein.\\
\emph{Längerfristige Anpassungen} geschehen durch \emph{Mutationen und Selektion}. Die Ökofaktoren sind dabei aber nicht die Treiber der Mutation, sie sind lediglich die Selektionsfaktoren. Die Chance einer Population sich erfolgreich an einen Ökofaktor anzupassen, steigt mit der Grösse der genetischen Variabilität.\\
Kulturpflanzen und Nutztiere besitzen eine geringe genetische Variabilität und sind deswegen stark anfällig für ändernde Ökofaktoren.

\subsubsection*{Arten der abiotischen Ökofaktoren}
Abiotische Ökofaktoren lassen sich in drei Gruppen aufteilen:
\begin{tabbing}
    \emph{Klima} \qquad \= z.B: Licht, Wasser, Zusammensetzung der Luft, Wind\\
    \emph{Lage} \> z.B: Höhenlage, Raumstruktur, Exposition\\
    \emph{Boden} \> z.B: Bodenart, Mineralsalzgehalt, mechanische Eigenschaften\\ 
\end{tabbing}

\subsection{Temperatur}
\subsubsection*{Wirkung der Temperatur auf Lebensvorgänge}
Die Geschwindigkeit chemischer Reaktionen ist von der Temperatur abhängig. Durch eine Temperaturveränderung von \SI{10}{\degreeCelsius} verändert sich die Reaktionsgeschwindigkeit um einen Faktor von $2$.\\
Der Toleranzbereich der meisten Lebewesen liegt zwischen \SI{-20}{\degreeCelsius} und \SI{+40}{\degreeCelsius}. Hitze- und kältetolerante Bakterien überleben bei bis zu \SI{-200}{\degreeCelsius} bzw. \SI{+150}{\degreeCelsius}.\\
Sinkt die Temperatur innerhalb eines Lebewesen \emph{unterhalb} des \emph{Gefrierpunkts} werden die \emph{Zellen} durch Eiskristalle \emph{beschädigt}. Dabei entstehen die Schäden oft beim Auftauen, wenn sich die Kristalle bewegen. Steigt die Temperatur innerhalb eines Lebewesens \emph{über $40-$\SI{50}{\degreeCelsius}}, \emph{denaturieren} seine \emph{Proteine}. Gleichwarme Organismen können mit Temperaturschwankungen besser umgehen, da ihre Innentemperatur konstant gehalten wird.

\subsubsection*{Bedeutung der Temperatur für Pflanzen}
Pflanzen sind Temperaturschwankungen besonders stark ausgesetzt, da sie standortgebunden und wechselwarm sind. Das Temperaturminimum ist durch das \emph{Gefrieren} von Flüssigkeiten im Pflanzenkörper gegeben, wobei der Gefrierpunkt durch gelöste Zucker und Proteine gesenkt werden kann. Hohe Temperaturen führen bei Pflanzen oft zum \emph{Austrocknen}. \\
Auch ist die Temperatur ein entscheidender Faktor für die \emph{Verbreitung} der Pflanzenarten. Die gürtelförmige Anordnung der Vegetationszonen vom Äquator aus, ist durch die Temperatur bestimmt.\\
Pflanzen können sich vor der Überhitzung schützen, indem sie über ihre Blätter Wasser verdunsten.\\
In den gemässigten bis polaren Klimazonen richten sich Pflanzen nach der jahreszeitlichen Periodik. Vorgänge wie Austreiben und Blütenbildung werden von der Temperatur diktiert.
Sommergrüne Gewächse überwintern, indem sie ihre \emph{Blätter abwerfen}. Krautige Pflanzen ziehen sich über Winter in den \emph{Boden} zurück.

\subsubsection*{Wirkung der Temperatur auf wechselwarme Tiere}
Bei wechselwarmen Tieren hängt die Körpertemperatur von der Aussentemperatur ab. Sie können aber extremen Temperaturen ausweichen. Bei \emph{Temperaturen ausserhalb} des \emph{Maximums} oder \emph{Minimums} verfallen diese in eine \emph{Hitze-} bzw. \emph{Kältestarre}. Wechselwarme Tiere sind mehrheitlich \emph{stenotherm}. Arten die sich dauerhaft bewegen, können ihre Körpertemperatur mittels der umgesetzten Energie konstant halten. Staatsbildende Insekten regulieren die Temperatur im Stock als Population.

\subsubsection*{Wirkung der Temperatur auf gleichwarme Tiere}
Gleichwarme Tiere regulieren ihre Körpertemperatur unabhängig von der Aussentemperatur. Dadurch ist ihre \emph{ökologische Potenz grösser} als die der Wechselwarmen, ihre \emph{Aktivität im Toleranzbereich} ist praktisch \emph{konstant} und sie können auch in kalten Nächten oder im Winter aktiv sein. Jedoch können sie ihre ihre Temperatur \emph{ausserhalb} des \emph{Minimums} oder \emph{Maximums} nicht halten und \emph{überleben nur kurze Zeit}.
\newpage
Da die Körpertemperatur meist \emph{über} der Aussentemperatur liegt, müssen Gleichwarme oft aktiv \emph{heizen}. Dies geschieht über die \emph{Abwärme} der \emph{Zellatmung}. Reicht diese nicht aus, kann durch \emph{Zitterbewegungen} der \emph{Muskeln} Wärme erzeugt werden.\\
Zur Konstanthaltung der Körpertemperatur tragen folgende Leistungen bei:
\begin{itemize}
    \item \emph{Hoher Stoff-} und \emph{Energieumsatz}.
    \item \emph{Leistungsfähiger Darm}, \emph{Lunge} und \emph{Kreislauf}.
    \item \emph{Verteilung der Wärme} hauptsächlich auf die \emph{Muskeln} und \emph{inneren Organe} mittels dem Blutkreislauf.
    \item \emph{Regelsystem} mit \emph{Temperaturfühlern}
    \item \emph{Fettschicht}
    \item \emph{Fell} oder \emph{Federn}
    \item Regulation der Wärmeabgabe über die \emph{Hautdurchblutung} oder die \emph{Abgabe} von \emph{Flüssigkeit} über Haut oder Lunge.
\end{itemize}
Da es schwer sein kann im Winter genügend Nahrung zu finden, machen viele wechselwarme Tiere einen \emph{Winterschlaf} oder eine \emph{Winterruhe}.
\emph{Winterschläfer} ziehen sich im Herbst an einen kältegeschützten Ort zurück und überwintern dort. \emph{Alle Körperfunktionen} werden \emph{reduziert} und ihre \emph{Körpertemperatur abgesenkt} (auf $0-\SI{9}{\degreeCelsius}$). Sie zehren während dem Winterschlaf von ihren \emph{Fettreserven}. Der Winterschlaf dauert je nach Art zwischen drei und sieben Monaten.
Bei der \emph{Winterruhe} wird die \emph{Körpertemperatur nicht so stark abgesenkt} und die Tiere sind \emph{häufiger wach}.
Auslöser für den Winterschlaf sind \emph{äussere Faktoren} wie Aussentemperatur oder Nahrungsmangel und \emph{innere Faktoren} wie die innere Uhr oder die Fettreserven.\\
Tiere die im Winter aktiv bleiben \emph{senken} ihre \emph{Körpertemperatur} während dem \emph{Schlafen}. Dieses Phänomen nennt sich \emph{Nachtabsenkung}.\\
Da flugfähige Vögel \emph{keine Fettreserven} anlegen können, reduzieren manche Vögel ihren Stoffwechsel oder wandern im Herbst in wärmere Gebiete aus.\\
Auch die Fortpflanzung richtet sich nach den Jahreszeiten. Nachkommen werden im Frühjahr geboren, da dann genügend Futter vorhanden ist und sie bis zum Winter eine gewisse Grösse erreichen können.\\
Kleine Tiere verbrauchen auf die gleiche Masse bezogen mehr Energie als grosse. Ein Grund für den \emph{höheren Energieverbrauch} ist, dass die \emph{Wärmeproduktion im inneren} stattfindet und der \emph{Wärmeverlust über die Haut} geschieht. D.h. je grösser ein Tier ist, desto weniger Wärme verliert es relativ zu der Wärme die es produziert. Beim Vergleich der Körperformen verwandter Tiere in unterschiedlichen Temperaturregionen fallen folgende zwei Regel auf:
\begin{itemize}
    \item \emph{Bergmann'sche Regel}: Tiere in \emph{kalten Gebieten} sind \emph{grösser}, als ihre nahen Verwandten in wärmeren Gebieten.
    \item \emph{Allen'sche Regel}: Tiere in \emph{kalten Gebieten} haben \emph{kürzere} Beine, Ohren und Schwänze. 
\end{itemize}
Die Bergmann'sche Regel lässt sich damit erklären, dass mit zunehmender Körpergrösse das wärmeproduzierende Volumen im Verhältnis grösser ist als die Fläche, die Wärme verliert. Die Allen'sche Regel vermindert die Oberfläche weiterhin.

\newpage

\subsection{Licht}
Während das Licht die Grundlage des Lebens bildet, indem es Energie für die Fotosynthese liefert, kann ein Teil der UV-Strahlen Mutationen auslösen und somit Zellen beschädigen. Gegen diese Strahlen sind die meisten Landtiere durch \emph{Pigmente} geschützt.

\subsubsection*{Bedeutung des Lichts für Pflanzen}
Die \emph{Geschwindigkeit} der \emph{Fotosynthese} nimmt mit der \emph{Lichtintensität} zu, bis ein Höchstwert erreicht wird und bleibt dann konstant. Bei einer zu hohen Beleuchtungsstärke nimmt die Fotosyntheseleistung wieder ab, da zu intensives Licht schädlich ist.\\
Bei \emph{Sonnenpflanzen} liegt die maximale Fotosynthesegeschwindigkeit bei \emph{stärkerer Beleuchtung} als bei Schattenpflanzen. In trockenen Gebieten müssen Pflanzen ihre Spaltöffnungen tagsüber zeitweise schliessen, um nicht zu viel Wasser zu verlieren. Das stoppt aber die Kohlenstoffdioxidaufnahme, wodurch die Fotosynthese langsamer wird. \emph{C4-Pflanzen} kommen mit einer tieferen Kohlenstoffdioxidkonzentration aus, indem sie es an 3 C-Atome binden.\\
Das Licht beeinflusst das \emph{Wachstum}, \emph{Gestalt} und \emph{Entwicklung} der Pflanze. Sprosspflanzen wachsen zum Licht hin und richten ihre Blätter nach dem Licht aus, da der Stängel auf der lichtabgewandten Seite stärker wächst (\emph{Fototropismus}). Auch \emph{hemmt} Licht das \emph{Längenwachstum} einer Pflanze und \emph{fördert} die Bildung von \emph{Blätter}. Blätter die mehr Sonnenlicht erhalten, können eine \emph{zweite Schicht} an \emph{Zellen} für die \emph{Fotosynthese} ausbilden. Blätter die weniger Sonnenlicht erhalten besitzen nur eine Schicht diese Zellen, werden dafür aber grösser um das Licht effizienter zu nutzen.\\
Die Länge von Tag und Nacht ist ein Signalgeber für folgende Vorgänge:
\begin{itemize}
    \item Die \emph{Blütenbildung} ist von der Länge der ununterbrochenen Dunkelheit abhängig. 
    \item Das \emph{Austreiben} der Pflanzen wird durch die Tageslänge und Temperatur im Frühjahr diktiert.
    \item Die \emph{Keimung der Samen} kann vom Licht abhängen.
    \item Das Öffnen der Spaltöffnungen und Schliessen der Blüten wird durch Licht ausgelöst.
    \item Einzellige Algen, die sich aktiv bewegen können, schwimmen zum Licht (\emph{Fototaxis})
\end{itemize}

\subsubsection*{Bedeutung des Lichts für Tiere}
Tieren mit Augen dient Licht zur \emph{Orientierung}. Es kann \emph{Stoffwechselprozesse} auslösen, wie die Produktion von Vitamin D. Auch dient der Tag-Nacht-Rhythmus als Taktgeber für die \emph{tageszeitliche Periodik} des \emph{Stoffwechsels} und dem \emph{Schlaf-Wach-Rhythmus}. Die Veränderung der Tageslänge kann auch \emph{jahreszeitabhängige Aktivitäten} wie die Balz auslösen.

\subsection{Wasser}
\subsubsection*{Allgemeine Bedeutung des Wassers für Lebewesen}
\begin{itemize}
    \item Alle \emph{Stoffwechselvorgänge} laufen in wässrigen Lösungen ab und viele Stoffwechselreaktionen verwenden Wasser als Edukt.
    \item Wasser ist der mengenmässige dominierende \emph{Bestandteil aller Lebewesen}.
    \item Wasser dient als \emph{Lebensraum} oder Transportmittel.
    \item Skelettlose Organismen werden durch den Druck des Wasser in ihrem Inneren gestützt.
\end{itemize}
Neben Lebewesen, die ihren Wasserhaushalt konstant halten, gibt es auch solche, die ihren Wasserhaushalt der Umgebung anpassen. Sie können so austrocknen und nehmen Wasser auf sobald es wieder feuchter ist.

\subsubsection*{Wasserhaushalt der Wasserbewohner}
Wasserbewohner müssen ihren Wasserhaushalt \emph{aktiv regulieren} (\emph{Osmoregulation}), da die Konzentration der gelösten Stoffe in der Umgebung anders ist als in ihrem Körper. Daher führt die Osmose zum Eindringen oder Abgabe von Wasser.\\
Bei \emph{Meeresbewohner} haben die Körperflüssigkeiten meist eine tiefere Salzkonzentration als das Meerwasser. Die Osmose führt zu \emph{Wasserabgabe}. Diesen Wasserverlust kompensieren sie mit \emph{aktiver Wasseraufnahme} (trinken) und \emph{Salzausscheidung}.\\
Bei \emph{Süsswasserbewohnern} führt die Osmose zur \emph{Aufnahme von Wasser}, da die Konzentration der gelösten Stoffe im Lebewesen höher ist. Sie müssen deswegen \emph{aktiv Wasser ausscheiden}. Bei Pflanzen beschränkt die \emph{Zellwand} die Wasseraufnahme. Einzeller verfügen meist über \emph{pulsierende Vakuolen}, die das Wasser nach aussen pumpen. Tiere scheiden Wasser durch \emph{Exkretionsorgane} zusammen mit den Endprodukten des Stoffwechsels aus.\\
Viele niedere Wasserbewohner sind \emph{isotonisch}. D.h. die Konzentration im Inneren ist gleich hoch wie in der Umgebung.

\subsubsection*{Anpassung ans Landleben}
Die \emph{Haut} von Landlebewesen verhindert, dass diese zu viel \emph{Wasser verlieren}. Bei Wirbeltieren ist diese verhornt; Gliederfüsser verfügen über einen Chitinpanzer. Dadurch können Landlebewesen nicht mehr durch ihre Haut atmen.\\
\emph{Ausscheidungsorgane konzentrieren} den \emph{Harn}, damit nicht zu viel Wasser verloren geht. Auch wird im Darm dem Nahrungsbrei möglichst viel Wasser entzogen.\\
Landeier müssen durch eine Schale vor dem Austrocknen geschützt werden, was eine innere Befruchtung voraussetzt.\\
Landlebewesen haben einen leistungsfähigeren Seh- und Geruchssinn. Auch riechen und schmecken sie mit zwei verschiedenen Organen.\\
Landtiere nehmen Wasser über ihre \emph{Nahrung} und durch \emph{Trinken} auf. Durch die Zellatmung kann aus der Nahrung Wasser gewonnen werden.

\subsubsection*{Wasserhaushalt der Landpflanzen}
Landpflanzen müssen Wasser aus dem \emph{Boden} aufnehmen. Ihre Oberfläche muss gegen \emph{unkontrollierten Wasserverlust} geschützt sein. Die meisten Landpflanzen sind Sprosspflanzen, deren Körper aus einem beblätterten Spross und einer Wurzel besteht. Sie nehmen Wasser über ihre Wurzel auf und transportieren es durch Gefässe in den Rest der Pflanze. Der Wassertransport innerhalb einer Pflanze beruht auf \emph{Transpiration} und den \emph{Anziehungskräften} zwischen Wassermolekülen. Moleküle, welche die Gefässe verlassen, verdunsten und ziehen weitere Moleküle nach. Auch werden so neue Wassermoleküle in die Wurzel aufgenommen. Diese Form des Wassertransports nennt sich \emph{Transpirationsstrom}. Damit eine Pflanze nicht zu viel Wasser verliert, ist ihre Oberfläche durch eine wachsartige Schicht (\emph{Cuticula}) gegen Verdunstung geschützt. Haare reduzieren die Luftbewegung auf der Oberfläche und vermindern so die Verdunstung. Da eine Pflanze für die Fotosynthese Kohlenstoffdioxid aufnehmen muss, verfügt sie über \emph{Spaltöffnungen} für den Gasaustausch. Um sich vor der Hitze zu schützen, können Pflanzen sich durch Transpiration kühlen.

\newpage

\paragraph*{Anpassungen von Pflanzen an die Verfügbarkeit von Wasser}
\begin{itemize}
    \item Trockenpflanzen
        \begin{itemize}
            \item Dicke Cuticula und tote Haare.
            \item Wenige Spaltöffnungen, die tief im Blatt sitzen.
            \item Kleine Blätter oder Dornen.
            \item Verdickter Stamm, der Wasser speichern kann.
        \end{itemize}
    \item Feuchtpflanzen
        \begin{itemize}
            \item Dünne Cuticula.
            \item Grosse Blätter.
            \item Zahlreiche Spaltöffnungen an höheren Positionen.
        \end{itemize}
    \item Wasserpflanzen
        \begin{itemize}
            \item Können Wasser, Mineralstoffe und Kohlenstoffdioxid über ihre ganze Oberfläche aufnehmen.
            \item Blätter die auf dem Wasser liegen besitzen Spaltöffnungen auf der Oberseite.
            \item Stängel und Blattstiele besitzen Luftröhren, die Luft zu den Wurzeln leiten.
        \end{itemize}
\end{itemize}

Mehrjährige Pflanzen haben im Winter Probleme mit der Wasserversorgung. Laubbäume werfen ihre Blätter ab, um die Transpiration zu senken. Die Nadeln der Nadelbäume sind durch ihre geringe Grösse und eine dicke Wachsschicht besser vor der Verdunstung geschützt.