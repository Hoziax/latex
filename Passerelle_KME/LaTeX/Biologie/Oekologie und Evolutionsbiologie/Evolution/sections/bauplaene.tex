\section{Baupläne}
\subsection{Bauplanvergleiche}
Vergleicht man das Aussehen von Delfinen und Haien, ähneln sich diese mit ihrem torpedoförmigen Körperbau auf den ersten Blick. Betrachtet man aber beide Tiere genauer, verschwindet die Ähnlichkeit recht schnell. Ihr ähnliches Aussehen scheint das Resultat konvergenter Evolution zu sein.\\
Vergleicht man die Vorderextremitäten von Menschen und Fledermäusen, fallen im Knochenbau sofort Ähnlichkeiten auf, wobei beide Exemplare für sehr verschiedene Zwecke verwendet werden. Diese Ähnlichkeiten scheinen auf einen gemeinsamen Vorfahren zurückzuführen.

\subsubsection*{Homologie und Analogie}
Anhand der oben genannten Beispiele gibt es zwei Ursachen für anatomische Ähnlichkeiten. Zum einen aufgrund der \textbf{Lebensweise}, zum anderen aufgrund der \textbf{Abstammung}.

\paragraph*{Homologe} Strukturen sind das Resultat einer \textbf{gemeinsamen Abstammung} und dementsprechend einer gemeinsamen genetischen Grundlage. Sie haben meist \textbf{unterschiedliche Funktionen} und sind das Resultat von evolutionärer \textbf{Divergenz}\footnote{Anpassung mittels Selektion an unterschiedliche Umweltbedingungen}.

\paragraph*{Analoge} Strukturen ähneln sich aufgrund ihrer \textbf{Form} und \textbf{Funktion} und beruhen \underline{nicht} auf einem gemeinsamen Bauplan. Sie entstehen durch \textbf{Konvergenz}.

\subsubsection*{Beispiele von Analogien}
\paragraph*{Analoge Flügel} sind von den Flugsauriern, Fledermäusen und Vögeln bekannt.
\begin{figure}[h!]
    \centering
    \begin{tikzpicture}
        \node[] (winged_dinosaur) at (0,0) {\includegraphics[width = 4cm]{images/analogismus_01.png}};
        \node[above = 0.35cm of winged_dinosaur] {{\small Flugsaurier}};

        \node[right = 0.5cm of winged_dinosaur] (bat) {\includegraphics[width = 4cm]{images/analogismus_02.png}};
        \node[above = 0.5cm of bat] {{\small Fledermaus}};

        \node[right = 5cm of winged_dinosaur] (bird) {\includegraphics[width = 4cm]{images/analogismus_03.png}};
        \node[above = 0cm of bird] {{\small Vogel}};
    \end{tikzpicture}
    \caption{Analoge Flügel}
    \label{fig:analogus wings}
\end{figure}

\paragraph*{Analoge Grabbeine} sind von den Maulwürfen und von den Maulwurfsgrillen bekannt. Beide verwenden ihre analogen Vorderextremitäten um unterirdische Gangsysteme zu graben.

\paragraph*{Nachahmung} kann auch ein Fall der Analogie sein. Ein Beispiel dafür, ist die \textit{Hummel-Ragwurz} (Orchidee), die mit ihrer Blüte eine Biene nachahmt und einen Sexuallockstoff abgibt, um Bestäuber anzulocken.

\paragraph*{Mimikry} ist ein Sonderfall der Nachahmung. Dabei ahmen harmlose Tiere, wehrhafte oder giftige Pflanzen oder Tiere nach, um sich vor Fressfeinden zu schützen.

\subsubsection*{Homologiekriterien}
Um als homologe Struktur zu qualifizieren, sollte sie eine der folgenden Kriterien erfüllen:
\begin{itemize}
    \item Kriterium der \textbf{Lage}
    \item Kriterium der \textbf{speziellen Qualität} der Strukturen
    \item Kriterium der \textbf{Stetigkeit} (Verknüpfung der Zwischenformen)
\end{itemize}

\paragraph*{Das Kriterium der Lage} verlangt, dass Strukturen trotz unterschiedlicher Ausprägung, stets die \textbf{gleiche Lagebeziehung} in einem tauglichen Vergleichsexemplar aufweisen. \\
Ein gutes Beispiel für dieses Kriterium sind die Vorderextremitäten der Wirbeltiere. Alle entspringen im Schulterbereich und haben das gleiche Schema: ein Oberarmknochen, zwei Unterarmknochen, mehrere Handwurzelknochen und strahlig angeordnete Mittelhand- Fingerknochen.
\begin{figure}[h!]
    \centering
    \hbox{
        \hspace{-1.5cm}
        \begin{tikzpicture}
            \matrix[column sep = 1cm, row sep = 1cm] (matrix) {
                \node[] (base_plan) {{\includegraphics[height = 4cm]{images/extremeties_01.png}}};
                & \node[] (human) {{\includegraphics[height = 4cm]{images/extremeties_02.png}}};
                & \node[] (turtle) {{\includegraphics[height = 4cm]{images/extremeties_03.png}}};
                & \node[] (dolphin) {{\includegraphics[height = 4cm]{images/extremeties_04.png}}};
                & \node[] (mole) {{\includegraphics[height = 4cm]{images/extremeties_05.png}}};
                & \node[] (penguin) {{\includegraphics[height = 4cm]{images/extremeties_06.png}}};\\
            };

            
            \node[above = 0cm of base_plan] {{\scriptsize \textbf{Grundbauplan}}};
            \node[above = 0.05cm of human] {{\scriptsize Mensch}};
            \node[above = 0.05cm of turtle] {{\scriptsize Meeresschildkröte}};
            \node[above = 0.04cm of dolphin] {{\scriptsize Delfin}};
            \node[above = 0.05cm of mole] {{\scriptsize Maulwurf}};
            \node[above = 0.04cm of penguin] {{\scriptsize Pinguin}};

            \node[] (upper) at (-8.5,1.2) {{\scriptsize Oberarm}};
            \node[] (spoke) at (-8.5,0) {{\scriptsize Speiche}};
            \node[] (elle) at (-8.5,-0.3) {{\scriptsize Elle}};
            \node[] (wrist) at (-8.5,-1) {{\scriptsize Handwurzel}};
            \node[] (middlehand) at (-8.5,-1.3) {{\scriptsize Mittelhand}};
            \node[] (digits) at (-8.5,-1.6) {{\scriptsize Finger}};

            \draw[] (upper) -- (-6.9,1.2);
            \draw[] (spoke) -- (-7, 0);
            \draw[] (elle) -- (-6.8, -0.3);
            \draw[] (wrist) -- (-6.9, -1.15);
            \draw[] (middlehand) -- (-7.1, -1.3);
            \draw[] (digits) -- (-7.2, -1.6);
        \end{tikzpicture}
    }
    \caption{Vorderextremitäten einiger Wirbeltiere}
    \label{fig:extremeties}
\end{figure}

\paragraph*{Das Kriterium der speziellen Qualität der Strukturen} kommt bei Strukturen, die in ihrem \textbf{Feinbau grosse Übereinstimmungen} aufwiesen, zur Anwendung.\\
Vergleicht man den Feinbau von Säugetierzähnen und Haifischschuppen, fällt auf, dass diese gleich aufgebaut und aus dem gleichen Material bestehen. 
\begin{figure}[h]
    \centering
        \begin{tikzpicture} [every node/.style={text centered}]
            \node[](tooth) at (0,0) {\includegraphics[height = 4cm]{images/qualitaet_strukutren_01.png}};
            \node[right = 4cm of tooth] (scale) {\includegraphics[height = 3cm]{images/qualitaet_strukutren_02.png}};

            \node[] (enamel) at (3.5,1.5) {{\small Zahnschmelz}};
            \node[below = 0.2cm of enamel] (dentine) {{\small Zahnbein}};
            \node[below = 1.2 of enamel] (cavity) {{\small Zahnhöhle}};
            \node[below = 1.7cm of enamel] (skin) {{\small Haut}};
            \node[below = 2.2cm of enamel] (basalplate) {{\small Basalplatte}};

            \draw[] (enamel) -- (8.2, 1.3);
            \draw[] (enamel) -- (0, 1.7);

            \draw[] (dentine) -- (7.8, 1);
            \draw[] (dentine) -- (0, 1);

            \draw[] (cavity) -- (7.1, 0);
            \draw[] (cavity) -- (0, 0);

            \draw[] (skin) -- (5.8, -0.4);
            \draw[] (skin) -- (0.1, -1.5);

            \draw[] (basalplate) -- (6.5, -1.1);
        \end{tikzpicture}
    \caption{Homologe Strukturen von Säugetierzahn und Haifischschuppe}
    \label{fir:structure}
\end{figure}

\newpage

\paragraph*{Das Kriterium der Stetigkeit} gilt dann, wenn \textbf{Zwischenformen} der Organe oder Strukturen bekannt sind. Zum Beispiel sind die Gehörknöchelchen der Säuger zu bestimmten Knochen des Knochenfisch-Kieferapparat homolog.
\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \matrix [column sep = 5cm, row sep = 2cm] (matrix) {
            \node[] (bony_fish) {\includegraphics[width = 3cm]{images/qualitaet_stetigkeit_01.png}};
            & \node[] (reptile) {\includegraphics[width = 3cm]{images/qualitaet_stetigkeit_02.png}};\\

            \node[] (link) {\includegraphics[width = 3cm]{images/qualitaet_stetigkeit_03.png}};
            & \node[] (mammal) {\includegraphics[width = 3cm]{images/qualitaet_stetigkeit_04.png}};\\
        };

        \node[right = 0.6cm of bony_fish] (desc_bf_3) {{\scriptsize Quadratum}};
        \node[above = 0.4cm of desc_bf_3] (desc_bf_1) {{\scriptsize Mittelohr}};
        \node[above = 0cm of desc_bf_3] (desc_bf_2) {{\scriptsize Kieferstiel}};
        \node[below = 0.2cm of desc_bf_3] (desc_bf_4) {{\scriptsize Articulare}};
        \node[below right = -0.2cm and 0.3cm of desc_bf_3, text width = 1.7cm] (desc_bf_5) {{\scriptsize primäres Kiefergelenk}};
        \draw[] (desc_bf_1.west) -- (-3, 2.8);
        \draw[] (desc_bf_2.west) -- (-3.1, 2.3);
        \draw[] (desc_bf_3.west) -- (-3.3,2);
        \draw[] (desc_bf_4.west) -- (-3.1,1.5);
        \draw[->, >=stealth] (desc_bf_5.west) -- (desc_bf_3.east);
        \draw[->, >=stealth] (desc_bf_5.west) -- (desc_bf_4.east);

        \node[right = 0.6cm of reptile] (desc_rep_2) {{\scriptsize primäres Kiefergelenk}};
        \node[above = 0.2cm of desc_rep_2] (desc_rep_1) {{\scriptsize Columella}};
        \draw[] (desc_rep_1.west) -- (5.5,2.62);
        \draw[] (desc_rep_2.west) -- (4.85,2.2);

        \node[right = 0.6cm of link] (desc_link_1) {{\scriptsize Steigbügel}};
        \node[below right = 0.2cm and -1.5cm of desc_link_1, text width = 2.8cm] (desc_link_2) {{\scriptsize Kiefergelenk mit Zwischenstellung}};
        \draw[] (desc_link_1.west) -- (-2.9,-2);
        \draw[] (desc_link_2.west) -- (-3.2,-2.4);

        \node[right = 0.6cm of mammal] (desc_ma_2) {{\scriptsize Steigbügel}};
        \node[above = 0cm of desc_ma_2] (desc_ma_1) {{\scriptsize Amboss}};
        \node[below = 0cm of desc_ma_2] (desc_ma_3) {{\scriptsize Hammer}};
        \node[below = 0.4 cm of desc_ma_2] (desc_ma_4) {{\scriptsize sekundäres Kiefergelenk}};
        \draw[] (desc_ma_1.west) -| (5, -2);
        \draw[] (desc_ma_2) -- (5.2, -2);
        \draw[] (desc_ma_3) -- (4.9, -2.2);
        \draw[] (desc_ma_4) -| (5, -2.5);
    \end{tikzpicture}
    \caption{Umwandlung der Kiefergelenkknochen zu den Gehörknöchelchen der Säuger}
    \label{fig:ear_transition}
\end{figure}

\subsection{Rudimente}
Rudimente sind \textbf{rückgebildete Merkmale} die nicht mehr gebraucht werden.\\
Beispiele dafür sind die Reste der Beckenkochen bei Walen, die zurückgebildeten Flügel bei flugunfähigen Vögeln oder folgende Elemente bei Menschen:
\begin{itemize}
    \item Der Blinddarm mit Wurmfortsatz diente als Gärkammer für Bakterien die Cellulose aufspalten.
    \item Das Steissbein als Rest der Schwanzwirbelsäule.
    \item Die Körperbehaarung als Reste des Fells.
    \item Ohrmuskeln
\end{itemize}
Rudimente können auch im Stoffwechsel auftreten. Beispielsweise können alle Primaten kein Vitamin C synthetisieren, da das letzte für die Synthese notwendige Enzym, durch eine Punktmutation bei einem gemeinsamen Vorfahren, defekt wurde.

\subsubsection*{Funktionswechsel}
Viele Rudimente verlieren ihre Funktion nicht vollständig, sondern erhalten eine \textbf{andere Funktion}. Ein solcher Funktionswechsel kann auch Grund dafür sein, dass das Rudiment noch nicht vollständig wegselektioniert wurde.

\subsubsection*{Atavismus}
Atavismus bezeichnet das \textbf{spontane Wiederauftreten} eines \textbf{Rudiments} bei einzelnen \textbf{Individuen}. Es handelt sich dabei um eine Umkehrung der Rudimentierung, weshalb man sie auch Entwicklungsrückschläge nennt.\\
Beispiele beim Menschen wäre eine fellartige Körperbehaarung oder ein kurzer Schwanz am Ende der Wirbelsäule.