Man stelle sich eine positiv geladene Punktladung $q_1$ vor, die sich starr im Raum befindet. 
An jedem Punkt um diese Punktladung herum könnte man nun mit dem Coulomb-Gesetz (\ref{eqn:coulomb}) berechnen, welche Kraft eine 
weitere positive Punktladung $q_2$ dort erfahren würde. An jedem Punkt $(x,y)$ mit der Distanz zu $q_1$ von $r=\sqrt{x^2+y^2}$ lässt 
sich nun ein Vektor der Kraft einzeichnen, die eine andere Ladung $q_2$ an diesem Punkt erfahren würde.\\

\begin{center}
    \begin{tikzpicture}
        \foreach \x in {-4,...,4}{
            \foreach \y in {-4,...,4}{  
                \fill[black!50] (\x,\y) circle (0.8mm);
                 \ifdim \x cm = 0.0 cm
                    \ifdim \y cm = 0.0 cm
                    \else
                        \draw[-Latex,very thick] (\x,\y) -- (0,{\y+(0.8-0.008*(((\x)^2+(\y)^2)*(1+(abs(\x)/abs(\y)))))*sign(\y)});  
                    \fi   
                \else
                    \ifdim \y cm = 0.0 cm
                        \draw[-Latex,very thick] (\x,\y) -- ({\x+(0.8-0.008*(((\x)^2+(\y)^2)*(1+(abs(\y)/abs(\x)))))*sign(\x)},0);
                    \else
                        
                        \draw[-Latex,very thick] (\x,\y) -- ({\x+(0.8-0.008*(((\x)^2+(\y)^2)*(1+(abs(\y)/abs(\x)))))*sign(\x)},{\y+(0.8-0.008*(((\x)^2+(\y)^2)*(1+(abs(\x)/abs(\y)))))*sign(\y)}); 

                    \fi  
                 \fi
             }
         }
        
        \fill[ball color=red!60] (0,0) circle (5mm);
        \node at (0,0) {\Huge{$+$}};
        \node at (0.75,-0.25) {$q_1$};
    \end{tikzpicture}
\end{center}

Was man dabei erhält, nennt man \textbf{Vektorfeld}. Die Funktion $F(x,y)=\vec{F}$ liefert an jedem Punkt einen Vektor zurück, der die dort 
herrschende Kraft beschreibt.\\

Ein \textbf{elektrisches Feld $\vec{E}$} ist also ein Kraftfeld, dass Kräfte auf geladene Teilchen ausüben kann. 
Die \glqq Pfeilrichtung\grqq{} der Vektoren lassen sich verbinden und bilden so die \textbf{elektrischen Feldlinien.}
Je näher die Feldlinien beisammen sind, so höher ist die \textbf{Feldstärke}.\\

Ist die Ladung negativ, so zeigen die Feldlinien in den Mittelpunkt der Ladung. Die Richtung der Kraft, 
die auf eine Punktladung $q_2$ wirkt, ist von dessen Ladung abhängig. Ist die Ladung positiv, so zeigt sie in dieselbe Richtung 
wie das elektrische Feld. Ist sie negativ (hier abgebildet), zeigt sie in die entgegengesetzte Richtung.


\begin{center}
    \begin{tikzpicture}
        \foreach \x in {0,5,...,360}{
           \draw[-Latex,red!30,rotate=\x] (0,0)--(3,0);
           \draw[-Latex,red!30,rotate=\x] (0,0)--(2.5,0);
           \draw[-Latex,red!30,rotate=\x] (0,0)--(2,0);
           \draw[-Latex,red!30,rotate=\x] (0,0)--(1.5,0);
         }
        
        \fill[ball color=red!60] (0,0) circle (5mm);
        \node at (0,0) {\Huge{$+$}};
        \node at (0.75,-0.25) {$q_1$};
        \fill[ball color=blue!60,rotate=45] (2,0) circle (2mm);
        \draw[-Latex,red,rotate=45,very thick] (2,0)--(3,0);
        \draw[Latex-,blue,rotate=45,very thick] (1,0)--(2,0);
        \node at (1.8,1.3) {$q_2$};
        \node at (1.2,2) {$\vec{E}(x,y)$};
        \node at (0.5,1.3) {$\vec{F}(x,y)$};
    \end{tikzpicture}
\end{center}

Das elektrische Feld ist definiert durch:
\begin{equation}\label{eqn:efield}
    \vec{E}=\frac{\vec{F}}{q}
\end{equation}
$q$ steht hier für eine Probeladung, die in das elektrische Feld gebracht wird. In der obigen Abbildung entspricht 
das $q_2$. Stellt man die Gleichung nach $\vec{F}$ um, so ist auch gleich oben genannter Zusammenhang ersichtlich.
Ist das Vorzeichen von $q_2$ negativ, so dreht sich das Vorzeichen von $\vec{E}$ um und die Ladung bewegt sich entgegen 
der Feldlinien-Richtung.\\

Setzt man das Coulomb-Gesetz (\ref{eqn:coulomb}) in (\ref{eqn:efield}) ein, so ergibt sich:
\begin{equation}
    \vec{E}=\frac{\frac{1}{4\pi\varepsilon}\frac{q_1\cancel{q_2}}{r^2}}{\cancel{q_2}}=\frac{1}{4\pi\varepsilon}\frac{q_1}{r^2}=k_e\frac{q_1}{r^2}
\end{equation}

Die Feldstärke ist also proportional zur Ladung und nimmt proportional zum Quadrat der Entfernung zur Punktladung ab.

\paragraph{Beispiel.} Eine freistehende Metallkugel hat eine Ladung von $1.2 \cdot 10^{-6}$ C. Berechnen Sie die Feldstärke 
im Abstand von 0.5 m vom Kugelzentrum. 
\begin{align*}
    \vec{E}&=k_e\frac{q_1}{r^2}\\
    \vec{E}&=8.988 \cdot 10^{9} \frac{\textrm{Nm}}{\textrm{C}^2} \frac{1.2 \cdot 10^{-6} \textrm{C}}{(0.5 \textrm{m})^2}\\
    \underline{\underline{\vec{E}}}&=\underline{\underline{43\;142.4 \frac{\textrm{N}}{\textrm{C}}}}
\end{align*}
