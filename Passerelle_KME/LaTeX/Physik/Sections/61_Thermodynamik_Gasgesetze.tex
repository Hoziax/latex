\clearpage % flushes out all floats
\subsection{Gasgesetze}
\subsubsection{Ideale Gase}

Das Modell der idealen Gase besagt folgendes:
    \begin{itemize}
        \item Die Atome eines idealen Gases sind (ausdehnungslose) Massepunkte.
        \item Diese Atome üben keine (anziehenden oder abstossenden) Kräfte aufeinander aus und können nur mit der Gefässwand zusammenstossen, nicht aber mit anderen Gasteilchen.
        \item Die Atome sind in ständiger Bewegung. Die Stösse dieser Atome gegen die Gefässwand sind elastisch, d.h. die Geschwindigkeit der Atome ändert sich nicht, wohl aber deren Richtung.
    \end{itemize}

Unter nicht allzu hohem Druck und bei nicht allzu tiefen Temperaturen (im Vergleich zum Verflüssigungspunkt) verhalten sich \textit{reale} Gase in sehr guter Näherung wie ideale Gase.

\subsubsubsection{Das Gesetz von Amontons}

Das Gesetz von Amontons gibt den Zusammenhang zwischen dem Druck \textit{p} und der Temperatur \textit{T} bei einem idealen Gas unter Konstanthaltung des Volumens \textit{V} und der Teilchenzahl \textit{N} an. Eine 
    solche Zustandsänderung der Gasmenge bei konstantem Volumen \textit{V} nennt man \textbf{isochor}. Bei einer Erwärmung des Gases erhöht sich also der Druck und bei einer Abkühlung wird er geringer. Die Formel hierzu lautet:
    \begin{equation}
        \frac{p_1}{T_1} = \frac{p_2}{T_2} \label{eq:Gesetz Amontons}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine leere, geschlossene Flasche ($21\si{\degreeCelsius}$, $1.02\si{\bar}$) wird an die Sonne gestellt und erwärmt sich auf $31\si{\degreeCelsius}$. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist der Druck in der Flasche? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \frac{p_1}{T_1} = \frac{p_2}{T_2} \ \Rightarrow \ p_2 = \frac{p_1 \cdot T_2}{T_1} = \frac{1.02\si{\bar} \cdot 304.15\si{\kelvin}}{294.15\si{\kelvin}} = \underline{\underline{1.055\si{\bar}}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} Die Luft in einem Backofen (Länge = $40\si{\cm}$, Breite = $40\si{\cm}$) wird von $20\si{\degreeCelsius}$ auf $200\si{\degreeCelsius}$ erwärmt. Der Druck im Backofen ist am Anfang $1\si{\bar}$. \\\hspace*{\fill}

\textit{Gesucht:}
\begin{enumerate}[label=\alph*)]
    \item Berechnen Sie den Innendruck am Ende des Aufwärmens, wenn keine Luft aus dem Backofen entweicht.
    \item Berechnen Sie das Volumen der Luft die entweicht, wenn der Backofen nicht luftdicht ist.
\end{enumerate}
\newpage

\textit{Lösung:}
\begin{enumerate}[label=\alph*)]
    \item $\frac{p_1}{T_1} = \frac{p_2}{T_2} \ \Rightarrow \ p_2 = \frac{p_1 \cdot T_2}{T_1} = \frac{10^5\si{\pascal} \cdot 475.15\si{\kelvin}}{295.15\si{\kelvin}} = \underline{\underline{1.61 \cdot 10^5\si{\pascal}}}$
    \item \begin{align*}
              p &= \text{konstant} \\
              \frac{V_1}{T_1} = \frac{V_2}{T_2} \ \Rightarrow \ V_2 &= \frac{V_1 \cdot T_2}{T_1} = \frac{0.048\si{\metre}^3 \cdot 475.15\si{\kelvin}}{295.15\si{\kelvin}} = 7.73 \cdot 10^{-2}\si{\metre}^3 \\
              \Delta V &= V_2 - V_1 = 7.73 \cdot 10^{-2}\si{\metre}^3 - 0.048\si{\metre}^3 = \underline{\underline{2.93 \cdot 10^{-2}\si{\metre}^3}}
          \end{align*}
\end{enumerate}

\subparagraph{Beispiel 3}

\textit{Gegeben:} In einem luftdichten, geschlossenen Behälter befindet sich Gas mit einer Temperatur von $20\si{\degreeCelsius}$. \\\hspace*{\fill}

\textit{Gesucht:} Auf welche Temperatur muss man das Gas erwärmen, damit sich der Druck verdreifacht? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \frac{p_1}{T_1} = \frac{p_2}{T_2} \ \Rightarrow \ T_2 = \frac{p_2 \cdot T_1}{p_1} = \frac{3\si{\bar} \cdot 293.15\si{\kelvin}}{1\si{\bar}} = \underline{\underline{879.45\si{\kelvin}}}
\]

\subsubsubsection{Das ideale Gasgesetz bei konstanter Gasmenge (Allgemeines Gasgesetz)}

Die Beziehungen von Boyle-Mariotte und Amontons können zu einem übergeordneten Gesetz vereinigt werden: Zu diesem Zweck wird (in Gedanken) eine konstante Menge eines idealen Gases, bei dem sich alle drei Zustandsgrössen Druck \textit{p}, Volumen \textit{V} und Temperatur \textit{T} ändern: \textit{Anfangszustand}: $p_1$, $V_1$ und $T_1$, \textit{Endzustand} $p_2$, $V_2$ und $T_2$ (Abbildung \ref{fig:Skizze zum Vorgehen}).

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Water left
            \draw[draw=none,fill=MyLightBlue] (-1.3,-1.5) -- (-0.2,-1.5) -- (-0.2,-4.8) -- (-1.3,-4.8) -- cycle node[anchor=center, shift={(5.5mm,-15mm)}, text=MyBlack] {\rotatebox{90}{$p_1,V_1,T_1$}};

            % Piston left
            \draw[fill=MyGrey,draw=MyGrey,thick] (-1.5,1.7) -- (0,1.7) -- (0,1.5) -- (-0.5,1.5) -- (-0.5,-1) -- (-0.2,-1) -- (-0.2,-1.5) -- (-1.3,-1.5) -- (-1.3,-1) -- (-1,-1) -- (-1,1.5) -- (-1.5,1.5) -- cycle;

            % Container left (walls are 0.2 wide) (clockwise from top left)
            \draw[fill=MyLightGrey,draw=MyBlack,thick] (-1.5,1) -- (-1,1) -- (-1,0.8) -- (-1.3,0.8) -- (-1.3,-4.8) -- (-0.2,-4.8) -- (-0.2,0.8) -- (-0.5,0.8) -- (-0.5,1) -- (0,1) -- (0,-5) -- (-1.5,-5) -- cycle;

            % Force arrow
            \draw[color=MyRed,very thick,->] (-0.75,-1) -- (-0.75,-0.25);

            % Connecting arrow left
            \draw[thick,color=MyGrey,fill=MyLightGrey] (0.5,-1.9) -- (0.875,-1.9) -- (0.875,-1.625) -- (3.375,-1.625) -- (3.375,-1.9) -- (3.5,-1.9) -- (3.5,-1.7) -- (3.75,-2) -- (3.5,-2.3) -- (3.5,-2.1) -- (3.375,-2.1) -- (3.375,-2.375) -- (0.875,-2.375) -- (0.875,-2.1) -- (0.5,-2.1) -- cycle node[anchor=south, shift={(16mm,4mm)}, text=MyBlack] {$T$ = konstant} node[anchor=north, shift={(16mm,-6mm)}, text=MyBlack] {isotherm};
            % text
            \node[color=MyBlack] at (2.125,-2) {$p_1 \cdot V_1 = p' \cdot V_2$};


            % Water middle
            \draw[draw=none,fill=MyLightBlue] (4.45,0) -- (5.55,0) -- (5.55,-4.8) -- (4.45,-4.8) -- cycle node[anchor=center, shift={(5.5mm,-22.5mm)}, text=MyBlack] {\rotatebox{90}{\textcolor{MyRed}{$p'$},\textcolor{MyRed}{$V_2$},$T_1$}};

            % Piston middle
            \draw[fill=MyGrey,draw=MyGrey,thick] (4.25,1.7) -- (5.75,1.7) -- (5.75,1.5) -- (5.25,1.5) -- (5.25,0.5) -- (5.55,0.5) -- (5.55,0) -- (4.45,0) -- (4.45,0.5) -- (4.75,0.5) -- (4.75,1.5) -- (4.25,1.5) -- cycle;

            % Container middle (walls are 0.2 wide) (clockwise from top left)
            \draw[fill=MyLightGrey,draw=MyBlack,thick] (4.25,1) -- (4.75,1) -- (4.75,0.8) -- (4.45,0.8) -- (4.45,-4.8) -- (5.55,-4.8) -- (5.55,0.8) -- (5.25,0.8) -- (5.25,1) -- (5.75,1) -- (5.75,-5) -- (4.25,-5) -- cycle;

            % Connecting arrow middle
            \draw[thick,color=MyGrey,fill=MyLightGrey] (6.25,-1.9) -- (6.625,-1.9) -- (6.625,-1.625) -- (9.125,-1.625) -- (9.125,-1.9) -- (9.25,-1.9) -- (9.25,-1.7) -- (9.5,-2) -- (9.25,-2.3) -- (9.25,-2.1) -- (9.125,-2.1) -- (9.125,-2.375) -- (6.625,-2.375) -- (6.625,-2.1) -- (6.25,-2.1) -- cycle node[anchor=south, shift={(16mm,4mm)}, text=MyBlack] {$V$ = konstant} node[anchor=north, shift={(16mm,-6mm)}, text=MyBlack] {isochor};
            % text
            \node[color=MyBlack] at (7.875,-2) {$\frac{p'}{T_1} = \frac{p_2}{T_2}$};


            % Water right
            \draw[draw=none,fill=MyRed!35] (10.2,0) -- (11.3,0) -- (11.3,-4.8) -- (10.2,-4.8) -- cycle node[anchor=center, shift={(5.5mm,-22.5mm)}, text=MyBlack] {\rotatebox{90}{\textcolor{MyRed}{$p_2$},\textcolor{MyRed}{$V_2$},\textcolor{MyRed}{$T_2$}}};

            % Piston right
            \draw[fill=MyGrey,draw=MyGrey,thick] (10,1.7) -- (11.5,1.7) -- (11.5,1.5) -- (11,1.5) -- (11,0.5) -- (11.3,0.5) -- (11.3,0) -- (10.2,0) -- (10.2,0.5) -- (10.5,0.5) -- (10.5,1.5) -- (10,1.5) -- cycle;

            % Container right (walls are 0.2 wide) (clockwise from top left)
            \draw[fill=MyLightGrey,draw=MyBlack,thick] (10,1) -- (10.5,1) -- (10.5,0.8) -- (10.2,0.8) -- (10.2,-4.8) -- (11.3,-4.8) -- (11.3,0.8) -- (11,0.8) -- (11,1) -- (11.5,1) -- (11.5,-5) -- (10,-5) -- cycle;

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Skizze zum Vorgehen.}
    \label{fig:Skizze zum Vorgehen}
\end{figure}
\newpage

In einem ersten Schritt wird dieses Gas bei konstanter Temperatur $T_1$ auf das Endvolumen $V_2$ gebracht. Der Gasdruck bei $V_2$ wird mit $p'$ bezeichnet. Gemäss dem Boyle-Mariotte'schem Gesetz gilt:
    \begin{equation*}
        p_1 \cdot V_1 = p' \cdot V_2 \ \text{oder} \ p' = \frac{p_1 \cdot V_1}{V_2}
    \end{equation*}

In einem zweiten Schritt wird dieses Gas bei konstantem Volumen $V_2$ von $T_1$ auf $T_2$ erwärmt, wobei der Druck von $p'$ auf $p_2$ ansteigt. Gemäss Gesetz von Amontons gilt jetzt:
    \begin{equation*}
        \frac{p'}{T_1} = \frac{p_2}{T_2} \ \Rightarrow \ \frac{p'}{T_1} = \frac{\frac{p_1 \cdot V_1}{V_2}}{T_1} = \frac{p_1 \cdot V_1}{V_2 \cdot T_1} = \frac{p_2}{T_2}
    \end{equation*}

Somit erhält man das Gasgesetz für konstante Gasmengen:
    \begin{equation}
        \frac{p_1 \cdot V_1}{T_1} = \frac{p_2 \cdot V_2}{T_2} \label{eq:Gasgesetz bei konstanter Gasmenge}
    \end{equation}

Die allgemeine Gasgleichung stellt nicht nur einen Zusammenhang zwischen den drei Zustandsgrössen Druck, Volumen und Temperatur her, sondern gibt zusätzlich auch noch eine Beziehung zur Teilchenanzahl an. Da eine 
    Stoffmenge von $n = 1\si{\mole}$ einer Anzahl von $N_A = 6.02 \cdot 10^{23}$ Teilchen entspricht (\glqq Avogadro-Konstante\grqq{}), folgt als weiterer Zusammenhang zwischen der \textit{Stoffmenge n} und der \textit{Teilchenzahl N} [ ]:
    \begin{equation}
        n = \frac{N}{N_A} = \frac{m}{M} \ \si{\mole} \label{eq:Stoffmenge}
    \end{equation}

Die \textit{molare Masse M} $[\frac{\si{\kg}}{\si{\mole}}]$ eines Gases kann anhand der relativen Atommasse u eines Elements aus einem Periodensystem der Elemente abgelesen werden. Die \textit{Gasmasse m} 
    $[\si{\kg}]$ bezeichnet das Gewicht des vorhandenen Gases.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine Luftmatratze wird bei $22\si{\degreeCelsius}$ so aufgepumpt, dass ein Druck von $1.8\si{\bar}$ herrscht. Durch die Sonneneinstrahlung steigt die Temperatur auf $55\si{\degreeCelsius}$. Das 
    Volumen der Luftmatratze kann sich nur noch um 0.2\% vergrössern. \\\hspace*{\fill}

\textit{Gesucht:} Wie hoch ist der Druck in der Luftmatratze bei maximalem Volumen? \\\hspace*{\fill}

\textit{Lösung:}
\[
    \frac{p_1 \cdot V_1}{T_1} = \frac{p_2 \cdot V_2}{T_2} \ \Rightarrow \ p_2 = \frac{p_1 \cdot V_1 \cdot T_2}{V_2 \cdot T_1} = \frac{1.8\si{\bar} \cdot 0.998 \cdot 328.15\si{\kelvin}}{1 \cdot 295.15\si{\kelvin}} = \underline{\underline{1.997\si{\bar}}}
\]
\newpage

\subsubsubsection{Die Universelle Gaskonstante}

Die Formel des allgemeinen Gasgesetzes (\ref{eq:Gasgesetz bei konstanter Gasmenge}) kann auch in folgender Form geschrieben werden:
    \begin{equation*}
        \frac{p_1 \cdot V_1}{T_1} = \frac{p_2 \cdot V_2}{T_2} = \textnormal{konstant} = R
    \end{equation*}

Indem man gewisse Annahmen trifft, lässt sich der Wert der universellen Gaskonstante \textit{R} in guter Annäherung errechnen. Für die nachfolgende Berechnung (\ref{eq:Universelle Gaskonstante}) wird folgendes 
    angenommen:
    \begin{itemize}
        \item Es herrscht Normdruck ($1013\si{\hecto\pascal}$).
        \item Es herrscht Normtemperatur ($273.15\si{\kelvin}$).
        \item Es herrscht Normvolumen ($22.4\si{\litre/\mole}$).
    \end{itemize}

Für eine Gasemenge von einem Mol gilt:
    \begin{equation}
        R = \frac{p \cdot V}{T} = \frac{1.013 \cdot 10^5\si{\pascal} \cdot 22.4\si{\litre/\mole} \cdot 10^{-3}}{273.15\si{\kelvin}} \approx 8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \label{eq:Universelle Gaskonstante}
    \end{equation}

Betrachtet man nun n Mol an Teilchen, so ist die Konstante n-mal so gross. Es gilt somit für beliebige Gasmengen innerhalb eines geschlossenen Systems:
    \begin{equation}
        R = \frac{p \cdot V}{T \cdot n} \ \Rightarrow \ p \cdot V = n \cdot R \cdot T \label{eq:Universelles Gasgesetz}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Die molare Masse von Wasserstoff beträgt $1.008\si{\gram/\mole}$. \\\hspace*{\fill}

\textit{Gesucht:} Berechnen Sie die Masse eines Wasserstoffatoms. \\\hspace*{\fill}

\textit{Lösung:}
\[
    n = \frac{m}{M} \ \Rightarrow \ m = n \cdot M \ \Rightarrow \ m = \frac{N}{N_A} \cdot M = \frac{1\si{\mole}}{6.02 \cdot 10^{23}} \cdot 1.008 \cdot 10^{-3}\si{\kg/\mole} = \underline{\underline{1.67 \cdot 10{-27}\si{\kg}}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} Die Temperatur beträgt $40\si{\degreeCelsius}$ und der Druck $2.5\si{\bar}$. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist das Volumen von $400\si{\gram}$ Sauerstoff ($O_2$)? \\\hspace*{\fill}

\textit{Lösung:}
\[
    p \cdot V = n \cdot R \cdot T  \ \Rightarrow \ V = \frac{n \cdot R \cdot T}{p} = \frac{\frac{400\si{\gram}}{32\si{\gram/\mole}} \cdot 8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \cdot 313.15\si{\kelvin}}{2.5 \cdot 1.013 \cdot 10^5\si{\pascal}} = \underline{\underline{0.128\si{\metre}^3}}
\]
\newpage

\subparagraph{Beispiel 3}

\textit{Gegeben:} Der Druck in einer Sauerstoffflasche beträgt $160\si{\bar}$ bei $20.2\si{\degreeCelsius}$. \\\hspace*{\fill}

\textit{Gesucht:}
\begin{enumerate}[label=\alph*)]
    \item Wie gross ist die Dichte vom Sauerstoffgas?
    \item Welche Gasmenge ist in der zwei Liter fassenden Flasche enthalten?
    \item Ohne Temperaturänderung wird so viel Gas entnommen, bis der Druck auf $150\si{\bar}$ sinkt. Wie viel Gas in Kilogramm wurde entnommen?
    \item Auf welchen Betrag steigt der Druck, wenn die Temperatur anschliessend auf $50\si{\degreeCelsius}$ steigt?
\end{enumerate}

\textit{Lösung:}
\begin{enumerate}[label=\alph*)]
    \item \begin{alignat*}{2}
            n = \frac{m}{M} \ &\Rightarrow \ m = n \cdot M && \\
            p \cdot V = n \cdot R \cdot T  \ &\Rightarrow \ V = \frac{n \cdot R \cdot T}{p} && \\
            &\Rightarrow \ \rho = \frac{m}{V} = \frac{M \cdot n}{\frac{n \cdot R \cdot T}{p}} &&= \frac{M \cdot p}{R \cdot T} \\
            & &&= \frac{0.032\si{\kg/\mole} \cdot 160 \cdot 1.013 \cdot 10^5\si{\pascal}}{8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \cdot 293.35\si{\kelvin}} \\
            & &&= \underline{\underline{212.76\si{\kg/\metre}^3}}
          \end{alignat*}
    \item \begin{equation*}
              n = \frac{m}{M} = \frac{\rho \cdot V}{M} = \frac{212.76\si{\kg/\metre}^3 \cdot 2 \cdot 10^{-3}\si{\metre}^3}{0.032\si{\kg/\mole}} = \underline{\underline{13.3\si{\mole}}}
          \end{equation*}
    \item \begin{align*}
              \frac{m}{V} = \frac{M \cdot n}{\frac{n \cdot R \cdot T}{p}} = \frac{M \cdot p}{R \cdot T} \ \Rightarrow \ m &= \frac{M \cdot p \cdot V}{R \cdot T} \\
              &= \frac{0.032\si{\kg/\mole} \cdot 150 \cdot 1.013 \cdot 10^5\si{\pascal} \cdot 2 \cdot 10^{-3}\si{\metre}^3}{8.31\frac{\si{\joule}}{\si{\mole}} \cdot 293.35\si{\kelvin}} = 0.399\si{\kg} \\
              \Delta m &= \frac{212.76\si{\kg/\metre}^3 \cdot 2}{10^3\si{\metre}^3} - 0.399\si{\kg} = \underline{\underline{26.6 \cdot 10^{-3}\si{\kg}}}
          \end{align*}
    \item \begin{equation*}
              m = \frac{M \cdot p \cdot V}{R \cdot T} \ \Rightarrow \ p = \frac{m \cdot R \cdot T}{M \cdot V} = \frac{0.399\si{\kg} \cdot 8.31\frac{\si{\joule}}{\si{\mole}} \cdot 323.15\si{\kelvin}}{0.032\si{\kg/\mole} \cdot 2 \cdot 10^{-3}\si{\metre}^3 \cdot 1.013 \cdot 10^5\si{\pascal}} = \underline{\underline{165.27\si{\bar}}}
          \end{equation*}
\end{enumerate}
\newpage

\subsubsubsection{Die Boltzmann-Konstante}

Möchte man nun unabhängig des Gases und seiner Stoffmenge argumentieren, dann kann das universelle Gasgesetz auch in anderer Form geschrieben werden:
    \begin{equation}
        p \cdot V = N \cdot K_B \cdot T \label{eq:Zustandsgleichung idealer Gase}
    \end{equation}

Hierbei bezeichnet $K_B$ die \textit{Boltzmann-Konstante}, die folgendermassen berechnet wird:
    \begin{equation}
        K_B = \frac{R}{N_A} = 1.381 \cdot 10^{-23} \frac{\si{\joule}}{\si{\kelvin}} \label{eq:Boltzmann-Konstante}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine Gasflasche, deren Innendruck $200\si{\bar}$ beträgt, fasst 20 Liter. Die Temperatur ist $19\si{\degreeCelsius}$. \\\hspace*{\fill}

\textit{Gesucht:} Wie viel Gramm Sauerstoff ($O_2$) befinden sich in der Gasflasche? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    p \cdot V = n \cdot R \cdot T  \ &\Rightarrow \ n = \frac{p \cdot V}{R \cdot T} = \frac{200 \cdot 1.013 \cdot 10^5 \cdot 20 \cdot 10^{-3}\si{\metre}^3}{8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \cdot 285.15\si{\kelvin}} = 174.79\si{\mole} \\
    &\rightarrow \ 174.79\si{\mole} \cdot \underbrace{32 \cdot 10^{-3}\si{\kg/\mole}}_{M_{O_2} = 32\si{\gram/\mole}} = \underline{\underline{5.59\si{\kg}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Eine Neonröhre (Länge = $1\si{\metre}$, Durchmesser = $3\si{\cm}$) weist bei $20\si{\degreeCelsius}$ einen Innendruck von $0.0025\si{\bar}$ auf. \\\hspace*{\fill}

\textit{Gesucht:} Wie viele Gasmoleküle befinden sich in der Neonröhre? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    V_{\textnormal{Röhre}} &= \pi \cdot r^2 \cdot h = \pi \cdot (1.5 \cdot 10^{-2}\si{\metre})^2 \cdot 1\si{\metre} = 7.069 \cdot 10^{-4}\si{\metre}^3 \\
    p \cdot V &= N \cdot K_B \cdot T \\
    &\Rightarrow \ N = \frac{p \cdot V}{K_B \cdot T} = \frac{0.0025 \cdot 1.013 \cdot 10^5 \cdot 7.069 \cdot 10^{-4}\si{\metre}^3}{1.381 \cdot 10^{-23} \frac{\si{\joule}}{\si{\kelvin}} \cdot 293.15\si{\kelvin}} = \underline{\underline{4.42 \cdot 10^{19} \ \textnormal{Teilchen}}}
\end{align*}