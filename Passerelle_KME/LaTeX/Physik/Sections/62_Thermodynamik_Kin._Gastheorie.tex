\clearpage % flushes out all floats
\subsection{Die Kinetische Gastheorie}

Viele reale Gase können unter Standardbedingungen in guter Näherung mittels des Modells der idealen Gase beschrieben werden: Die Anziehungskräfte zwischen den einzelnen Teilchen ist meist vernachlässigbar gering, 
    und ebenso ist das Volumen der einzelnen Teilchen klein im Vergleich zum Gesamtvolumen des Gases. Geht man von diesen Annahmen aus, so kann ein Gas als grosse Anzahl einzelner Atome oder Moleküle \textit{N} angesehen werden, die eine Masse \textit{m} besitzen und sich mit einheitlicher Geschwindigkeit $\overrightarrow{v}$ in unterschiedliche Richtungen bewegen, wobei die einzelnen Bewegungsrichtungen und Geschwindigkeiten statistisch gleich verteilt (isotrop) sind. Ebenfalls sind die Stösse der Atome auf die Gefässwand vollkommen elastisch.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
            
            % Angles
            \draw[thick,draw=MyBlack] (6.725,1) arc (270:210.1:0.5) node[anchor=west, shift={(0.25mm,-0.25mm)}, text=MyBlack] {$\alpha$};
            \draw[thick,draw=MyGreen] (6.475,0.5) arc (180:90:0.25);
            \draw[draw=none,fill=MyGreen] (6.6,0.6) circle (0.025); % circle
            
            % Lines
            \draw[very thick,draw=MyGreen] (5,0.5) -- (6.725,1.5) node[anchor=north, shift={(2mm,5mm)}, text=MyBlack] {$A$};
            \draw[very thick,draw=MyGreen] (5,0.5) -- (6.725,-0.5) node[anchor=north, shift={(2mm,0mm)}, text=MyBlack] {$B$};
            \draw[very thick,draw=MyGreen] (5,0.5) -- (6.725,0.5) node[anchor=west, shift={(2mm,0mm)}, text=MyBlack] {$M$};
            \draw[very thick,draw=MyGrey] (6,1.5) -- (6.725,1.5) -- (6.725,-0.5);
            \draw[very thick,draw=MyBlack] (5,0.5) -- (3.275,1.5) node[anchor=north, shift={(5mm,-5mm)}, text=MyBlack] {$r$};
            
            % Mass point
            \draw[draw=none,fill=MyOrange] (5,1.5) circle (0.1) node[anchor=south, shift={(0mm,0mm)}, text=MyOrange] {$m$};

            % Circle
            \draw[very thick,draw=MyBlack,fill=none] (5,0.5) circle (2);
            
            % Arrow
            \draw[thick,->,draw=MyBlack] (5,1.5) -- (5.75,1.5) node[anchor=north, shift={(-4.5mm,0mm)}, text=MyBlack] {$\overrightarrow{v}$};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Vereinfachtes Gasmodell in einem kreisförmigen Behälter.}
\end{figure}

In einem einfachen Modell kann man von einem einzelnen Gasteilchen ausgehen, das sich in einem kreisförmigen Behälter befindet. Die Treffpunkte des Gasteilchens mit der Gefässwand werden mit der Kreismitte verbunden. Die daraus resultierende Strecke $\overline{AB}$ bezeichnet die freie Weglänge $l_F$, die das Teilchen ohne Interaktion passiert. \\\hspace*{\fill}

Mit den folgenden paar Überlegungen gelangen wir zur erwünschten Schlussgleichung:
    \begin{itemize}
        \item Das freie Wegstück $l_F$ ist gleich $2 \cdot r \cdot \cos(\alpha)$
        \item Die vom Gasteilchen zurückgelegte Strecke in einem Zeitintervall $\Delta t$ ist $s = \overrightarrow{v} \cdot \Delta t$
        \item Die Anzahl Wandstösse, die das Gasteilchen in einem Zeitintervall $\Delta t$ erfährt, ist $z = \frac{\Delta s}{l_F} = \frac{\overrightarrow{v} \cdot \Delta t}{2 \cdot r \cdot \cos(\alpha)}$
        \item Die Kraft, die das Gasteilchen auf die Gefässwand ausübt, ist $\overrightarrow{F} = \frac{m \cdot v^2}{r}$
        \item Die Gesamtkraft, die alle Gasteilchen auf die Gefässwand ausüben, ist demnach $\overrightarrow{F}_{Tot} = N \cdot \frac{m \cdot v^2}{r}$
        \item Die Gesamtkraft auf die Gefässwand pro Kugeloberfläche lautet $\frac{\overrightarrow{F}}{A} = \frac{N \cdot m \cdot v^2}{r} / 4 \cdot \pi r^2$
    \end{itemize}
    \vspace{1em}

Die Formel des letzten Gedanken lässt sich folgendermassen vereinfachen:
    \begin{align*}
        \frac{\overrightarrow{F}}{A} = \frac{N \cdot m \cdot v^2}{r} / 4\pi r^2 &= \frac{N \cdot m \cdot v^2}{\underbrace{4 \cdot \pi r^3}_{3V}} \\
                                                                                &= \frac{N \cdot m \cdot v^2}{3V}
    \end{align*}
    \newpage

Da $p = \frac{\overrightarrow{F}}{A}$ gilt, folgt:
    \begin{align}
        p &= \frac{N \cdot m \cdot v^2}{3V} \nonumber \\
        &\Rightarrow \ p \cdot V = \frac{N \cdot m \cdot v^2}{3} \nonumber \\
        &\Rightarrow \ p \cdot V = \frac{2}{3} \cdot N \cdot \frac{m \cdot v^2}{2} \label{eq:Gesamtformel kinetische Gastheorie}
    \end{align}

Der Ausdruck $\frac{m \cdot v^2}{2}$ ist gleich der kinetischen Energie, die das Gasteilchen besitzt:
    \begin{equation}
        E_{Kin} = \frac{m \cdot v^2}{2} \label{eq:Kinetische Energie kin. Gastheorie}
    \end{equation}

Nach dem universellen Gasgesetz (\ref{eq:Universelles Gasgesetz}) lässt sich nun folgender Zusammenhang herstellen:
    \begin{equation}
        p \cdot V = n \cdot R \cdot T = N \cdot K_B \cdot T = \frac{2}{3} \cdot N \cdot \frac{m \cdot v^2}{2}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Stickstoffmoleküle befinden sich in Luft bei $20\si{\degreeCelsius}$ und Normdruck. \\\hspace*{\fill}

\textit{Gesucht:} Berechnen Sie die Geschwindigkeit der Moleküle. \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \frac{2}{3} \cdot N \cdot \frac{m \cdot v^2}{2} &= n \cdot R \cdot T \ \textnormal{(Anmerkung: $N \cdot m = m_g$, $n = \frac{m_g}{M}$)} \\
    \frac{v^2}{3} &= \frac{R \cdot T}{M} \\
    v &= \sqrt{\frac{3 \cdot R \cdot T}{M}} = \sqrt{\frac{3 \cdot 8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \cdot 293.15\si{\kelvin}}{28.02 \cdot 10^{-3}\si{\kg/\mole}}} = \underline{\underline{510.71\si{\metre/\second}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} In einem Behälter mit einem Volumen von 22.4 Liter befinden sich $6.02 \cdot 10^{23}$ Teilchen eines idealen Gases, dessen Druck $1.01\si{\bar}$ beträgt. \\\hspace*{\fill}

\textit{Gesucht:}
\begin{enumerate}[label=\alph*)]
    \item Wie gross ist die kinetische Energie eines dieser Teilchen?
    \item Wie gross ist die Geschwindigkeit der Teilchen, wenn die Gasmasse \textit{m} $6.64 \cdot 10^{-27}\si{\kg}$ beträgt?
\end{enumerate}
\newpage

\textit{Lösung:}
\begin{enumerate}[label=\alph*)]
    \item \begin{align*}
              p \cdot V = \frac{2}{3} \cdot N \cdot \frac{m \cdot v^2}{2} \ &\Rightarrow \ \frac{m \cdot v^2}{2} = \frac{p \cdot V}{\frac{2}{3} \cdot N} \\
                                                                            &= \frac{1.01 \cdot 1.013 \cdot 10^5 \cdot 22.4 \cdot 10^{-3}\si{\metre}^3}{\frac{2}{3} \cdot 6.02 \cdot 10^{23}} = \underline{\underline{5.71 \cdot 10^{-21}\si{\joule}}}
          \end{align*}
    \item \[
              E_{Kin} = \frac{m \cdot v^2}{2} \ \Rightarrow \ v = \sqrt{\frac{2 \cdot E_{Kin}}{m}} = \sqrt{\frac{2 \cdot 5.71 \cdot 10^{-21}\si{\joule}}{6.64 \cdot 10^{-27}\si{\kg}}} = \underline{\underline{1'311.5\si{\metre/\second}}}
          \]
\end{enumerate}

\subparagraph{Beispiel 3}

\textit{Gegeben:} Die Temperatur beträgt $27\si{\degreeCelsius}$. \\\hspace*{\fill}

\textit{Gesucht:} Wie hoch ist die Geschwindigkeit von Sauerstoff- und Wasserstoffmolekülen? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \textnormal{$O_2$:} \ v &= \sqrt{\frac{3 \cdot R \cdot T}{M}} = \sqrt{\frac{3 \cdot 8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \cdot 300.15\si{\kelvin}}{32 \cdot 10^{-3}\si{\kg/\mole}}} = \underline{\underline{483.57\si{\metre/\second}}} \\
    \textnormal{$H_2$:} \ v &= \sqrt{\frac{3 \cdot R \cdot T}{M}} = \sqrt{\frac{3 \cdot 8.31\frac{\si{\joule}}{\si{\mole} \cdot \si{\kelvin}} \cdot 300.15\si{\kelvin}}{2.02 \cdot 10^{-3}\si{\kg/\mole}}} = \underline{\underline{1'924.66\si{\metre/\second}}}
\end{align*}

\subsubsection{Die Maxwell-Boltzmann-Verteilung}

Die Maxwell-Boltzmann-Verteilung oder auch Maxwell'sche Geschwindigkeitsverteilung ist eine Wahrscheinlichkeitsverteilung der Geschwindigkeiten der Gasteilchen eines beliebigen Gases, wobei die Verteilung aller 
    vorkommenden Geschwindigkeiten durch eine Verteilungsfunktion $p(v)$ angegeben werden kann. Eine solche Funktion gibt an, mit welcher Häufigkeit eine Anzahl $\Delta N$ aller \textit{N} Gasteilchen eine Geschwindigkeit zwischen $v$ und $v + \Delta v$ besitzt. \\\hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}
            % Source: https://latex.org/forum/viewtopic.php?t=15531&start=10
            % Parameters:
            \def\kB{1.3806488e-23} % boltzmann constant
            \def\hundredK{100} % 100 K
            \def\rt{300} % ~ room temperature
            \def\thousandK{1000} % 100 K
            \def\Beta#1{1/(\kB*#1)}
            \def\amu{1.660538921e-27} % atomar mass unit in kg
            \def\HMass{2*\amu} % Hydrogen
            % Plot:
            \begin{axis}[
                domain  = 0:8000,
                xlabel  = {$v \ [\si{\metre/\second}]$},
                ylabel  = $p(v)$,
            ]
            \addplot[color=MyRed]
                {sqrt(2/pi)*(\HMass*\Beta{\hundredK})^(3/2)*x^2*exp(-.5*\HMass*\Beta{\hundredK}*x^2)};
            \addplot[color=MyYellow]
                {sqrt(2/pi)*(\HMass*\Beta{\rt})^(3/2)*x^2*exp(-.5*\HMass*\Beta{\rt}*x^2)};
            \addplot[color=MyLightBlue]
                {sqrt(2/pi)*(\HMass*\Beta{\thousandK})^(3/2)*x^2*exp(-.5*\HMass*\Beta{\thousandK}*x^2)};
            \legend{\SI{100}{\kelvin},\SI{300}{\kelvin},\SI{1000}{\kelvin}}
            \end{axis}

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Geschwindigkeitsverteilung von Gasteilchen am Beispiel von Wasserstoff.}
    \label{fig:Geschwindigkeitsverteilung von Gasteilchen am Beispiel von Wasserstoff}
\end{figure}
\newpage

Bei Gasteilchen entspricht die Häufigkeitsverteilung nicht einer Normalverteilung, sondern einer so genannten Maxwell'schen Verteilung. In der Abbildung 
    \ref{fig:Geschwindigkeitsverteilung von Gasteilchen am Beispiel von Wasserstoff} ist diese Verteilungsfunktion für drei verschiedene Temperaturen dargestellt. Die verschiedenen Teilchengeschwindigkeiten können auch mit einer Box, die das Gas und ein Loch enthält und einem Sensor gemessen werden; die Teilchen, die näher zur Box gemessen werden, sind langsamer als diejenigen, die von der Box weiter weg sind (Abbildung \ref{fig:Unterschiedliche Teilchengeschwindigkeit anhand der Distanz zur Gasquelle}).

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Ground
            \draw[color=MyBlack,thick] (2,-0.5) -- (8,-0.5);
            \foreach \x in {0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75,3,3.25,3.5,3.75,4,4.75,5,5.25,5.5,5.75}
                \draw[color=MyBlack,thick] (\x + 2,-0.5) -- (\x + 1.75,-0.75);

            % Box
            \draw[draw=none,fill=MyGrey] (3.5,-0.5) rectangle (4,0);
            \draw[very thick,draw=MyBlack,fill=none] (4.5,0.85) -- (4.5,1.5) -- (3,1.5) -- (3,0) -- (4.5,0) -- (4.5,0.65);

            % Gas particles
            \draw plot [only marks, mark=*, mark size=0.5, domain=3:4.5, samples=400] (\x,{rnd*1.5}); % domain = x_1:x_2 (from which x coordinate to which x coordinate); rnd returns values between 0 and 1
            \draw[thick,->,MyBlack] (2.25,0.75) -- (3,0.75) node[anchor=east, shift={(-7.5mm,0mm)}, text=MyBlack] {Gas};

            % text
            \node[draw,thin,color=MyBlack,fill=white] at (3.75,0.75) {$\bar{v}$};
            
            % Sensor
            \draw[draw=none,fill=MyGreen] (5.5,-0.5) rectangle (7,-0.375);
            \draw[thick,->,MyBlack] (6.25,-1) -- (6.25,-0.5) node[anchor=north, shift={(0mm,-5mm)}, text=MyBlack] {Sensor};

            % Lines
            \draw[MyRed,very thick] (4.5,0.75) .. controls (5.25,0.5) .. (5.75,-0.375) node[anchor=south, shift={(5mm,8mm)}] {langsam}; % langsam
            \draw[MyGrey,very thick] (4.5,0.75) .. controls (5.5,0.5) .. (6.25,-0.375) node[anchor=south, shift={(5mm,4mm)}] {mittel}; % mittel
            \draw[MyLightBlue,very thick] (4.5,0.75) .. controls (5.75,0.5) .. (6.75,-0.375) node[anchor=south, shift={(6mm,-1mm)}] {schnell}; % schnell

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Unterschiedliche Teilchengeschwindigkeit anhand der Distanz zur Gasquelle.}
    \label{fig:Unterschiedliche Teilchengeschwindigkeit anhand der Distanz zur Gasquelle}
\end{figure}

Die Geschwindigkeit, die dem Maximum der jeweiligen Kurve entspricht, wird als wahrscheinlichste Geschwindigkeit $\hat{v}$ bezeichnet; sie stimmt \textbf{nicht} mit der mittleren Geschwindigkeit $\bar{v}$ aller 
    Geschwindigkeitswerte überein. \\\hspace*{\fill}

Die Maxwell-Boltzmann-Verteilung erklärt beispielsweise den Prozess der Verdunstung. So kann feuchte Wäsche bei Temperaturen von $20\si{\degreeCelsius}$ trocknen, da es in dieser Verteilungskurve einen geringen 
    Anteil von Molekülen mit der erforderlich hohen Geschwindigkeit gibt, welche sich aus dem Flüssigkeitsverband lösen können. Es wird also auch bei niedrigen Temperaturen immer einige Moleküle geben, die schnell genug sind, die Anziehungskräfte durch ihre Nachbarn zu überwinden und vom flüssigen oder festen Aggregatzustand in den gasförmigen Aggregatzustand überzugehen, was man als Verdampfung bzw. Sublimation bezeichnet. Umgekehrt gibt es aber auch unter den vergleichsweise schnellen Teilchen des Gases immer einige, die keine ausreichende Geschwindigkeit besitzen und daher wieder vom gasförmigen in den flüssigen oder festen Aggregatzustand wechseln, was man als Kondensation bzw. Resublimation bezeichnet.