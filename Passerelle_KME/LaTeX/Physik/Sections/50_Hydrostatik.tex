\clearpage % flushes out all floats
\section{Hydrostatik}

Die Hydrostatik ist die Lehre vom Gleichgewicht in ruhenden Flüssigkeiten (Fluiden) bei Einwirkung äusserer Kräfte. Die grundlegende Aufgabe der Hydrostatik ist die Bestimmung der Druckverteilung in einer ruhenden 
    Flüssigkeit. Die Kohäsionskräfte einer Flüssigkeit hängt mit der Coulomb-Wechselwirkung zusammen. Diese Kräfte haben eine sehr kurze Reichweite und nehmen pro Meter mit $r^{-8}$ ab. \\\hspace*{\fill}

Flüssigkeiten lassen sich durch mechanische Belastung (fast) nicht zusammenpressen -- sie sind also inkompressibel. Dies gilt sowohl für von aussen wirkende Kräfte wie auch für den sogenannten 
    \glqq Schweredruck\grqq{}, der sich aus dem Eigengewicht der Flüssigkeit ergibt.

\subsection{Der Druck}

Übt man mit einem Kolben eine Kraft auf eine Flüssigkeit aus, die sich in einem geschlossenen Behälter befindet, so bleibt das Volumen der Flüssigkeit unverändert; allerdings baut sich im Inneren der Flüssigkeit ein 
    \glqq Gegendruck\grqq{} auf, der die von aussen einwirkende Kraft ausgleicht. Als Druck bezeichnet man allgemein das Verhältnis einer Kraft $\overrightarrow{F}$ zu einer Fläche \textit{A}, auf welche die Kraft senkrecht einwirkt:
    \begin{equation}
        p = \frac{\overrightarrow{F}}{A} \ [\si{\pascal}] = \left[\frac{\si{\newton}}{\si{\metre}^2}\right] \label{eq:Der Druck}
    \end{equation}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Syringe
            \draw[fill=MyLightBlue,draw=none] (3.55,1.5) -- (6.5,1.5) -- (6.5,0.75) -- (7.5,0.75) -- (7.5,0.25) -- (6.5,0.25) -- (6.5,-0.5) -- (3.55,-0.5) -- cycle node[anchor=center, shift={(15mm,-10mm)}] {p}; % Water in syringe

            \draw[fill=MyGrey,draw=none] (0.5,1.5) -- (0.5,0.6) -- (3.45,0.6) -- (3.45,1.5) -- (3.55,1.5) -- (3.55,-0.5) -- (3.45,-0.5) -- (3.45,0.4) -- (0.5,0.4) -- (0.5,-0.5) -- (0.35,-0.5) -- (0.35,1.5) -- cycle; % Pressing thingy

            \draw[MyBlack,thick] (2.5,1.5) -- (6.5,1.5) -- (6.5,0.75) -- (7.5,0.75) -- (7.5,0.25) -- (6.5,0.25) -- (6.5,-0.5) -- (2.5,-0.5) -- cycle; % Container
            \draw[MyBlack,very thick] (2.5,2) -- (2.5,-1); % Vertical line

            % Arrow pushing force
            \draw[color=MyRed,very thick,->] (0.5,0.5) -- (1.5,0.5) node[anchor=center, shift={(-3mm,3mm)}, text=MyRed] {$\overrightarrow{F}$};

            % Cross-section
            \draw[MyBlack,thick,dashed] (6.5,1.5) -- (9,1.5);
            \draw[MyBlack,thick,dashed] (6.5,-0.5) -- (9,-0.5);
            \draw[MyBlack,thick] (9,0.5) circle (1) node[anchor=center, shift={(0mm,0mm)}] {\large A};

            % Pressure arrows
            \draw[color=MyRed,thick,->] (4.5,0.5) -- (3.75,0.5); % West
            \draw[color=MyRed,thick,->] (4.75,0.75) -- (4,1.25); % Northwest
            \draw[color=MyRed,thick,->] (5,0.75) -- (5,1.35); % North
            \draw[color=MyRed,thick,->] (5.25,0.75) -- (6,1.25); % Northeast
            \draw[color=MyRed,thick,->] (5.5,0.5) -- (6.25,0.5); % East
            \draw[color=MyRed,thick,->] (4.75,0.25) -- (4,-0.25); % Southwest
            \draw[color=MyRed,thick,->] (5,0.25) -- (5,-0.35); % South
            \draw[color=MyRed,thick,->] (5.25,0.25) -- (6,-0.25); % Southeast
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Druck in Flüssigkeiten am Beispiel des Kolbendrucks in einer Spritze.}
\end{figure}

Im Gegensatz zur Kraft ist der Druck keine gerichtete physikalische Grösse, d.h. sie ist von ihrer Richtung unabhängig. Man spricht deshalb von einem Skalar. \\\hspace*{\fill}

Im Vergleich zum Luftdruck ist 1 Pascal ein sehr kleiner Druck. Neben der Basiseinheit Pascal (Pa) benutzt man für den Druck aus praktischen Gründen auch die Einheit bar. Auf Meereshöhe beträgt der Luftdruck 
    ungefähr 1 bar:
    \begin{equation}
        1\si{\bar} = 10^5\si{\pascal} \label{eq:Bar zu Pascal}
    \end{equation}

Genauer beträgt der Luftdruck auf Meereshöhe $1.013 \cdot 10^5\si{\pascal}$, was einer Atmosphäre (1atm) entspricht:
    \begin{equation}
        1\textrm{atm} = 1.013 \cdot 10^5\si{\pascal} = 1.013\si{\bar} \label{eq:Atmosphäre zu Pascal zu Bar}
    \end{equation}

In der Medizin wird die Druckeinheit $\si{\mmHg}$, auch als Millimeter-Quecksilbersäule bezeichnet, zur Angabe des Druckes von Körperflüssigkeiten angewendet. Physikalisch beschreibt $\si{\mmHg}$ den statischen 
    Druck, der von einer Quecksilbersäule von einen Millimeter Höhe erzeugt wird:
    \begin{equation}
        760\si{\mm} \textrm{ Hg-Säule} = 760\textrm{Torr} = 1\textrm{atm} \label{eq:Hg-Säule zu Torr zu Atmosphäre}
    \end{equation}
    \newpage

\subsubsection{Der Satz von Pascal}

Das Pascal'sche Prinzip sagt aus, dass sich Druck in ruhenden, gewichtslosen Flüssigkeiten und Gasen in alle Raumrichtungen ausbreitet und im Fluidum in alle Richtungen stets rechtwinklig auf jede Begrenzungsfläche 
    wirkt. Der Druck ist in der Flüssigkeit überall gleich gross.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Panzer ($m = 42.4\si{\tonne}$) steht auf zwei Panzerketten von insgesamt $4.6\si{\metre}^2$. Eine Stecknadel ($A = 0.01\si{\mm}^2$) drückt mit $5\si{\newton}$ auf eine Unterlage. \\\hspace*{\fill}

\textit{Gesucht:} Vergleichen Sie den ausgeübten Druck zwischen dem Panzer und der Stecknadel. \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    p_{Panzer} &= \frac{\overrightarrow{F}}{A} = \frac{m \cdot a}{A} = \frac{m \cdot g}{A} = \frac{42'400\si{\kilogram} \cdot 9.81\si{\metre/\second}^2}{4.6\si{\metre}^2} = \underline{\underline{90'422.61\si{\pascal}}} \\
    p_{Stecknadel} &= \frac{\overrightarrow{F}}{A} = \frac{5\si{\newton}}{10^{-8}\si{\metre}^2} = \underline{\underline{500'000'000\si{\pascal}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Wasserhahn mit einer Fläche von $2\si{\cm}^2$ wird per Daumen mit $40\si{\newton}$ zugehalten. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist der Druck in der Wasserleitung? \\\hspace*{\fill}

\textit{Lösung:}
\[
    p = \frac{\overrightarrow{F}}{A} = \frac{40\si{\newton}}{2 \cdot 10^{-4}\si{\metre}^2} = \underline{\underline{200'000\si{\pascal}}}
\]

\subparagraph{Beispiel 3}

\textit{Gegeben:} Das Navigerät wird per Saugnapf an die Windschutzscheibe angebracht. \\\hspace*{\fill}

\textit{Gesucht:} Welche Kraft übt die Luft auf den runden Gummisaugnapf ($r = 3\si{\cm}$) aus? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    A &= \pi \cdot r^2 = 2.83 \cdot 10^{-3}\si{\metre}^2 \\
    p &= \frac{\overrightarrow{F}}{A} \ \Rightarrow \ \overrightarrow{F} = A \cdot p = 2.83 \cdot 10^{-3}\si{\metre}^2 \cdot 1.013 \cdot 10^5\si{\pascal} = \underline{\underline{286.42\si{\newton}}}
\end{align*}
\newpage

\subsection{Der Hydraulische Hebel}

Eine wichtige technische Anwendung der Inkompressibilität und gleichmässigen Druckausbreitung in Flüssigkeiten sind hydraulische Anlagen. Hierbei wird auf der einen Seite eine (verhältnismässig) schwache Kraft auf 
    einen Kolben mit möglichst geringem Durchmesser ausgeübt. Der durch den Kolben ausgeübte Druck $p = \frac{\overrightarrow{F}_1}{A_1}$, also das Verhältnis aus der ausgeübten Kraft $\overrightarrow{F}_1$ und der Querschnittsfläche $A_1$ des Kolbens, entspricht näherungsweise dem insgesamt in der Flüssigkeit wirkenden Druck, da gegenüber ihm der Schweredruck meist vernachlässigbar klein ist.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Water
            \draw[fill=MyLightBlue,draw=none] (1.3,0) -- (1.3,-0.7) -- (7.7,-0.7) -- (7.7,0.5) -- (9.8,0.5) -- (9.8,-1.5) -- (0.2,-1.5) -- (0.2,0) -- cycle;

            % Piston left
            \draw[fill=MyGrey,draw=MyGrey,thick] (0,1.7) -- (1.5,1.7) -- (1.5,1.5) -- (1,1.5) -- (1,0.3) -- (1.3,0.3) -- (1.3,0) -- (0.2,0) -- (0.2,0.3) -- (0.5,0.3) -- (0.5,1.5) -- (0,1.5) -- cycle;
            \node[color=MyBlack] at (2.5,1.25) {Druckkolben}; % text

            % Piston right
            \draw[fill=MyGrey,draw=MyGrey,thick] (7.5,2.2) -- (10,2.2) -- (10,2) -- (9.25,2) -- (9.25,0.8) -- (9.8,0.8) -- (9.8,0.5) -- (7.7,0.5) -- (7.7,0.8) -- (8.25,0.8) -- (8.25,2) -- (7.5,2) -- cycle;
            \node[color=MyBlack] at (6.9,1.25) {Presskolben}; % text

            % Basin (top part) (walls are 0.2 wide) (clockwise from top left)
            \draw[fill=MyLightGrey,draw=MyBlack,thick] (1,1) -- (1.5,1) -- (1.5,-0.5) -- (7.5,-0.5) -- (7.5,1) -- (8.25,1) -- (8.25,0.8) -- (7.7,0.8) -- (7.7,-0.7) -- (1.3,-0.7) -- (1.3,0.8) -- (1,0.8) -- cycle;

            % Basin (bottom part) (walls are 0.2 wide) (anticlockwise from top left)
            \draw[fill=MyLightGrey,draw=MyBlack,thick] (0.5,1) -- (0,1) -- (0,-1.7) -- (10,-1.7) -- (10,1) -- (9.25,1) -- (9.25,0.8) -- (9.8,0.8) -- (9.8,-1.5) -- (0.2,-1.5) -- (0.2,0.8) -- (0.5,0.8) -- cycle;

            % Arrows
            \draw[color=MyRed,very thick,->] (0.75,3) -- (0.75,1.8) node[anchor=west, shift={(1.5mm,4.5mm)}, text=MyRed] {$\overrightarrow{F}_1$}; % Left
            \draw[color=MyGreen,very thick,->] (8.75,2.3) -- (8.75,3.5) node[anchor=west, shift={(1.5mm,-7.5mm)}, text=MyGreen] {$\overrightarrow{F}_2$}; % Right

            % Delta s (left side)
            \draw[MyOrange,very thick,dashed] (-0.5,0.5) -- (1.3,0.5); % top
            \draw[MyOrange,very thick,dashed] (-0.5,0) -- (1.3,0); % bottom
            \draw[MyOrange,very thick] (-1,0.5) -- (-0.6,0.5); % difference thingy top
            \draw[MyOrange,very thick] (-0.8,0.5) -- (-0.8,0) node[anchor=center, shift={(-5mm,2.5mm)}, text=MyOrange] {$\Delta s_1$}; % difference thingy vertical
            \draw[MyOrange,very thick] (-1,0) -- (-0.6,0); % difference thingy bottom

            % Delta s (right side)
            \draw[MyOrange,very thick,dashed] (7.7,0.5) -- (10.5,0.5); % top
            \draw[MyOrange,very thick,dashed] (7.7,0.25) -- (10.5,0.25); % bottom
            \draw[MyOrange,very thick] (10.6,0.5) -- (11,0.5); % difference thingy top
            \draw[MyOrange,very thick] (10.8,0.5) -- (10.8,0.25) node[anchor=center, shift={(6mm,1.25mm)}, text=MyOrange] {$\Delta s_2$}; % difference thingy vertical
            \draw[MyOrange,very thick] (10.6,0.25) -- (11,0.25); % difference thingy bottom

            % Surface area
            \draw[MyViolet,very thick,<->] (0.2,-0.5) -- (1.3,-0.5) node[anchor=center, shift={(-5.5mm,-3mm)}, text=MyViolet] {$A_1$}; % left
            \draw[MyViolet,very thick,<->] (7.7,-0.5) -- (9.8,-0.5) node[anchor=center, shift={(-10.5mm,-3mm)}, text=MyViolet] {$A_2$}; % right
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{\glqq Gleichgewicht\grqq{} in einer hydraulischen Anlage.}
\end{figure}

Auf der anderen Seite der hydraulischen Anlage befindet sich ein zweiter Kolben mit (verhältnismässig) grosser Querschnittsfläche $A_2$. Da innerhalb der Flüssigkeit der Kolbendruck 
    $p = \frac{\overrightarrow{F}}{A}$ an allen Stellen gleich gross ist, wird beim Hineinpressen des kleinen Kolbens eine Kraft auf den grossen Kolben ausgeübt, die um das Verhältnis der Kolbenflächen verstärkt ist:
    \begin{equation}
        p = \frac{\overrightarrow{F}_1}{A_1} = \frac{\overrightarrow{F}_2}{A_2} \ \Rightarrow \ \frac{\overrightarrow{F}_1}{\overrightarrow{F}_2} = \frac{A_1}{A_2} \label{eq:Hydraulischer Hebel}
    \end{equation}

Eine hydraulische Anlage stellt somit ebenfalls einen Kraftwandler dar. Die goldene Regel der Mechanik gilt unverändert: Um den grossen Kolben um eine Höhe $s_2$ anzuheben, muss man den kleinen Kolben um eine 
    entsprechend längere Wegstrecke $s_1$ bewegen. Es gilt mit der obigen Gleichung (\ref{eq:Hydraulischer Hebel}) die \textbf{Energieerhaltung}:
    \begin{equation}
        \Delta W_1 = \overrightarrow{F}_1 \cdot \Delta s_1 = \overrightarrow{F}_2 \cdot \Delta s_2 = \Delta W_2 \label{eq:Hydraulischer Hebel Energieerhaltung}
    \end{equation}

Analog zur Energieerhaltung besteht bei der \textbf{Volumenerhaltung} ein Zusammenhang zwischen dem Volumen \textit{V}, der Fläche \textit{A} und den zurückgelegten Strecken:
    \begin{equation}
        \Delta V_1 = A_1 \cdot \Delta s_1 = A_2 \cdot \Delta s_2 = \Delta V_2 \label{eq:Hydraulischer Hebel Volumenerhaltung}
    \end{equation}
    \newpage

\subsection{Der Hydrostatische Druck (Schwere-/Gewichtsdruck)}

Der hydrostatische Druck (auch Schweredruck genannt) ist jener Druck, welcher sich innerhalb eines ruhenden Fluids durch den Einfluss der Schwerkraft (Gewichtskraft) einstellt. Der hydrostatische Druck betrachtet 
    nur ruhende statische Fluide oder Gase, keine Fluidströmungen. Es handelt sich hierbei um den senkrechten Druck in einer ruhenden Flüssigkeit. \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Outer cylinder
            \draw[draw=none,fill=MyLightBlue] (3.5,2) -- (6.5,2) -- (6.5,-1.5) -- (3.5,-1.5) -- cycle node[anchor=center, shift={(7mm,-30mm)}, text=MyBlack, align=left] {$\rho_{Med.,1}$};
            \draw[color=MyDarkBlue,fill=MyLightBlue] (5,2) ellipse (1.5 and 0.25);
            %% Ellipses
            \draw[color=MyBlack,very thick] (5,3) ellipse (1.5 and 0.25); % Top
            \draw [color=MyBlack,very thick,fill=MyLightBlue] plot [smooth, tension=1.75] coordinates {(3.5,-1.5) (5,-1.75) (6.5,-1.5)}; % Bottom (full line)
            \draw [color=MyBlack,very thick,dashed] plot [smooth, tension=1.75] coordinates {(3.5,-1.5) (5,-1.25) (6.5,-1.5)}; % Bottom (dotted line)

            %% Lines
            \draw[color=MyBlack,very thick] (3.5,3) -- (3.5,-1.5); % Left
            \draw[color=MyBlack,very thick] (6.5,3) -- (6.5,-1.5); % Right

            % Inner cylinder
            \draw[draw=none,fill=MyBrown!75] (4,2) -- (6,2) -- (6,-0.5) -- (4,-0.5) -- cycle node[anchor=center, shift={(10mm,-10mm)}, text=MyBlack] {$\rho_{Medium,2}$};
            %% Ellipses
            \draw[color=MyGrey,very thick,fill=MyBrown!75] (5,2) ellipse (1 and 0.25) node[anchor=center, shift={(0mm,0mm)}, text=MyBlack] {$A$}; % Top
            \draw [color=MyGrey,very thick,fill=MyBrown!75] plot [smooth, tension=1.75] coordinates {(4,-0.5) (5,-0.75) (6,-0.5)}; % Bottom (full line)
            \draw [color=MyGrey,very thick,dashed] plot [smooth, tension=1.75] coordinates {(4,-0.5) (5,-0.25) (6,-0.5)}; % Bottom (dotted line)

            %% Lines
            \draw[color=MyGrey,very thick] (4,2) -- (4,-0.5); % Left
            \draw[color=MyGrey,very thick] (6,2) -- (6,-0.5); % Right

            % Arrows (Forces)
            \draw[color=MyYellow,very thick,->] (5,0.5) -- (5,-0.5) node[anchor=west, shift={(0mm,6mm)}, text=MyYellow] {$\overrightarrow{F}_G$};
            \draw[color=MyRed,very thick,->] (5,-1.5) -- (5,-0.5) node[anchor=west, shift={(0mm,-5mm)}, text=MyRed] {$\overrightarrow{F}$};
            \draw [color=MyGrey,very thick] plot [smooth, tension=1.75] coordinates {(4,-0.5) (5,-0.75) (6,-0.5)}; % Bottom (full line) -> to give illusion the arrow points underneath the cylinder

            % Height difference
            \draw[MyOrange,very thick,dashed] (2.2,2) -- (4,2); % top
            \draw[MyOrange,very thick,dashed] (2.2,-0.5) -- (4,-0.5); % bottom
            \draw[MyOrange,very thick] (1.7,2) -- (2.1,2) node[anchor=center, shift={(-10mm,0mm)}, text=MyBlack] {$p = 0$}; % difference thingy top
            \draw[MyOrange,very thick] (1.9,2) -- (1.9,-0.5) node[anchor=center, shift={(-2mm,12.5mm)}, text=MyOrange] {$h$}; % difference thingy vertical
            \draw[MyOrange,very thick] (1.7,-0.5) -- (2.1,-0.5) node[anchor=center, shift={(-7mm,0mm)}, text=MyBlack] {$p$}; % difference thingy bottom
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Schweredruck einer Flüssigkeitssäule.}
\end{figure}

Der hydrostatische Druck entspricht dem Druck an einem gewählten Messpunkt, der in einer ruhenden Flüssigkeit durch ihre eigene Masse erzeugt wird. Er entsteht also dadurch, dass das Eigengewicht der höheren 
    Flüssigkeitsschichten auf die unteren Schichten drückt. Es gilt:
    \begin{align}
        \overrightarrow{F}_{Boden} &= \overrightarrow{F}_G \nonumber \\
        \Rightarrow \ p \cdot A &= m \cdot g \nonumber \\
        \Rightarrow \ p(h) &= \frac{m \cdot g}{A} = \frac{\rho \cdot V \cdot g}{A} = \frac{\rho \cdot A \cdot h \cdot g}{A} = \rho \cdot g \cdot h \label{eq:Hydrostatischer Druck Flüssigkeitsdruck}
    \end{align}
    Der Schweredruck in einer Flüssigkeit hängt, da der Ortsfaktor \textit{g} konstant ist, nur von der Dichte der Flüssigkeit und der Höhe der Flüssigkeitssäule ab. \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style
    
            % Axis
            \foreach \i in {-0.5,0.5,1.5} {\node at (\i + 4.5,2) {$+$};} % axis ticks x axis
            \foreach \i in {1.5,1,0.5,0} {\node at (3.5,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3.5,2) -- (6.5,2) node[anchor=west, shift={(0,2.5mm)}] {$\textrm{Druck} \, [\si{\bar}]$}; % x axis
            \draw[thick] (3.5,2.1) -- (3.5,1.9) node[anchor=south, shift={(0,2mm)}] {0}; % x axis #0
            \draw[thick] (4.5,2.1) -- (4.5,1.9) node[anchor=south, shift={(0,2mm)}] {1}; % x axis #1
            \draw[thick] (5.5,2.1) -- (5.5,1.9) node[anchor=south, shift={(0,2mm)}] {2}; % x axis #2

            \draw[thick,->] (3.5,2) -- (3.5,-1) node[anchor=east, shift={(0,-2.5mm)}] {$\textrm{Tiefe} \, [\si{\metre}]$}; % y axis
            \draw[thick] (3.4,-0.5) -- (3.6,-0.5) node[anchor=east, shift={(-2.5mm,0)}] {10}; % y axis #10

            % Line
            \draw[MyLightBlue,very thick] (4.5,2) -- (5.5,-0.5);
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{\glqq Handgelenk-mal-Pi-Regel\grqq{} für den Druck in der Tiefe.}
\end{figure}

Als Faustregel sollte man sich merken, dass pro zehn Meter Tiefe der hydrostatische Druck um ein bar zunimmt. \\\hspace*{\fill}

Um den Gesamtdruck, der auf einer Wassersäule herrscht, zu berechnen, addiert man noch den Luftdruck dazu:
    \begin{equation}
        p_{Tot} = p_{Luft} + p(h) = p_{Luft} + \rho_{Fluid} \cdot g \cdot h \label{eq:Hydrostatischer Druck Gesamtdruck}
    \end{equation}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Eine Wurst hat einen Überdruck von 0.05 bar. Man steckt eine Gabel in die Wurst. \\\hspace*{\fill}

\textit{Gesucht:} Wie weit spritzt das Wasser maximal auf? \\\hspace*{\fill}

\textit{Lösung:}
\[
    p(h) = \rho \cdot g \cdot h \ \Rightarrow \ h = \frac{p}{\rho \cdot g} = \frac{0.05 \cdot 10^5\si{\pascal}}{1'000\si{\kg/\metre}^3 \cdot 9.81\si{\metre/\second}^2} = \underline{\underline{0.51\si{\metre}}}
\]

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Taucher befindet sich in einer Wassertiefe von 30 Meter. \\\hspace*{\fill}

\textit{Gesucht:} Mit welchem Druck muss die Atemluft aus der Sauerstoffflasche bereitgestellt werden? \\\hspace*{\fill}

\textit{Lösung:}
\[
    p_{Flasche} = p_{Luft} + (30 \cdot 1\si{\bar}) = 1\si{\bar} + 3\si{\bar} = \underline{\underline{4\si{\bar}}}
\]
\newpage

\subsubsection{Das Hydrostatische Paradoxon}

Die Zunahme des Schweredrucks mit der Tiefe ist unabhängig von der Form der darüber liegenden Wassersäule. Dieses als \glqq hydrostatisches Paradoxon\grqq{} bekannte Prinzip kann man beispielsweise mittels einer Anordnung von unterschiedlich geformten Glasgefässen zeigen (siehe Abbildung \ref{fig:Hydrostatisches Paradoxon}), die untereinander durch Wasserleitungen verbunden sind (\glqq kommunizierende Gefässe\grqq{}).

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Water
            \draw[fill=MyLightBlue,draw=none] (5.5,1.5) -- (6,1.5) -- (5.55,1.3) -- (5.505,1.4) -- cycle; % Fixes hole on left side
            \draw[fill=MyLightBlue,draw=none] (7,1.5) -- (7.5,1.5) -- (7.495,1.4) -- (7.45,1.3) -- cycle; % Fixes hole on right side
            \draw[fill=MyLightBlue,draw=none] (2,1.5) -- (2,0) -- (3.5,0) [rounded corners=20pt]  -- (4,1) [sharp corners] -- (3.5,1.5) -- (4.5,1.5) [rounded corners=20pt] -- (5,1) [sharp corners] -- (4.5,0) -- (6,0) [rounded corners=10pt] -- (6,0.75) -- (5.35,1.5) [sharp corners] -- (5.5,1.5) -- (7.5,1.5) [rounded corners=10pt] -- (7.65,1.5) -- (7,0.75) [sharp corners] -- (7,0) -- (9,0) -- (9,1.5) -- (10,1.5) -- (10,-1) -- (0,-1) -- (0,1.5) -- cycle;
            
            % Container
            \draw[fill=none,draw=MyBlack,thick] (0,3) -- (0,-1) -- (10,-1) -- (10,3);
            \draw[fill=none,draw=MyBlack,thick] (2,3) -- (2,0) -- (3.5,0) [rounded corners=20pt]  -- (4,1) -- (3,2) -- (3.5,3);
            \draw[fill=none,draw=MyBlack,thick] [rounded corners=20pt] (4.5,3) -- (4,2) -- (5,1) [sharp corners] -- (4.5,0) -- (6,0) [rounded corners=10pt] -- (6,0.75) -- (5.35,1.5) -- (6,2.25) -- (6,3);
            \draw[fill=none,draw=MyBlack,thick] [rounded corners=10pt] (7,3) -- (7,2.25) -- (7.65,1.5) -- (7,0.75) [sharp corners] -- (7,0) -- (9,0) -- (9,3);
    
        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Gleiche Wasserniveaus bei verbundenen Gefässen unterschiedlicher Form.}
    \label{fig:Hydrostatisches Paradoxon}
\end{figure}

Bei einer solchen Anordnung ist das Wasserniveau in allen Gefässen gleich hoch –- ein höheres Wasserniveau in einem der Gefässe hätte einen höheren Wasserdruck auf die unteren Wasserschichten in diesem Gefäss zur 
    Folge, wodurch wiederum Wasser von dort in die übrigen Gefässe gepresst würde. Der Bodendruck ist also derselbe, solange die Grundfläche und die Wasserhöhe dieselbe ist, egal welche Form das Gefäss hat. \\\hspace*{\fill}

Dieses Prinzip wird zum Beispiel bei der Konstruktion von Siphons als \glqq Geruchstopper\grqq{} verwendet (beispielsweise an Waschbecken). \hspace*{\fill}
\newpage

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:}
\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Cylinder
            \draw[draw=none,fill=MyBrown!75] (4,-2) -- (6,-2) -- (6,-4.5) -- (4,-4.5) -- cycle;
            %% Ellipses
            \draw[color=MyGrey,very thick,fill=MyBrown!75] (5,-2) ellipse (1 and 0.25); % Top
            \draw [color=MyGrey,very thick,fill=MyBrown!75] plot [smooth, tension=1.75] coordinates {(4,-4.5) (5,-4.75) (6,-4.5)}; % Bottom (full line)
            \draw [color=MyGrey,very thick,dashed] plot [smooth, tension=1.75] coordinates {(4,-4.5) (5,-4.25) (6,-4.5)}; % Bottom (dotted line)
            %% Lines
            \draw[color=MyGrey,very thick] (4,-2) -- (4,-4.5); % Left
            \draw[color=MyGrey,very thick] (6,-2) -- (6,-4.5) node[anchor=center, shift={(9mm,12.5mm)}, text=MyBlack] {$V = 500\si{\liter}$}; % Right

            % Pipe
            \draw[draw=none,fill=MyBrown!75] (4.5,0) -- (5.5,0) -- (5.5,-1.875) -- (4.5,-1.875) -- cycle;
            %% Ellipses
            \draw[color=MyGrey,very thick,fill=MyBrown!75] (5,0) ellipse (0.5 and 0.125); % Top
            \draw [color=MyGrey,very thick,fill=none] plot [smooth, tension=1.75] coordinates {(4.5,-1.875) (5,-2) (5.5,-1.875)}; % Bottom (full line)
            \draw [color=MyGrey,very thick,dashed] plot [smooth, tension=1.75] coordinates {(4.5,-1.875) (5,-1.75) (5.5,-1.875)}; % Bottom (dotted line)
            %% Lines
            \draw[color=MyGrey,very thick] (4.5,0) -- (4.5,-1.875); % Left
            \draw[color=MyGrey,very thick] (5.5,0) -- (5.5,-1.875); % Right
            %% Area
            \draw[MyBlack,thick] (3.5,0.25) -- (5,0) node[anchor=center, shift={(-25mm,2.5mm)}, text=MyBlack] {$A = 0.9\si{\cm}^2$};

            % Height difference
            \draw[MyOrange,very thick,dashed] (2.2,-2) -- (4,-2); % top
            \draw[MyOrange,very thick,dashed] (2.2,-4.5) -- (4,-4.5); % bottom
            \draw[MyOrange,very thick] (1.7,-2) -- (2.1,-2); % difference thingy top
            \draw[MyOrange,very thick] (1.9,-2) -- (1.9,-4.5) node[anchor=center, shift={(-8mm,12.5mm)}, text=MyOrange] {$h = 1\si{\metre}$}; % difference thingy vertical
            \draw[MyOrange,very thick] (1.7,-4.5) -- (2.1,-4.5); % difference thingy bottom

            % Glas
            \draw[draw=none,fill=MyLightBlue,thick] (5.6,0.9) -- (6,0.5) -- (5.75,0.25) -- (5.35,0.65); % water
            \draw[MyBlack,thick] (5.5,1) -- (6,0.5) -- (5.75,0.25) -- (5.25,0.75) node[anchor=center, shift={(20mm,0mm)}, text=MyBlack] {$V_{Glas} = 0.25\si{\liter}$}; % container
    
        \end{tikzpicture}
    \end{center}

\end{figure}

\textit{Gesucht:} Wie viele Weingläser müssen reingeschüttet werden, damit der Druck im Fass sich vervierfacht? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    p &= \rho \cdot g \cdot h \\
    4p &= \rho \cdot g \cdot 4h \text{ (4h = insgesamte Höhe)} \\
    &\Rightarrow \ 3h \text{ (Höhe Rohr)} \cdot A = 3\si{\metre} \cdot 9 \cdot 10^{-5} = 2.7 \cdot 10^{-4}\si{\metre}^3 \\
    &\Rightarrow \ \frac{2.7 \cdot 10^{-4}\si{\metre}^3}{2.5 \cdot 10^{-4}\si{\metre}^3} = \underline{\underline{1.08\text{ Gläser}}}
\end{align*}
\newpage

\subsection{Die Auftriebskraft}

Befindet sich ein Körper in einer Flüssigkeit oder in einem Gas, so verringert sich scheinbar seine Gewichtskraft. Diese Erscheinung wird als statischer Auftrieb bezeichnet, die der Gewichtskraft entgegen gerichtete 
    Kraft als Auftriebskraft. Für einen Körper, der sich in einer Flüssigkeit oder in einem Gas befindet, gilt: Die auf einen Körper wirkende Auftriebskraft ist gleich der Gewichtskraft der von ihm verdrängten Flüssigkeits- bzw. Gasmenge (archimedisches Gesetz). \\\hspace*{\fill}

Die Auftriebskraft ist die gemessene Kraft auf den Körper vor dem eintauchen minus die gemessene Kraft auf den Körper nach dem Eintauchen. Sie trägt zu einer \textbf{scheinbaren} Verminderung von 
    $\overrightarrow{F}_G$ bei. Faktoren, die zum Auftrieb des Körpers beisteuern, sind das Volumen \textit{V} und die Dichte $\rho$ des Körpers:
    \begin{equation}
        \overrightarrow{F}_A = \rho_{\textrm{Flüssigkeit}} \cdot g \cdot V_{\textrm{Eingetauchtes Volumen}} \label{eq:Auftriebskraft}
    \end{equation}

\subsubsection{Der Satz des Archimedes}

Die Auftriebskraft an einem Körper ist umso grösser, je grösser sein eingetauchtes Volumen ist. Gleichzeitig verdrängt der eingetauchte Körper mit seinem Volumen ein genauso grosses Volumen Flüssigkeit. Die 
    verdrängte Flüssigkeit hat eine bestimmte Gewichtskraft, die umso grösser ist, je grösser ihr Volumen ist. Damit ergibt sich ein Zusammenhang zwischen der Auftriebskraft an einem Körper und der Gewichtskraft der von ihm verdrängten Flüssigkeit bzw. des verdrängten Gases. Dieser Zusammenhang wird durch das archimedische Gesetz beschrieben. Es lautet:
    \begin{quote}
        Für einen Körper, der sich in einer Flüssigkeit oder in einem Gas befindet, gilt: Die auf den Körper wirkende Auftriebskraft $\overrightarrow{F}_A$ ist \textbf{gleich} der Gewichtskraft $\overrightarrow{F}_G$ der von ihm verdrängten Flüssigkeits- bzw. Gasmenge.        
    \end{quote}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein U-Boot, welches Zylinderförmig, 35 Meter lang ist und einen Durchmesser von fünf Meter hat, ist in Wasser ($\rho_{Wasser} = 10^3\si{\kg/\metre}^3$) eingetaucht. \\\hspace*{\fill}

\textit{Gesucht:} Wie gross ist die Auftriebskraft? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    \overrightarrow{F}_A &= \rho \cdot g \cdot V \\
                         &= 10^3\si{\kg/\metre}^3 \cdot 9.81\si{\metre/\second}^2 \cdot \pi \cdot r^2 \cdot h \\
                         &= 10^3\si{\kg/\metre}^3 \cdot 9.81\si{\metre/\second}^2 \cdot \pi \cdot (2.5\si{\metre})^2 \cdot 35\si{\metre} \\
                         &= \underline{\underline{6.7\si{\MN}}}
\end{align*}
\newpage

\subsubsection{Der Zusammenhang zwischen \texorpdfstring{$F_A$ und $F_G$}{FA und FG}}

Die Bedeutung des Auftriebs besteht vor allem darin, dass von ihm abhängig ist, ob ein Körper in Wasser oder in Luft sinkt, schwebt, steigt oder schwimmt. Das spielt in vielen Bereichen von Natur, Technik und Alltag 
    eine Rolle, was die nachfolgenden Beispiele zeigen. \\\hspace*{\fill}

Für Fische ist es günstig, wenn sie unter Wasser schweben, also weder von selbst nach oben steigen noch nach unten sinken. Das wird durch die Schwimmblase erreicht. In ihr befindet sich soviel Luft, dass die 
    Auftriebskraft genauso gross ist wie die Gewichtskraft. Bei Schiffen muss die Konstruktion so erfolgen, dass sie sicher schwimmen. Damit das der Fall ist, muss die Auftriebskraft so gross sein, dass das Schiff auch bei voller Beladung genügend weit aus dem Wasser ragt. \\\hspace*{\fill}

Es besteht folgender Zusammenhang zwischen der Auftriebskraft und der Gewichtskraft:
    \begin{center}
        \begin{tabular}{c|c|c|c} % Alignment of Columns
        Sinken & Steigen & Schweben & Schwimmen \\
        \hline
        \rule{0pt}{3.5ex} % Spacing
        $\overrightarrow{F}_A < \overrightarrow{F}_G$ & $\overrightarrow{F}_A > \overrightarrow{F}_G$ & $\overrightarrow{F}_A = \overrightarrow{F}_G$ & $\overrightarrow{F}_A = \overrightarrow{F}_G$ \\
        \end{tabular}
    \end{center}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Kalksteinblock ($\rho = 3'100\si{\kg/\metre}^3$) wird als Würfel mit einer Seitenlänge von 40 Zentimeter idealisiert. Er liegt in einem Teich zwei Zentimeter unter der 
    Wasseroberfläche. \hspace*{\fill}
    \begin{figure}[H]
        \begin{center}
            \begin{tikzpicture}[>=latex] % arrow style
    
                % Water
                \draw[color=MyDarkBlue,fill=MyLightBlue] (3.5,1) -- (3.5,-1) -- (6.5,-1) -- (6.5,1) -- cycle;
        
                % Container
                \draw[thick,color=MyBlack] (3.5,2) -- (3.5,-1) -- (6.5,-1) -- (6.5,2);
    
                % Bottom Block
                \draw[thick,color=MyBrown] (4.75,-0.5) -- (4.75,-1) -- (5.25,-1) -- (5.25,-0.5) -- cycle; % front
                \draw[thick,color=MyBrown] (5,-0.25) -- (5.5,-0.25) -- (5.5,-0.75); % back (full line)
                \draw[thick,color=MyBrown,dashed] (5,-0.25) -- (5,-0.75) -- (5.5,-0.75); % back (dotted line)
                \draw[thick,color=MyBrown] (4.75,-0.5) -- (5,-0.25); % diagonal left
                \draw[thick,color=MyBrown] (5.25,-0.5) -- (5.5,-0.25); % diagonal top right
                \draw[thick,color=MyBrown] (5.25,-1) -- (5.5,-0.75); % diagonal bottom right
                \draw[thick,color=MyBrown,dashed] (4.75,-1) -- (5,-0.75); % diagonal dashed
    
                % Top Block
                \draw[thick,color=MyBrown,densely dashed] (4.75,2.5) -- (4.75,2) -- (5.25,2) -- (5.25,2.5) -- cycle; % front
                \draw[thick,color=MyBrown,densely dashed] (5,2.75) -- (5.5,2.75) -- (5.5,2.25) -- (5,2.25) -- cycle; % back
                \draw[thick,color=MyBrown,densely dashed] (4.75,2.5) -- (5,2.75); % diagonal left
                \draw[thick,color=MyBrown,densely dashed] (5.25,2.5) -- (5.5,2.75); % diagonal top right
                \draw[thick,color=MyBrown,densely dashed] (5.25,2) -- (5.5,2.25); % diagonal bottom right
                \draw[thick,color=MyBrown,densely dashed] (4.75,2) -- (5,2.25); % diagonal bottom left
    
                % Height difference
                \draw[MyOrange,thick] (2.75,2) -- (3.5,2); % top
                \draw[MyOrange,thick] (2.75,1) -- (3.5,1); % bottom
                \draw[MyOrange,thick,<->] (3,1) -- (3,2) node[anchor=east, shift={(-2mm,-5mm)}] {$1\si{\metre}$}; % top arrow
                \draw[MyOrange,thick,<->] (3,-1) -- (3,1) node[anchor=east, shift={(-2mm,-10mm)}] {$2\si{\metre}$}; % bottom arrow
    
            \end{tikzpicture}
        \end{center}
    
    \end{figure}

\textit{Gesucht:}
\begin{enumerate}
    \item Welche Arbeit ist zu verrichten, um den Block im Ganzen um drei Meter zu heben?
    \item Zeichnen Sie das Kraft-Weg-Diagramm zu Teilaufgabe 1.
\end{enumerate}
\newpage

\textit{Lösung:}
\begin{enumerate}
    \item \begin{align*}
              \overrightarrow{F}_A &= \rho_{Wasser} \cdot g \cdot V = 1'000\si{\kg/\metre}^3 \cdot 9.81\si{\metre/\second}^2 \cdot 0.064\si{\metre}^3 = 627.84\si{\newton} \\
              \overrightarrow{F}_G &= m \cdot g = \rho_{Kalk} \cdot V \cdot g = 3'100\si{\kg/\metre}^3 \cdot 0.064\si{\metre}^3 \cdot 9.81\si{\metre/\second}^2 = 1'946.304\si{\newton} \\
              W_{Hub,Wasser} &= \overrightarrow{F} \cdot s = (\overrightarrow{F}_G - \overrightarrow{F}_A) \cdot s = (1'946.304\si{\newton} - 627.84\si{\newton}) \cdot 2\si{\metre} = 2'636.928\si{\joule} \\
              W_{Hub,Luft} &= \overrightarrow{F} \cdot s = \overrightarrow{F}_G \cdot s = 1'946.304\si{\newton} \cdot 1\si{\metre} = 1'946.304\si{\joule} \\
              W_{Tot} &= W_{Hub,Wasser} + W_{Hub,Luft} = 2'636.928\si{\joule} + 1'946.304\si{\joule} = \underline{\underline{4'583.23\si{\joule}}}
          \end{align*}
     \item \leavevmode\vadjust{\vspace{-\baselineskip}}
                \begin{flushleft}
                    \begin{tikzpicture}[>=latex] % arrow style

                        % Axis
                        \draw[thick,->] (3.5,2) -- (6.5,2) node[anchor=west, shift={(0,2.5mm)}] {$\overrightarrow{F} \, [\si{\newton}]$}; % x axis
                        \draw[thick] (3.5,2.1) -- (3.5,1.9) node[anchor=south, shift={(0,2mm)}] {0}; % x axis #0
                        \draw[thick,->] (3.5,2) -- (3.5,-2) node[anchor=east, shift={(0,-2.5mm)}] {$s \, [\si{\metre}]$}; % y axis
                        \draw[thick] (3.4,0) -- (3.6,0) node[anchor=east, shift={(-2mm,0mm)}] {2}; % x axis #2

                        % Line
                        \draw[MyLightBlue,very thick] (5.5,2) -- (5.5,0) -- (4.5,-1) -- (4.5,-2);
                        \draw[MyLightBlue,very thick] (5.3,0) -- (5.7,0) node[anchor=west, shift={(0,-2.5mm)}] {Start Wasser-Luft-Übergang};
                        \draw[MyLightBlue,very thick] (4.3,-1) -- (4.7,-1) node[anchor=west, shift={(0,-2.5mm)}] {Ende Wasser-Luft-Übergang};
                
                    \end{tikzpicture}
                \end{flushleft}
\end{enumerate}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Holzquader mit einer Höhe von 40 Millimeter sinkt in Benzin ($\rho = 0.72\si{\gram/\cm^3}$) um $\Delta h = 8\si{\mm}$ tiefer ein als in Wasser. \\\hspace*{\fill}

\textit{Gesucht:} Welche Dichte hat der Holzquader? \\\hspace*{\fill}

\textit{Lösung:}
\begin{itemize}
    \item In Benzin: $\overrightarrow{F}_{A,Benzin} = \overrightarrow{F}_{G,Benzin}$
    \item In Wasser: $\overrightarrow{F}_{A,Wasser} = \overrightarrow{F}_{G,Wasser}$
\end{itemize}
\begin{align*}
    &\Rightarrow \ \overrightarrow{F}_{A,Benzin} = \overrightarrow{F}_{A,Wasser} \\
    &\Rightarrow \ \rho_{Benzin} \cdot g \cdot V_{Eintauch,Benzin} = \rho_{Wasser} \cdot g \cdot V_{Eintauch,Wasser} \\
    &\Rightarrow \ \rho_B \cdot A \cdot h_{Eintauch,B} = \rho_W \cdot A \cdot h_{Eintauch,W} \\
    &\Rightarrow \ \rho_B \cdot (h_{Eintauch,W} + 0.008\si{\metre}) = \rho_W \cdot h_{Eintauch,W} \\
    &\Rightarrow \ h_{Eintauch,W} = \frac{\rho_B \cdot 0.008\si{\metre}}{\rho_W - \rho_B} = \frac{720\si{\kg/\metre^3} \cdot 0.008\si{\metre}}{1'000\si{\kg/\metre^3} - 720\si{\kg/\metre^3}} = 20.57 \cdot 10^{-3}\si{\metre}^3
\end{align*}
\begin{align*}
    \overrightarrow{F}_{A,W} = \overrightarrow{F}_G \ &\Rightarrow \ \rho_W \cdot g \cdot V_W = m \cdot g \\
                                                      &\Rightarrow \ \rho_W \cdot A \cdot h_{Eintauch,W} = \rho_H \cdot A \cdot h \\
                                                      &\Rightarrow \ \rho_H = \frac{\rho_W \cdot h_{Eintauch,W}}{h} \\
                                                      &\Rightarrow \ \rho_H = \frac{1'000\si{\kg/\metre^3} \cdot 20.57 \cdot 10^{-3}\si{\metre}^3}{0.04\si{\metre}} \\
                                                      &\Rightarrow \ \rho_H = \underline{\underline{514.29\si{\kg/\metre}^3}}
\end{align*}
\newpage