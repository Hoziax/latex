\clearpage % flushes out all floats
\subsection{Schaltungen}
\subsubsection{Supraleitung}

Im Jahre 1908 gelang es dem Niederländer Heike Kamerlingh Onnes, Helium zu verflüssigen. Bei Untersuchungen im Bereich der Tieftemperaturphysik stellte er 1911 etwas erstaunliches fest: Beim abkühlen von Quecksilber 
    auf eine Temperatur von $-269\si{\degreeCelsius} \ (\approx 4\si{\kelvin})$ stellte er fest, dass es unterhalb von $4.19\si{\kelvin}$ sprungartig seinen elektrischen Widerstand \textit{R} verlor.

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            \begin{axis}
                [
                    xmin=4.00,
                    xmax=4.40,
                    ymin=0,
                    ymax=0.15,
                    ytick={0,0.025,...,0.15},
                    xmajorgrids=true,
                    ymajorgrids=true,
                    grid style=thick,
                    inner xsep=2mm,
                    inner ysep=2mm,
                    x tick label style={/pgf/number format/.cd,%
                        scaled x ticks = false,
                        set decimal separator={.},
                        precision=2,
                        fixed,
                        /pgf/number format/fixed zerofill},
                    y tick label style={/pgf/number format/.cd,%
                        scaled x ticks = false,
                        set decimal separator={.},
                        precision=3,
                        fixed},
                    x unit=\si{\kelvin},
                    xlabel=Temperatur,
                    y unit=\si{\ohm},
                    ylabel=Widerstand,
                ]
                \draw[->,thick,MyLightBlue] (190,0) -- (175,12.5) node[anchor=south, shift={(-2mm,-1mm)}, text=MyBlack] {$10^{-5}\si{\ohm}$};
                \addplot[rounded corners,color=MyLightBlue,thick] coordinates {(4.4,0.13)(4.225,0.113)(4.201,0.0025)(4.19,0)};

            \end{axis}

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Originalnotiz von Heike Kamerlingh Onnes: Untersuchung von Quecksilber.}
\end{figure}

In einem Supraleiter kann der Strom also verlustfrei fliessen. Eine Theorie der Supraleitung wurde erst 1957 dank der Quantenmechanik gefunden.

\subsubsection{Der unverzweigte Stromkreis (Serieschaltung)}

Wie sieht ein unverzweigter Stromkreis -- eine sogenannte Serieschaltung -- aus? Hier eine Skizze, die das Ganze veranschaulicht:

\begin{figure}[H]
    \begin{center}
        \begin{circuitikz}[>=latex] % arrow style
            
            % Circuit
            \draw
                (0,0) to [battery1, l=$U_0$] (0,-2)
                to [ammeter,fill=MyRed,l=$I$] (11,-2)
                to [short] (11,0)
                to [european resistor,l=$R_2$] (5.5,0)
                to [european resistor,l=$R_1$] (0,0)
            ;
            \draw[->,very thick,MyDarkBlue] (4.75,-2) -- (3,-2) node[anchor=north, shift={(8.75mm,0mm)}, text=MyBlack] {$I$};
            \draw (-0.25,-0.7) node{$+$};
            \draw (-0.25,-1.2) node{$-$};

            %% Right resistance
            \draw
                (6.25,0) to [short,*-] (6.25,1)
                to [voltmeter,fill=MyGreen,l=$U_2$] (10.25,1)
                to [short,-*] (10.25,0)
            ;
            \draw[->,very thick,MyDarkBlue] (9,0) -- (10.75,0) node[anchor=north, shift={(-8.75mm,0mm)}, text=MyBlack] {$I$};

            %% Left resistance
            \draw
                (0.75,0) to [short,*-] (0.75,1)
                to [voltmeter,fill=MyGreen,l=$U_1$] (4.75,1)
                to [short,-*] (4.75,0)
            ;
            \draw[->,very thick,MyDarkBlue] (0.25,0) -- (2,0) node[anchor=north, shift={(-8.75mm,0mm)}, text=MyBlack] {$I$};
            
        \end{circuitikz}
    \end{center}
    \vspace{-1em}
    \caption{Serieschaltung.}
\end{figure}


Bei einer Serieschaltung zweier Widerstände setzt sich die Gesamtspannung $U_0$ aus der Summe der beiden Teilspannungen $U_1$ und $U_2$ zusammen. Man kann auch die beiden Einzelwiderstände $R_1$ und $R_2$ durch 
    einen sogenannten \textit{Ersatzwiderstand R} ersetzen, der sich elektrisch gleich verhält wie die beiden Einzelwiderstände. Aus dem gesagten kann man zusammengefasst aufschreiben:
    \begin{align}
        R &= R_1 + R_2 \label{eq:Ersatzwiderstand R Serieschaltung} \\
        U_0 &= U_1 + U_2 \label{eq:Gesamtspannung Serieschaltung} \\
        I &= \frac{U_0}{R} = \frac{U_0}{R_1 + R_2} \label{eq:Stromstärke Serieschaltung}
    \end{align}

\subsubsection{Der verzweigte Stromkreis (Parallelschaltung)}

Bei einer Parallelschaltung zweier Ohm'scher Widerstände setzt sich die Gesamtstromstärke \textit{I} aus der Summe der beiden Teilstromstärken $I_1$ und $I_2$ zusammen.

\begin{figure}[H]
    \begin{center}
        \begin{circuitikz}[>=latex] % arrow style
            
            % Circuit
            \draw
                (0,0) to [battery1, l=$U_0$] (0,-3)
                to [ammeter,fill=MyRed,l=$I$] (11,-3)
                (0,0) to [short] (2,0)
                to [short] (2,1)
                to [short] (3.5,1)
                to [ammeter,fill=MyRed] (5.5,1)
                to [european resistor,l=$R_1$] (8,1)
                to [short] (9,1)
                to [short] (9,0)
                to [short] (11,0)
                to [short] (11,-3)
                (2,0) to [short,*-] (2,-1)
                to [short] (3.5,-1)
                to [ammeter,fill=MyRed] (5.5,-1)
                to [european resistor,l=$R_2$] (8,-1)
                to [short] (9,-1)
                to [short,-*] (9,0)
            ;
            \draw[->,very thick,MyDarkBlue] (2.25,1) -- (3.5,1) node[anchor=north, shift={(-6.25mm,0mm)}, text=MyBlack] {$I_1$};
            \draw[->,very thick,MyDarkBlue] (2.25,-1) -- (3.5,-1) node[anchor=south, shift={(-6.25mm,0mm)}, text=MyBlack] {$I_2$};
            \draw[->,very thick,MyDarkBlue] (10,0) -- (11,0) -- (11,-0.75) node[anchor=south, shift={(-3mm,2mm)}, text=MyBlack] {$I$};
            \draw[->,very thick,MyDarkBlue] (4.75,-3) -- (3,-3) node[anchor=north, shift={(8.75mm,0mm)}, text=MyBlack] {$I$};
            \draw (-0.25,-1.2) node{$+$};
            \draw (-0.25,-1.7) node{$-$};
            
        \end{circuitikz}
    \end{center}
    \vspace{-1em}
    \caption{Parallelschaltung.}
\end{figure}

Die Batteriespannung $U_0$ und die Spannungen über den beiden Widerständen sind gleich gross. Für die Stromstärke gilt daher:
    \begin{align}
        I &= I_1 + I_2 \label{Stromstärke Parallelschaltung} \\
        \frac{U}{R} &= \frac{U}{R_1} + \frac{U}{R_2}
    \end{align}

Anstelle der beiden Widerstände $R_1$ und $R_2$ kann auch hier ein Ersatzwiderstand \textit{R}, der sich elektrisch gleich verhält wie die beiden Einzelwiderstände, genommen werden. Es gilt:
    \begin{align}
        \frac{1}{R} &= \frac{1}{R_1} + \frac{1}{R_2} \\
        R &= \frac{R_1 \cdot R_2}{R_1 + R_2} \label{eq:Ersatzwiderstand Parallelschaltung}
    \end{align}