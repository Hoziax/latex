\clearpage % flushes out all floats
\section{Aerostatik}

Die Aerostatik ist die Lehre von Gleichgewichtszuständen von Gasen. Sie ist als Teilgebiet der Fluidstatik die Lehre der unbewegten, insbesondere der strömungsfreien, kompressiblen Fluide (Gase). Die Aerostatik 
    beschäftigt sich mit der Dichteverteilung vornehmlich in der Luft. Eine der zugehörigen Anwendungen ist die barometrische Höhenformel. \\\hspace*{\fill}

Der Luftdruck an einem beliebigen Ort der Erdatmosphäre ist der hydrostatische Druck der Luft, der an diesem Ort herrscht. Dieser Druck entsteht durch die Gewichtskraft $\overrightarrow{F}_G$ der Luftsäule, die auf 
    der Erdoberfläche oder einem Körper steht. Der mittlere Luftdruck der Atmosphäre (der \glqq atmosphärische Druck\grqq{}) auf Meereshöhe beträgt normgemäss $101'325\si{\pascal} = 1'013.25\si{\hecto\pascal} = 1.013 \cdot 10^5\si{\pascal} = 1.013\si{\bar}$. \\\hspace*{\fill}

Da die Luft -- oder Gase im Allgemeinen -- \textbf{kompressibel} sind, ist ihr Druckverhalten relativ zur Höhe über Meer nicht mehr linear:
    \begin{figure}[H]
        \begin{center}
            \begin{tikzpicture}[>=latex] % arrow style

                % Axis
                \foreach \i in {-0.5,0,0.5,1} {\node at (\i + 4.5,-1) {$+$};} % axis ticks x axis
                \foreach \i in {-0.5,0,0.5,1,1.5} {\node at (3.5,\i) {$+$};} % axis ticks y axis
                \draw[thick,->] (3.5,-1) -- (6.5,-1) node[anchor=north, shift={(12mm,0)}] {$\textrm{Luftdruck} \, [\si{\pascal}]$}; % x axis
                \draw[thick] (6,-1.1) -- (6,-0.9) node[anchor=north, shift={(0,-1mm)}] {$10^5$}; % x axis #10^5
                \draw[thick] (3.5,-1.1) -- (3.5,-0.9) node[anchor=north, shift={(0,-1mm)}] {0}; % x axis #0
                \draw[thick,->] (3.5,-1) -- (3.5,2) node[anchor=east, shift={(0,-2.5mm)}] {$\textrm{Höhe} \, [\si{\metre}]$}; % y axis
                \draw[thick] (3.5,-0.75) -- (3.5,-1) -- (3.75,-1); % make axes look connected
                
                % Parabola
                \draw[MyLightBlue,very thick,bend right] (3.75,1.5) to (6,-1);

            \end{tikzpicture}
        \end{center}
        \vspace{-1em}
        \caption{Druckverhalten in Relation zur Höhe.}
    \end{figure}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gegeben:} Ein Fernseher mit einer Höhe von 0.4 Meter und einer Breite von 0.55 Meter befindet sich auf einem Tisch. \\\hspace*{\fill}

\textit{Gesucht:} Mit welcher Kraft drückt der Luftdruck auf den Bildschirm? \\\hspace*{\fill}

\textit{Lösung:}
\[
    p = \frac{\overrightarrow{F}}{A} \ \Rightarrow \ \overrightarrow{F} = p \cdot A = 101'325\si{\pascal} \cdot 0.22\si{\metre}^2 = \underline{\underline{22'291.5\si{\newton}}}
\]
    
\subsection{Die Barometrische Höhenformel}

Die barometrische Höhenformel beschreibt die vertikale Verteilung der (Gas-)Teilchen in der Atmosphäre der Erde, also die Abhängigkeit des Luftdruckes von der Höhe. Man spricht daher auch von einem vertikalen 
    Druck-Gradienten, der jedoch aufgrund der hohen Wetterdynamik innerhalb der unteren Atmosphäre nur mit Näherungen auf mathematischem Wege beschrieben werden kann. \\\hspace*{\fill}

In der einfachsten Form kann grob angenommen werden, dass der Luftdruck in der Nähe des Meeresspiegels um ein Hektopascal (entsprechend $1\permil$ des mittleren Luftdrucks) je acht Meter Höhenzunahme abnimmt. Etwas 
    besser ist die Näherung, dass der Druck mit zunehmender Höhe exponentiell abnimmt. Dieser Zusammenhang war 1686 erstmals von Edmond Halley erkannt worden. \\\hspace*{\fill}

Die in einführender Literatur und im Schulunterricht meist zitierte klassische barometrische Höhenformel gilt für den Spezialfall, dass die Temperatur \textit{T} in jeder Höhe gleich, die Atmosphäre also 
    \textbf{isotherm} ist. Die Formel lautet folgendermassen:
    \begin{equation}
        p(h_1) = p(h_0) \cdot e^{- \frac{\Delta h}{8'000\si{\metre}}} \label{eq:Barometrische Höhenformel}
    \end{equation}
    Hierbei ist $p(h_1)$ der Druck, der in einer bestimmten Höhe herrscht, $p(h_0)$ der Druck am Boden (meist auf Meereshöhe) und $\Delta h$ der Höhenunterschied zwischen $p(h_1)$ und $p(h_0)$. Des Weiteren ist \textit{e} die Euler'sche Zahl, dessen Gegenoperation der \textit{ln} (logarithmus naturalis) ist. \\\hspace*{\fill}

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1}

\textit{Gesucht:} In welcher Höhe ist der Luftdruck auf die Hälfte des Normaldrucks gesunken? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    p(h_1) &= p(h_0) \cdot e^{- \frac{\Delta h}{8'000\si{\metre}}} \\
    &\Rightarrow \ h = 8'000\si{\metre} \cdot ln \left(\frac{0.5 \cdot p(h_0)}{p(h_0)}\right) \cdot (-1) \\
    &\Rightarrow \ h = 8'000 \cdot ln(0.5) \cdot (-1) = \underline{\underline{5'545.18\si{\metre}}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Ein Flugzeug hat bei einer Flughöhe von 10'000 Meter einen Kabinendruck von 2'300 Meter Höhe. \\\hspace*{\fill}

\textit{Gesucht:} Welche Kraft drückt auf die Kabinentür, welche 0.8 Meter auf 2 Meter misst? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    p(2'300\si{\metre}) &= 101'325\si{\pascal} \cdot e^{- \frac{2'300\si{\metre}}{8'000\si{\metre}}} = 76'007.59\si{\pascal} \\
    p(10'000\si{\metre}) &= 101'325\si{\pascal} \cdot e^{- \frac{10'000\si{\metre}}{8'000\si{\metre}}} = 29'030.09\si{\pascal} \\
    \Rightarrow \ \overrightarrow{F} &= \Delta p \cdot A = (76'007.59\si{\pascal} - 29'030.09\si{\pascal}) \cdot (0.8\si{\metre \cdot 2\si{\metre}}) = \underline{\underline{75'163.98\si{\pascal}}}
\end{align*}
\newpage

\subsection{Die Kompressibilität der Gase}
\subsubsection{Das Gesetz von Boyle-Mariotte}

Bei der Untersuchung der Druckabhängigkeit des Gasvolumens bei \textbf{konstanter Temperatur} (= Isothermer Vorgang) und konstanter Gasmenge zeigt sich, dass das Produkt aus Gasdruck \textit{p} und Gasvolumen 
    \textit{V} konstant ist: Wird das Volumen halbiert, so verdoppelt sich der Druck \textit{p}; wird das Volumen verdoppelt, halbiert sich der Druck. Dies ist das Gesetz von Boyle-Mariotte:
    \begin{equation}
        p \cdot V = \text{konstant} = c \label{eq:Boyle-Mariotte Formel 1}
    \end{equation}
    \begin{equation}
        p_1 \cdot V_1 = p_2 \cdot V_2 = \text{konstant} = c \label{eq:Boyle-Mariotte Formel 2}
    \end{equation}

Die Funktion $p = \frac{c}{V}$ zeigt die Abhängigkeit des Drucks \textit{p} eines Gases mit dem Volumen \textit{V} bei konstanter Temperatur \textit{T} an. Der Graph -- eine Hyperbel -- dieser Funktion heisst 
    \textbf{Isotherme} (Abbildung \ref{fig:Gasdruck in Funktion des Volumens.}, graue Kurve T). Für höhere Temperaturen ($T' > T$) liegen die Isothermen im p-V-Diagramm über der (grauen) T-Isotherme, für tiefere Temperaturen ($T'' < T$) unter der grauen T-Isotherme. \hspace*{\fill}

\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Grid
            \draw[step=0.5cm,white,very thin] (-2.3,-1.4) grid (12.3,3.4); % Used to align graph in middle
            
            % Axis
            \foreach \i in {1,2,} {\node at (\i + 3,-1) {$+$};} % axis ticks x axis
            \foreach \i in {-0.5,0.5,1.5} {\node at (3,\i) {$+$};} % axis ticks y axis
            \draw[thick,->] (3,-1) -- (7,-1) node[anchor=north, shift={(7mm,0)}] {Volumen}; % x axis
            \draw[thick] (3.5,-1.1) -- (3.5,-0.9) node[anchor=north, shift={(0,-1mm)}] {5}; % x axis #5
            \draw[thick] (4.5,-1.1) -- (4.5,-0.9) node[anchor=north, shift={(0,-1mm)}] {15}; % x axis #15
            \draw[thick] (5.5,-1.1) -- (5.5,-0.9) node[anchor=north, shift={(0,-1mm)}] {25}; % x axis #25
            \draw[thick] (6.5,-1.1) -- (6.5,-0.9) node[anchor=north, shift={(0,-1mm)}] {35}; % x axis #35

            \draw[thick,->] (3,-1) -- (3,3) node[anchor=east, shift={(0,-2.5mm)}] {Druck}; % y axis
            \draw[thick] (2.9,0) -- (3.1,0) node[anchor=east, shift={(-1mm,0mm)}] {0.5}; % y axis #0.5
            \draw[thick] (2.9,1) -- (3.1,1) node[anchor=east, shift={(-1mm,0mm)}] {1}; % y axis #1
            \draw[thick] (2.9,2) -- (3.1,2) node[anchor=east, shift={(-1mm,0mm)}] {1.5}; % y axis #1.5
            \draw[thick] (3,-0.75) -- (3,-1) -- (3.25,-1); % make axes look connected

            % Dashed line
            \draw[MyBlack,thick,dashed] (5.5,2) -- (5.5,-1);
            
            % Parabola
            \draw[MyGreen,very thick] (3.2,1.8) .. controls (3.7,0.35) .. (4.7,-0.8) node[anchor=south, shift={(-4.5mm,7.5mm)}] {T''}; % T''
            \draw[MyGrey,very thick] (4.2,2) .. controls (4.7,0.55) .. (5.7,-0.6) node[anchor=south, shift={(-4mm,6.5mm)}] {T}; % T
            \draw[MyLightBlue,very thick] (5.2,2.2) .. controls (5.7,0.75) .. (6.7,-0.4) node[anchor=south, shift={(-4.5mm,8.5mm)}] {T'}; % T'

            % Isotherme
            \draw[MyBlack,thick,->] (7.5,0.25) -- (6.5,0) node[anchor=west, shift={(10mm,2.5mm)}] {Isotherme (Hyperbel)};

        \end{tikzpicture}
    \end{center}
    \vspace{-1em}
    \caption{Gasdruck in Funktion des Volumens.}
    \label{fig:Gasdruck in Funktion des Volumens.}
\end{figure}

Das Gesetz von Boyle-Mariotte gilt für ein ideales Gas, dessen Atome keine Ausdehnung haben, also Massepunkte sind, und keine Kräfte aufeinander ausüben.

\paragraph{Beispielaufgaben}
\subparagraph{Beispiel 1 (\glqq Bieraufgabe\grqq)}

\textit{Gegeben:}
\begin{figure}[H]
    \begin{center}
        \begin{tikzpicture}[>=latex] % arrow style

            % Cylinder
            \draw[draw=none,fill=MyLightBlue] (4,1) -- (6,1) -- (6,-1) -- (4,-1) -- cycle; % water
            \draw[color=MyDarkBlue,thick,fill=MyLightBlue] (5,1) ellipse (1 and 0.25); % water
            %% Ellipses
            \draw[color=MyGrey,very thick,fill=none] (5,2) ellipse (1 and 0.25); % Top
            \draw [color=MyGrey,very thick,fill=MyLightBlue] plot [smooth, tension=1.75] coordinates {(4,-1) (5,-1.25) (6,-1)}; % Bottom (full line)
            \draw [color=MyGrey,very thick,dashed] plot [smooth, tension=1.75] coordinates {(4,-1) (5,-0.75) (6,-1)}; % Bottom (dotted line)
            %% Lines
            \draw[color=MyGrey,very thick] (4,2) -- (4,-1); % Left
            \draw[color=MyGrey,very thick] (6,2) -- (6,-1); % Right

            % Height difference
            \draw[MyOrange,very thick,dashed] (2.2,1) -- (5,1); % top
            \draw[MyOrange,very thick,dashed] (2.2,-1) -- (5,-1); % bottom
            \draw[MyOrange,very thick] (1.7,1) -- (2.1,1); % difference thingy top
            \draw[MyOrange,very thick] (1.9,1) -- (1.9,-1) node[anchor=center, shift={(-8mm,12.5mm)}, text=MyOrange] {$h = 20\si{\cm}$}; % difference thingy vertical
            \draw[MyOrange,very thick] (1.7,-1) -- (2.1,-1); % difference thingy bottom

            % Bubbles
            \draw[draw=none,fill=MyGrey] (5,0.875) circle (0.125); %top
            \draw[draw=none,fill=MyGrey] (5,-0.875) circle (0.125); %bottom

            % Arrows
            \draw[MyBlack,thick,->] (6.5,0.875) -- (5.2,0.875) node[anchor=west, shift={(13mm,-1mm)}] {$p_0,V_0$};
            \draw[MyBlack,thick,->] (6.5,-0.875) -- (5.2,-0.875) node[anchor=west, shift={(13mm,1mm)}] {$p_1,V_1$};

        \end{tikzpicture}
    \end{center}

\end{figure}

Die Flüssigkeit ist als Wasser zu behandeln, $p(h_0)$ ist $101'325\si{\pascal}$ und die Temperatur ist konstant. Die Formel für das Volumen einer Kugel ist $V = \frac{4}{3} \cdot \pi \cdot r^3$. \\\hspace*{\fill}

\textit{Gesucht:} Was ist das Verhältnis zwischen $d_0$ und $d_1$? \\\hspace*{\fill}

\textit{Lösung:}
\begin{align*}
    p_0 \cdot V_0 &= p_1 \cdot V_1 \\
    p_0 \cdot \frac{4}{3} \cdot \pi \cdot \left(\frac{d_0}{2}\right)^3 &= (p_0 + \rho \cdot g \cdot h) \cdot \left(\frac{d_1}{2}\right)^3 \\
    p_0 \cdot \frac{d_0^3}{8} &= (p_0 + \rho \cdot g \cdot h) \cdot \frac{d_1^3}{8} \\
    p_0 \cdot d_0^3 &= (p_0 + \rho \cdot g \cdot h) \cdot d_1^3 \\
    d_0^3 &= \frac{(p_0 + \rho \cdot g \cdot h)}{p_0} \cdot d_1^3 \\
    d_0 &= \sqrt[3]{\frac{(p_0 + \rho \cdot g \cdot h)}{p_0}} \cdot d_1 \\
    d_0 &= \sqrt[3]{\frac{(101'325\si{\pascal} + 1'000\si{\kg/\metre^3} \cdot 9.81\si{\metre/\second}^2 \cdot 0.2\si{\metre})}{101'325\si{\pascal}}} \cdot d_1 \\
    d_0 &= \underline{\underline{1.0064 \cdot d_1}}
\end{align*}

\subparagraph{Beispiel 2}

\textit{Gegeben:} Eine Fläche von 20x20 Zentimeter befindet sich auf Meereshöhe. \\\hspace*{\fill}

\textit{Gesucht:} Welche Kraft wirkt auf die Fläche? \\\hspace*{\fill}

\textit{Lösung:}
\[
    p = \frac{\overrightarrow{F}}{A} \ \Rightarrow \ \overrightarrow{F} = p \cdot A = 101'325\si{\pascal} \cdot 0.04\si{\metre}^2 = \underline{\underline{4'053\si{\newton}}}
\]