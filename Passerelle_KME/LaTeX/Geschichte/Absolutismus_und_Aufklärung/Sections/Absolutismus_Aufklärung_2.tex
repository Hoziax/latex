\clearpage % flushes out all floats
\newpage
\section{Die Aufklärung}
\subsection{Alphabetisierung und wissenschaftliche Revolution im 17. Jahrhundert}

Die Werke grosser Philosophen und Wissenschaftler des 17. Jahrhunderts geben darüber Auskunft, wie sich das Denken der Menschen aus der bis dahin herrschenden Vormundschaft der Theologie löste. V.a. seit der Epoche 
    des Humanismus und der Renaissance waren die Menschen für neue Kenntnisse bereit. \par

Dass sie es konnten, hängt mit der Erfindung des Buchdrucks und dem Ausbruch der Reformation zusammen, weil danach immer mehr Menschen lesen und schreiben lernten. Diese \textbf{Alphabetisierung} betraf zunächst nur 
    einen kleinen Teil der Bevölkerung, die städtischen und ländlichen Oberschichten. Die Folgen davon waren, dass die neuen Erkenntnisse der Philosophen eine grosse Zahl von Menschen erfasste und sie zum eigenem Nachdenken, eigenem Suchen und Fragen, anregten. Auf diese Weise folgte der Alphabetisierung ein \textbf{Prozess der zunehmenden Kenntnisaneignung und -ausweitung}, welche sich von der Mitte des 17. bis zum Ende des 18. Jahrhunderts auf allen Ebenen des wissenschaftlichen Lebens verfolgen lässt. Nennenswerte Beispiele sind der Holländer \textbf{Christian Huygens}, der den Bau von immer besseren Uhren, Mikroskopen und Fernrohren vorantrieb; der Engländer \textbf{Isaac Newton}, der die nach ihm benannten \glqq Axiome\grqq{} entwickelte. \par

Nicht überall waren es die Universitäten, an denen der neue Geist wehte: In Frankreich versperrte sich ihm die Pariser Universität Sorbonne unter dem Einfluss der Kirche. Hier waren es die Akademien und freie 
    Vereinigungen von Gelehrten und Gebildeten, die für Diskussion und Verbreitung sorgten. \par

Zum wesentlichen Medium der Verbreitung von wissenschaftlichen Erkenntnissen diente das gedruckte Buch. In dieser Zeit war das Buch jedoch weit mehr als nur Träger wissenschaftlicher Informationen:
    \begin{itemize}
        \item Zwischen 1680 und 1780 wuchs in Westeuropa der Buchbestand um das Zehnfache, wofür nicht allein die Gelehrten verantwortlich waren.
        \item Auch die Produktion von Erbauungs- und Bildungsbüchern, Romanen und Erzählungen nahm in diesen 100 Jahren zu.
    \end{itemize}
    Nicht nur wohlhabende Bürger, sondern auch das mittlere Bürgertum und viele Landbewohner wurden in dieser Zeit von der Alphabetisierung erfasst.

\subsection{Die europäische Aufklärungsbewegung}

Die \textbf{Bildungsbewegung der Aufklärung} beruhte auf den Voraussetzungen, die die wissenschaftliche Revolution des 17. Jahrhunderts geschaffen hatte und ging aus ihr hervor. Viele Gelehrte des 17. Jahrhunderts 
    können als unmittelbare Vorläufer der Ausklärung betrachtet werden. Im Kern wollten die Aufklärer nichts anderes als sie: ihre Umwelt erkennen, entdecken und verstehen. Dabei galt es überlieferte Dogmen kritisch zu hinterfragen und sich von traditionellen Autoritäten zu lösen. \textbf{Immanuel Kant} hat 1783 eine berühmte Definition der Bewegung versucht:
    \begin{quote}[Immanuel Kant]{1783}
        Aufklärung ist der Ausgang des Menschen aus seiner selbst verschuldeten Unmündigkeit. Unmündigkeit ist das Unvermögen, sich seines Verstandes ohne die Leitung eines anderen zu bedienen. Selbstverschuldet ist diese Unmündigkeit, wenn die Ursache derselben nicht am Mangel des Verstandes, sondern der Entschliessung und des Muthes liegt, sich seiner ohne die Leitung eines anderen zu bedienen. Sapere aude! Habe Muth dich deines eigenen Vertandes zu bedienen! ist also der Wahlspruch der Aufklärung.
    \end{quote}

Die Aufklärer erhoben den Anspruch, ein neues Zeitalter einzuleiten, in dem das \glqq Licht der Vernunft\grqq{} anstelle unbewiesener Glaubenssätze sowie kirchlicher und fürstlicher Bevormundung herrschen sollte. 
    Die \textbf{kritische Vernunft} sollte zum Massstab allen Handelns (Rationalismus) und die Erfahrung zum Massstab des Erkennens (Empirismus) werden. Ziel war der selbstbewusste, kritische Bürger. Die Aufklärer forderten auch Toleranz gegenüber Andersdenkenden und -gläubigen, die Gleichheit der Menschen, die freie Entwicklung der Produktion sowie eine breite und staatlich kontrollierte Schulbildung. Diesem Programm entsprechend entfaltete sich die europäische Aufklärung ausgeprägt als eine \textbf{pädagogische Bewegung}. Die Aufklärer waren durch einen starken Glauben an den stetigen \textbf{wissenschaftlich-technischen und geistig-moralischen Fortschritt} geprägt.

\subsection{Die Herausbildung der bürgerlichen Staatstheorie}

Auch die Grundlagen für den modernen Verfassungsstaat wurden von den Aufklärern gelegt. Ausgehend von der Kritik an Kirche und absolutem Königtum entwarfen sie eine Theorie des Staates, mit der sich Herrschaft 
    vernünftig begründen liess. Dadurch wurden in ganz Europa die Grundlagen des sakralen Königtums erschüttert. Die Aufklärer verwiesen die Fürsten und alle, die im öffentlichen Leben Verantwortung trugen, auf ihre Pflicht, dieses Leben so einzurichten und zu reformieren, dass Glück und das allgemeine Beste für möglichst viele Menschen verwirklicht würden.
    \begin{itemize}
        \item \textbf{Thomas Hobbes} (1588 - 1679) begründete die Notwendigkeit eines starken Herrschers, der mit Hilfe des staatlichen Gewaltmonopols den \glqq Kampf aller gegen alle\grqq{} zu unterbinden hatte.    
                  Aus ihrem Selbsterhaltungstrieb unterwarfen sich die Menschen laut Hobbes dem Staat freiwillig und wurden so zu Untertanen.
        \item \textbf{John Locke} (1632 - 1704), einer der wichtigsten Denker, ging wie Hobbes ebenfalls von einem staatslosen Naturzustand der Menschen aus. Mithilfe seiner Vertragstheorie begründete er jedoch, wie 
                  diese sich \textbf{Gesellschaftsverträge} und somit Regierungen gaben und damit den staatslosen Zustand verliessen. Anders als Hobbes verpflichtete er den Staat, die \textbf{Naturrechte} des Menschen, die allen von Geburt an zustünden, zu schützen. Nur so legitimierte sich sich die Existenz des Staates. In diesem Sinn war es folgerichtig, dass Locke auch ein \textbf{Widerstandsrecht} gegen die Machthaber postulierte, sofern sie diese Menschenrechte verletzten. \par
              Die wichtigsten dieser Rechte waren der Anspruch auf das eigene Leben, auf die persönliche Freiheit und auf den Besitz des erworbenen Eigentums. Seine Schriften lieferten Begründung und Legitimation 
                  der \glqq Glorious Revolution\grqq{}.
        \item \textbf{Charles de Montesquieu} (1689 - 1755) baute die Idee der \textbf{gegenseitigen Kontrolle der staatlichen Gewalten} (Gewaltenteilung) entscheidend aus. Er wollte die Rechtssicherheit und damit 
                  die Justiz als eigenständige staatliche Gewalt etablieren. Er unterschied drei voneinander unabhängige Gewalten: die legislative (gesetzgebende), exekutive (gesetzausführende) und judikative (richterliche) Gewalt. Nur dies konnte in seinen Augen gewähr bieten, staatliche Macht zu beschränken und die Rechte des Individuums zu schützen.
        \item \textbf{Jean-Jacques Rousseau} (1717 - 1778) formulierte die Idee der \textbf{Volkssouveränität} und entwarf damit die Idee einer Staatsform, die eine tatsächliche Volksherrschaft darstellte. Er 
                  orientierte sich dabei am Modell der Demokratie. Allerdings stellte sich schon bei ihm die Frage, wie zu verfahren wäre, wenn in einer Abstimmung eine Mehrheit des Volkes nicht im Sinne des Gemeinwohls entschiede. \par
              Eine mögliche Antwort darauf lautete, dass eine bestimmte politische Gruppe den Alleinvertretungsanspruch für das Gemeinwohl beanspruchte. Deshalb wird in seiner Theorie häufig der Ansatz zu einer 
                  Diktatur gesehen.
    \end{itemize}

\subsection{Aufgeklärter Absolutismus}

Das politische Denken der Aufklärung setzte auch das politische Handeln der Fürsten unter Druck: Sie mussten sich Reformforderungen gegenüber öffnen und mit Aufklärern ins Gespräch kommen. Seit Mitte des 18. 
    Jahrhunderts geschah das an so vielen Höfen, dass eine veränderte Form des Absolutismus entstand: der \glqq aufgeklärte Absolutismus\grqq{}. Diese Bezeichnung weist auf ein Paradox hin, denn Aufklärung meint vernunftgeleitetes Handeln nach Gesetzen, die der menschlichen Kritik und Analyse jederzeit zugänglich ist, Absolutismus jedoch eine an Satzungen nicht gebundene politische Herrschaft, die i.d.R. von einem Einzelnen ausgeübt wird. Deshalb nennen viele Historiker diese Zeit \glqq\textbf{Reformabsolutismus}\grqq{}, weil politische, wirtschaftliche und soziale Reformen ein besonderes Kennzeichen der europäischen Staaten in dieser Zeit waren. \par

Mit der Idee des aufgeklärten Absolutismus versuchte die alteuropäische Monarchie den rasanten Fortschritt des 18. Jahrhunderts für sich selbst nutzbar zu machen. Im Gegensatz zu Preussen, Österreich und Russland 
    hat Frankreich, obschon sie eine grosse Aufklärungsbewegung besass, keinen wirklichen aufgeklärten Absolutismus gekannt. Reformen wurden zwar viele Vorgeschlagen, doch nur halbherzig vom König unterstütz, weswegen die Pläne bald aufgegeben werden mussten. Die französischen Könige klammerten sich immer wieder an die Traditionen der absoluten Monarchie. \par

In der zweiten Hälfte des 18. Jahrhunderts wurden so viele Hoffnungen geweckt und enttäuscht, was zur Entstehung einer revolutionären Stimmung im Lande beitrug. In anderen Staaten hingegen hat der aufgeklärte 
    Absolutismus z.T. beachtliche Erfolge errungen wie die \textbf{Einrichtung von öffentlichen Schulen}, die Humanisierung des Strafrechts oder die \textbf{Steigerung der Wirtschaftsleistung}. Allerdings wurde in keinem dieser Staaten die monarchische Gewalt eingeschränkt; der Untertanenstaat blieb bestehen, die Aussenpolitik war weiterhin auf die rücksichtslose Machtausdehnung des eigenen Staates ausgerichtet.