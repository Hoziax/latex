\clearpage % flushes out all floats
\newpage
\section{Der Absolutismus}
\subsection{Der Wandel des mittelalterlichen Staates}
\subsubsection{Die Idee der Staatsraison}

Im Verlauf des 16. und frühen 17. Jahrhunderts veränderten sich die europäischen Monarchien, und mit ihnen der Staat. Bis dahin trug das Herrscherbild noch Züge, die dem Mittelalter und der Renaissance entstammten, 
    also der Zeit vor 1500. Damals begriffen sich die Monarchen als oberste Adlige ihres Landes und betonten den Gedanken der Gefolgschaft. Sie reisten viel in ihren Ländern umher, hielten Gerichtssitzungen ab und kamen ihrer vornehmsten Aufgabe, der des obersten Richters, nicht selten persönlich nach. Ihr Lebens- und Herrschaftsstil hatte viel Persönliches, Direktes, Unvermitteltes und wenig Amtliches an sich. \par

Spätestens seit Mitte des 17. Jahrhunderts sah dies völlig anders aus: Die neuen Fürsten waren strenge Amtspersonen, von einer Aura der Ordnung umgeben, von den Untertanen weit entfernt, die zumeist in grossen 
    Hofanlagen ausserhalb der mittelalterlichen Stadtkerne wohnten, von einer Fülle von Beamten abgeschirmt und dem Irdischen fast entrückt. Ihrem Selbstverständnis nach waren sie tatsächlich dem Göttlichen nahe. \par

Der Wandel hatte tiefe Ursachen:  Die mit der \textbf{Reformation} eingeleitete Spaltung der Christenheit in protestantische und römisch-katholische Glaubensrichtungen führte im Zuge der Konfessionalisierung im 16. 
    und 17. Jahrhundert zu \textbf{Bürger- und Religionskriegen} in Mittel- und Westeuropa. Dies stürzte weite Teile in eine politische, wirtschaftliche und soziale Krise, was die königsmacht in dem Ländern schwächte. Daher übte die Vorstellung einer starken Zentralgewalt, die Konfliktlösungen durchsetzen konnte, eine grosse Faszination aus. Dies veranlasste Philosophen und Staatsmänner wie \textbf{Thomas Hobbes}, eine durch den Staat zu schaffende und zu überwachende Ordnung zu entwickeln. In ihre stellt der Staat eine über den Interessen der gesellschaftlichen Gruppen, insbesondere über den religiösen Parteien, stehende Kraft dar, die über eine Staatsraison -- eine nicht weiter ableitbare Begründung -- verfügte. In diesem Zusammenhang entwickelte sich auch die Auffassung, dass den Repräsentanten des Staates eine deutlich über alle Untertanen hinausgehobene Stellung eingeräumt werden müsse, wenn sie dem Staat und seinen Interessen wirkungsvoll dienen sollten.

\subsubsection{Die Epoche des Absolutismus}

Die Zeit, die von herausgehobenen Monarchen geprägt wurde, bezeichnet man als Epoche des \textbf{Absolutismus}. Das Wort bedeutet so viel wie \glqq losgelöst\grqq{} und wurde im 16. Jahrhundert vom französischen 
    Juristen Jean Bodin in die Staatslehre eingeführt. Er bezeichnete die oberste Gewalt eines Staates als die \glqq gegenüber den Bürgern und Untertanen höchste und von den Gesetzen losgelöste Gewalt\grqq{}. \par

Die europäischen Landesherren leiteten daraus eine umfassende Legitimation ihrer Herrschaft ohne Beschränkungen von innen oder aussen ab. Die Idee der \glqq\textbf{absoluten Monarchie}\grqq{} wurde zum Inbegriff 
    fürstlichen Herrschaftswillens und Machtstrebens und verband sich im 17. Jahrhundert mit einer wachsenden religiösen Begründung des Fürstentitels. Das \textbf{Gottesgnadentum} bekam eine neue Dimension. Die Fürsten rückten ein Stück näher an Gott bzw. holten Gott ein Stück näher zu sich herab. \par

Diese neue Staats- und Regierungsform brachte eine \textbf{Professionalisierung des Regierens} mit sich. Im Gerichtswesen, in der Diplomatie und an den Höfen, in der Finanzverwaltung, im Heeres- und Verkehrswesen, 
    in der Wirtschaftsförderung und im Bildungswesen wuchsen dem frühneuzeitlichen Fürstenstaat neue Aufgaben zu, denen er mit einer beachtlichen Ausweitung der Staatstätigkeit und des Staatspersonals entsprechen musste. Man bezeichnet diesen wichtigen Vorgang als \textbf{Bürokratisierung}.

\subsubsection{Opposition gegen den Absolutismus}

Der Prozess der staatlichen Verdichtung vollzog sich nicht reibungslos. Fast alle europäische Staaten waren vom Anfang bis über die Mitte des 17. Jahrhunderts hinaus Schauplatz innerer Konflikte und Aufstände, 
    gepaart mit kriegerischen Auseinandersetzungen mit dem Ausland. Vielerorts waren die Aufstände von konfessionellen Auseinandersetzungen überlagert, sodass sich viele verschiedene Aufstandsmotive vermischten.In allen Staaten zählte der Adel zu dieser Zeit zu den unruhigsten sozialen Gruppen, da der königliche Machtzuwachs seine Vorrechte am meisten bedrohte \textrightarrow{} so waren Adlige fast überall an Aufstandsunternehmungen beteiligt. \par

Doch nicht nur der Adel übte Druck auf die monarchischen Regierungen aus, noch grösserer Druck ging von den zahllosen \textbf{Volkserhebungen} aus, die Europa in der Frühphase des Absolutismus erschütterten. Ihre 
    Träger kamen v.a. aus den nichtständischen Unterschichten, aber auch Bauern und Bürger nahmen teil. Selten hatten die Kämpfe ein klares Ziel; sie stritten i.d.R. nicht für eine Verbesserung der Lage, sondern gegen eine Verschlechterung von Lebensbedingungen. Nicht die Herrscher oder der Staat waren Ziel ihres Angriffs, vielmehr deren Repräsentanten vor Ort, die Beamten, die Steuern eintrieben, Abgaben kassierten, Getreide requirierten etc. Im Gegensatz zu den königlichen Heeren waren die städtischen und ländlichen Volkshaufen schlecht bewaffnet und schlecht organisiert. Sie hatten keine Chance, wenn sich der Landesherr zu einer massiven Gegenwehr entschloss. Volksaufstände wurden schnell und kompromisslos niedergeschlagen. In einem Grossreich wie Frankreich war es allerdings nicht einfach, überall Truppen zur Stelle zu haben, da Aufstände an vielen Orten gleichzeitig ausbrachen. Hier hat es häufig Verhandlungen und befristete Kompromisse gegeben, so z.B. die Aussetzung von Steuerforderungen. \par

Aufhalten liess sich der Prozess der absolutistischen Staatsbildung auf diese Weise nicht. Immerhin zeigen die Volksaufstände des 17. Jahrhunderts, dass \glqq das Volk\grqq{} auch damals gelegentlich schon 
    handelndes Subjekt, nicht immer nur willenloses Objekt der Politik war. Ein erfolgreicher Aufstand geschah in den Provinzen der nördlichen Niederlande, wo in der zweiten Hälfte des 16. Jahrhunderts der Kampf gegen König Philipp II. (Spanien) der neue Staat der \glqq Vereinigten Niederlande\grqq{} zur Folge hatte.

\subsection{Frankreich -- Vorbild für die europäischen Fürstenstaaten}
\subsubsection{Ludwig XIV. -- der Sonnenkönig}

Der französische König Ludwig XIV. bestieg mit 23 Jahren 1661 den Thron. Nach Jahrzenten der Kriege und Bürgerkriege konnte er mit der Unterstützunge von Adel und Bürgertum rechnen, die den König als Garanten des 
    inneren Friedens stark sehen wollten. Ausserdem gab das für Frankreich siegreiche Ende des Dreissigjährigen Kriegs (1618 - 1648) dem französischen Königtum günstige Möglichkeiten an die Hand, seine Macht nach innen und aussen zu vergrössern. Hierauf aufbauend wollte Ludwig XIV. Ruhm und Ansehen bei allen Fürsten Europas gewinnen. \par
    
Seinen Ministern erklärte er bei Herrschaftsantritt, er werde von jetzt an die Staatsgeschäfte selbst in die Hand nehmen. Ihm wurde der Anspruch \glqq\textbf{L'état c'est moi.}\grqq{}
    (\glqq Der Staat bin ich.\grqq{}) zugeschrieben. Er konnte nach eigenem Willen Gesetze erlassen, gleichzeitig stand er über ihnen. Er war der Mittelpunkt, um denen sich wie eine Sonne alles drehte: Entsprechend diesem Bild wird er auch \glqq Sonnenkönig\grqq{} genannt.

\subsubsection{Der Hofstaat}

Der Mittelpunkt des Staates war der Hof, die königliche Residenz. Als Ausdruck seiner Macht liess Ludwig XIV. in Versailles ein gewaltiges Schloss erbauen, das zum Vorbild vieler fürstlicher Residenzen in Europa 
    wurde. Hier liefen alle Fäden zusammen, hier mussten alle erscheinen, die am Glanz und der Macht des Königs teilhaben wollten: der Hofadel, die Offiziere, die Staatsbeamten, die ausländischen Gesandten und die Künstler. Der König zwang sich und seinem Hof eine komplizierte Etikette auf, die zum Massstab für das Ansehen jedes Einzelnen wurde. \par

Hatte der Adel früher die Macht des Königs bedroht, so wetteiferte er nun um seine Gunst, denn Aufstieg und Ansehen gab es nur noch im Dienst des Königs. Adlige konnten Offiziere im Heer werden, oder sie lebten in 
    Versailles, wo sie um Ehrenämter und Geldzuwendungen buhlten. Der vom König auferlegte Zwang zur Verschwendung trieb viele in den Ruin, denn es gehörte zur höfischen Sitte, kostspielige Spitzen, Perücken und Seidengewänder zu tragen, teure Essen zu geben und viel Puder und Parfüm zu verwenden. Eine wichtige Rolle spielten die Hofdamen, allen voran die Geliebten des Königs (sog. Mätressen). Sie nahmen Einfluss auf die Vergabe von Stellungen und Ehrungen. \par

Alles in allem standen rund 20'000 Menschen im Dienst des Königs.

\subsubsection{Die Beamten}

Die Verwaltung des Staates wurde in erster Linie von Beamten durchgeführt, die aus dem wohlhabenden Bürgertum kamen. Sie bauten eine Zentralregierung auf, die in verschiedene Bereiche des öffentlichen Lebens 
    eingriff. Der Verwaltungsapparat war, verglichen mit heutigen Staaten, recht klein. Gestützt auf drei Minister, etwa 30 Räte, knapp 100 Sonderbeauftragte und 30 direkte Vertreter des Königs in den Provinzen (Intendanten) und deren Beamte und Schreiber -- insgesamt nicht  viel mehr als 1'000 Personen -- regierte der König den Staat weitgehend selbstständig. \par

Die geringe Zahl der Beamten zeigt allerdings, dass im 17. Jahrhundert viele Angelegenheiten noch nicht staatlich geregelt waren. Ausserhalb des \glqq Hof- und Beamtenstaates\grqq{} besass der König relativ wenig 
    Einfluss. In diesem Sinne wich die Realität vom königlichen Anspruch und Selbstbild beträchtlich ab.

\subsubsection{Das Heer}

In der zweiten Hälfte des 17. Jahrhunderts baute Frankreich eine Armee auf, die grösser war als die jedes anderen Landes in Europa. Von 1664 bis 1703 stieg die Zahl der Soldaten von 45'000 auf \textbf{400'000 Mann}. 
    Auch Söldner aus verschiedenen Kantonen der Eidgenossenschaft dienten dem französischen Monarchen. Dieses gewaltige Heer war die wichtigste Grundlage der Herrschaft Ludwigs XIV., sowohl im eigenen Land als auch in Europa. In Frankreich sollte es oppositionelle Adlige, aber auch Bauern und Bürger von Auflehnungen gegen seine Herrschaft abhalten. Nach aussen wollte er mit Hilfe dieser grossen Armee seine Machtansprüche gegenüber den anderen europäischen Staaten durchsetzen. \par

Der Dreissigjährige Krieg hatte gezeigt, dass mühsam zusammengetrommelte, von Militärunternehmen gekaufte Landsknechtsheere unzuverlässig und kostspielig waren. Mit einem \textbf{stehenden Heer}, das ständig unter 
    Waffen stand und immer verfügbar war, konnte der König die Macht nach innen und aussen besser behaupten. Dies hatte jedoch seinen Preis: In Friedenszeiten verschlang das Heer ein Drittel, in Kriegszeiten zwei Drittel der gesamten Staatsausgaben. Ein wichtiger Grund für den Aufbau eines Zentralstaates bestand deshalb darin, immer wieder Geld für die Armee zu beschaffen.

\subsubsection{Ständegesellschaft}

Die Gesellschaft war in \textbf{drei Stände} gegliedert: \textbf{Adel}, \textbf{Klerus} und \textbf{Dritter Stand}, zu dem die reichen Bürger, aber auch Bauern und Tagelöhner gehörten. Der Adel und hohe Klerus 
    bildeten die führende Schicht -- die Aristokratie -- die etwa 2\% der Bevölkerung ausmachte. Sie genossen Vorrechte, mussten keine Steuern zahlen und lebten von den Abgaben ihrer abhängigen Bauern. \par

Innerhalb des Stadtbürgertums war seit dem 16. Jahrhundert eine neue Führungsschicht entstanden. Auf dieses \textbf{reiche Bürgertum} musste sich der König stützen, wenn er genügend Geld für seinen Hofstaat und die 
    Armee zusammenbekommen wollte. Der Staat nahm deshalb immer häufiger Anleihen bei reichen Bürgern auf. Er verpachtete an sie auch das Recht, Steuern einzuziehen. Dafür mussten sie dem König eine festgesetzte Summe im Voraus bezahlen, konnten jedoch dann ein Vielfaches davon aus den Steuerpflichtigen herausholen. Auch Ämter und Titel verkaufte der Staat für viel Geld an die reichen Bürger. Schliesslich gab es etwa 40'000 Amtsinhaber, den \textbf{Amtsadel}, von dessen Geld der König abhängig war. Umgekehrt waren sie auch vom König abhängig, weil er ihnen zu sozialem Ansehen und Einfluss verholfen hatte. \par

Für Teile des reichen Bürgertums hatte sich die Situation verbessert, für die Masse der Bevölkerung jedoch nicht: Sie mussten weiter Abgaben und Steuern zahlen. Für die kostspieligen Kriege wurden die Steuern 
    ständig erhöht oder neue eingeführt. Steuereintreiber zogen durch das ganze Land; wehrten sich die Bauern, schlug die Armee die Aufstände nieder.

\subsubsection{Einschränkung der Religionsfreiheit}

Nach den Konfessionskämpfen im 16. Jahrhundert hatte die katholische Monarchie die Protestanten 1598 unter staatlichem Schutz geduldet (Edikt von Nantes). Dieser Erlass hatte den calvinistischen Protestanten 
    religiöse Toleranz und volle Bürgerrechte gewährt, zugleich aber den \textbf{Katholizismus als Staatsreligion} fixiert. Die gegenreformatorischen Katholiken wollten sich damit schon bald nicht mehr abfinden: Neid über den wirtschaftlichen Erfolg der Calvinisten -- in Frankreich Hugenotten genannt -- aber auch Angst vor Unruhen spielten dabei eine Rolle. V.a. beeinträchtigten sie die Einheit der Religion und den allumfassenden Machtanspruch des Königs. \par

Mit der \textbf{Aufhebung des Edikts von Nantes 1685} entzog der König den Calvinisten das Recht auf Ausübung ihrer Religion. Mehr als 200'000 verliessen daraufhin heimlich das Land, obwohl darauf die Galeerenstrafe 
    stand.

\subsubsection{Grenzen der königlichen Macht}

Der König war nur \textbf{beschränkt unabhängig}. Gerade finanziell war er nicht \glqq absolut\grqq{}: Hier war er von Steuern und Krediten abhängig. Missernten und wirtschaftliche Fehlschläge senkten das 
    Steueraufkommen; ausserdem liehen die reichen Bürger ihr Geld nur gegen hohen Zinsen aus. \par

Aber auch in der Politik standen seinen Machtansprüchen Hindernisse im Weg: Mit seinem \textbf{kleinen Staatsapparat} konnte er nicht alle Gebiete des Reiches kontrollieren. Obwohl die Adligen entmachtet worden 
    waren, so blieben sie als Grundherren auf ihren Ländereien unabhängig. Städte, Ständeversammlungen in den Provinzen und die Kirche besassen ausserdem alte Privilegien, die sie gegen den Herrschaftsanspruch des Königs verteidigten. Der König war auch von seinen Beamten abhängig, auf dessen Informationen er Entscheidungen fällte. Nicht zuletzt kritisierten Kunst und Wissenschaft den absoluten Macht- und Herrschaftsanspruch.

\subsubsection{Der Merkantilismus}

Besonders wichtig für den Absolutismus war, dass die Wirtschaft des Landes möglichst viel Geld in die Staatskasse brachte. \textbf{Jean-Baptiste Colbert} legte dazu die Grundlagen. Er war von Ludwig XIV. mit 
    umfangreichen Vollmachten ausgestattet worden und für Finanzen, Wirtschaft, Verkehr, Marine, Kolonien, Bauten und die Forstverwaltung zuständig. Er setzte sich als Ziel, die Staatsfinanzen in Ordnung zu bringen. Den König dazu zu bringen, auf seine kostspielige Hofhaltung zu verzichten, funktionierte nicht. Deshalb blieb nur die Möglichkeit, die Staatseinnahmen zu steigern. \par
    
Von einer blühenden Wirtschaft konnte der Staat hohe Steuern und Kredite erhalten, weshalb Colbert versucht, die Produktion im Handwerk zu steigern und durch die Errichtung von Manufakturen zu erhöhen. Gleichzeitig 
    wurde der Absatz der im Land hergestellten Produkte durch \textbf{Schutzzölle} geschützt. Nur Rohstoffe, die aus Frankreich und seinen Kolonien kamen und mit der eigenen Handelsflotte ins Land gebracht wurden, sollten verarbeitet werden. Schliesslich sollten staatliche Grossprojekte wie Kanal- und Strassenbauten die Wirtschaft ankurbeln und billigere Transporte der Waren ermöglichen. \par

Mit dieser \textbf{planmässigen staatlichen Wirtschaftspolitik, dem Merkantilismus}, gelang es Colbert, dem König immer wieder zu Geld zu verschaffen. Sein eigentliches Ziel, die Staatsschulden einzudämmen, 
    erreichte er aber nicht.

\subsubsection{Förderung von Kultur und Wissenschaft}

Nicht nur die Wirtschaft, auch Künste und Wissenschaft sollten dem Staat Nutzen und Ansehen bringen. Der König bezahlte Künstler und zog viele Schriftsteller an den Hof. Sie sollten die Herrschaft des Königs preisen 
    und die neue Staatsform verherrlichen. Wie die \textbf{Kunst in den Dienst der Politik} gestellt wurde, so geschah dies noch stärker mit der Sprache. \par

Französisch sollte das Latein ablösen und Weltgeltung über alle anderen Sprachen erlangen. Dazu musste die Sprache am Anspruch der Gebildeten in Frankreich ausgerichtet werden, weswegen 1634 die \glqq Académie 
    française\grqq{} gegründet wurde. Zur Vereinheitlichung der Rechtsschreibung wurden Regeln aufgestellt, die sich nicht nach der Aussprache, sondern nach der gelehrten, komplizierten Schreibweise richteten. Schliesslich sollten an Sprache und Schrift \glqq Gebildete von den Unwissenden zu unterscheiden\grqq{} sein. 60 Jahre lang arbeitete die Akademie daran; die Sprache des Hofes und der gebildeten Bürger wurde zur Norm, die bis heute prägend gewirkt hat. \par

Insgesamt wurde Frankreich in dieser Zeit zur führenden Kulturnation in Europa.

\subsection{Die Herausbildung der Konstitutionellen Monarchie in England}
\subsubsection{Die Durchlässigkeit der englischen Gesellschaft}

Eine Konsequenz aus den englischen Revolutionen war, dass die bestimmende Oberschicht in England keine so geschlossene Gruppe war wie der Adel im absolutistischen Frankreich. Besitzende Bürger hatten die gleichen 
    politischen Rechte wie besitzende Adlige. Anders als auf dem Kontinent galt wirtschaftliche Tätigkeit nicht als \glqq unfein\grqq{}. Die strenge Trennung zwischen grundbesitzendem Adel und handeltreibendem Bürgertum gab es nicht. Der vom Erwerbsdenken geprägte \textbf{bürgerliche Geist} drang auch in den Adel ein. \par

Hinzu kam, dass das Steuersystem gerechter war: Alle Grundbesitzer, egal ob adlig oder nicht, mussten Grundsteuer zahlen. Trotz allen Unterschieden bestand ein gewisser Zusammenhalt über die Standesgrenzen hinweg, 
    weil die Söhne von Adligen dem Gesetz nach Bürgerliche waren. Nur der älteste Sohn eines Adligen erbte den Titel des Vaters; die übrigen Kinder mussten sich mit Tüchtigkeit und Glück ihren eigenen Platz in der Gesellschaft suchen. Es gab auch mehr Heiraten zwischen den Schichten und mehr gesellschaftlicher Auf- und Abstieg.

\subsubsection{Die Bedeutung der Kolonien}

Seit Mitte des 17. Jahrhunderts war das Hauptinteresse Englands zunehmend auf die Beherrschung der Meere, d.h. auf Kolonien und \textbf{Fernhandel} gerichtet. So wurden Handels- und Kriegsflotte im 18. Jahrhundert 
    ständig ausgebaut. Dank dieser Flotte und einer Handelspolitik, die durch Schutzgesetze englischen Schiffen und Bürgern Sonderrechte sicherte, setzten sich die Engländer auf den Weltmeeren gegen ihre Konkurrenten durch. Spanien und Portugal wurden ebenso zurückgedrängt wie die Niederlande und Frankreich. \par

Nach dem Siebenjährigen Krieg (1756 - 1763) stieg England zur führenden Macht in der Welt auf. Entsprechend der \textbf{Weltmachtstellung} entwickelte sich der Aussenhandel. Wie andere Nationen bereicherten sie sich 
    am \textbf{Sklavenhandel}, britische Händler machten hierbei die grössten Gewinne. Die englischen Kaufleute benutzten die Kolonien nicht nur als Lieferanten von Edelmetallen und Rohstoffen, sondern erschlossen sie auch als \textbf{Absatzmärkte für englische Waren}.

\subsection{Die Politik der europäischen Grossmächte im 17. und 18. Jahrhundert}
\subsubsection{Das Streben nach Vorherrschaft}

Nach dem Dreissigjährigen Krieg hofften viele auf Frieden, doch das Gegenteil geschah: Das Jahrhundert nach dem Krieg ist durch eine fast ununterbrochene Abfolge von Kriegen gekennzeichnet. Die europäischen Regenten 
    im 17. und 18. Jahrhundert taten alles daran, die Macht ihres Staates zu vergrössern. Diesem Ziel dienten im Inneren der Aufbau einer leistungsfähigen Verwaltung und die Förderung von Handel und Gewerbe. Das rascheste und wirksamste Mittel, die Macht des eigenen Staates zu vergrössern, war jedoch, sich fremde Gebiete einzuverleiben -- sei es durch Verträge, etwa beim Heiraten oder Erbschaften, oder durch Krieg. \par

Das Streben nach Gebietserweiterung und nach \textbf{Hegemonie} war nicht neu. Neu war, dass die Herrscher ihr Streben rücksichtslos durchzusetzen versuchten. Sie betrachteten es als ihre wichtigste Aufgabe, ihr 
    Herrschafts- und Einflussgebiet auszuweiten. Mit diesem Ziel vor Augen wandten sie zwei Mittel in der Aussenpolitik an: \textbf{Diplomatie und Krieg}. Frieden hatte für die damaligen Herrscher keinen hohen Wert. \par

Nicht um des Friedens willen, sondern um Vorteile gegenüber den Konkurrenten zu erlangen, schlossen sie Bündnisse und brachen sie wieder, wenn es der eigene Vorteil ratsam war. Ludwig XIV. hatte diese Politik in der 
    zweiten Hälfte des 17. Jahrhunderts erfolgreich betrieben, einerseits weil Spanien mit dem Verlust seiner Flotte 1588 die Seeherrschaft verloren hatte und auch auf dem Kontinent Macht verlor, andererseits weil die französische Diplomatie die europäischen Mächte geschickt gegeneinander ausspielen konnte, weil diese auch nur ihren eigenen Vorteil sahen. Erst am Anfang des 18. Jahrhunderts gelang es den anderen Mächten, Frankreichs Streben nach Vorherrschaft gemeinsam zu stoppen.