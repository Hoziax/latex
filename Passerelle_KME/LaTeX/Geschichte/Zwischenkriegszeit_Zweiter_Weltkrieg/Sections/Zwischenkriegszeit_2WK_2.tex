\clearpage % flushes out all floats
\newpage
\section{Die grosse Depression und ihre Überwindung}
\subsection{Börsenkrach}

Im Herbst 1929 kam es in New York zu einem schweren Börsenkrach. Innerhalb einer Woche fielen die Aktienkurse um fast die Hälfte. Begonnen hatte der Kurssturz am 24. Oktober 1929 (\glqq\textbf{Black Thursday}\grqq{}).

\subsubsection{Ursachen des \glqq schwarzen Donnerstags\grqq{}}

Die Ursachen des \glqq schwarzen Donnerstags\grqq{} waren die Folgenden:
\begin{enumerate}
    \item Seit Mitte der 1920er-Jahre waren die Kurse an der amerikanischen Börse unaufhaltsam gestiegen, was \textbf{Börsenspekulationen} zur Folge hatte. 1928/29 wurden fast unbegrenzt Aktien herausgegeben, was
          den Kapitalmarkt aufblähte.
    \item Zu den wichtigsten langfristigen Ursachen gehörte die Überproduktion in der Landwirtschaft. Die sich in Europa erholende Landwirtschaft führte in den USA zu sinkenden Preisen und schrumpfenden Einkommen
          der Farmer. Der Einkommensverlust führte dazu, dass viele Bauern ihre Hypothekarzinsen nicht mehr zahlen konnten, was wiederum viele kleinere und mittlere Banken belastete. In der Industrie und
          Bauwirtschaft trat bis 1929 eine Sättigung des Marktes ein, was ebenfalls zu sinkenden Preisen führte. Zurückzuführen ist diese Sättigung auf den extrem ungleich verteilten Einkommenszuwachs während der
          1920er-Jahre: Das reichste Prozent der Amerikaner hatte sein Einkommen um 75 Prozent erhöht, während der Durchschnitt eine Steigerung von 9 Prozent erreichte.
    \item Einen strukturellen Schwachpunkt der amerikanischen Wirtschaft stellte das \textbf{Finanz- und Bankenwesen} dar, das \textbf{keinen wirksamen Kontrollen} unterworfen war. Das Bankensystem war zudem durch
          den starken Anstieg der Kredite stark belastet. Als die Spekulationsblase platzte, geriet das Finanzsystem in Notlage. Anstatt die Geldmenge zu erhöhen, um für Liquidität zu sorgen, wurden
          \textbf{Kreditrestriktionen} eingeführt, was die Panik noch verstärkte. Die wirtschaftlichen Schwierigkeiten der USA wirkten sich durch den Stopp des Kreditflusses ins Ausland und die Stockung im
          \textbf{Kriegsschulden- und Reparationskreislauf} umgehend auf die Welt aus, was wiederum auf Amerika zurückschlug.
\end{enumerate}

\subsubsection{Die Folgen des \glqq schwarzen Donnerstags\grqq{}}

Die Folgen dieses grössten Börsenkrachs in der Geschichte waren katastrophal:
\begin{itemize}
    \item Fast 10'000 Banken brachen zusammen.
    \item Die Industrieproduktion schrumpfte um 10 Prozent.
    \item Wer arbeitslos wurde, bekam keine Arbeitslosenunterstützung, da es keine Arbeitslosenversicherung gab.
    \item Die Nachfrage sank weiter, die Preise und Löhne fielen. Wer noch Arbeit hatte, verdiente gerade genug, um das Lebensnotwendigste kaufen zu können.
    \item Am schlimmsten traf es die Farmer im Mittleren Westen der USA. Neben den sinkenden Preisen hatte sie zwischen 1930 und 1941 mit anhaltender Dürre, Bodenerosion und Sandstürmen zu kämpfen. Die Zerstörung
          der ökologischen Gleichgewichts durch den maschinellen Getreideanbau führte zur \glqq\textbf{Dust-Bowl}\grqq{}-Katastrophe. Hunderttausende mussten ihre Farmen verlassen.
\end{itemize}
    Vier Jahre nach dem Börsenkrach hatten die USA knapp 13 Millionen Arbeitslose, fast ein Viertel der erwerbsfähigen Bevölkerung.

\subsection{Der \glqq New Deal\grqq{}: Ausweg aus der Krise?}

Der während der Krise amtierende Präsident Herbert Clark Hoover hielt es als Verfechter der liberalen Wirtschaftstheorie nicht für die Aufgabe des Staates, unterstützend in die Wirtschaft einzugreifen. Er
    vertraute zur Überwindung der Krise auf die Korrekturkräfte des Marktes. Im Wahlkampfjahr 1932 sprach sich Franklin Delano Roosevelt dafür aus, dass der Staat \glqq eine ständige Verantwortung für die
    allgemeine öffentliche Wohlfahrt\grqq{} übernehmen müsse. Er versprach in seinem Programm des \glqq\textbf{New Deal}\grqq{} von einer \glqq Neuverteilung der Karten\grqq{} in der amerikanischen Gesellschaft.
    Er gewann gegen Hoover haushoch. \par

Der \glqq New Deal\grqq{} veränderte die amerikanische Gesellschaft grundlegend: Erstmals griff der Staat in einem solchen Ausmass in Wirtschaft und Gesellschaft ein, um mit sozialen und wirtschaftspolitischen
    Reformen den Bedürftigen zu helfen. Dem liberalen Wirtschaftssystem wurden jetzt von staatlicher Seite Grenzen gesetzt, und es wurden Schritte zum Aufbau eines \textbf{Sozialstaates} unternommen. \par

Höchste Priorität beanspruchte die Bankenkrise. Um das Vertrauen der Bevölkerung wiederherzustellen, setzte Roosevelt im Kongress eine stärkere \textbf{Kontrolle} des \textbf{Finanzsektors} und der \textbf{Börse}
    durch. Ausserdem wurde eine Versicherung für Spareinlagen gegründet und die Möglichkeit geschaffen, über staatliche Institutionen bedrohte Hypotheken zu refinanzieren. Das Vertrauen der Sparer kehrte rasch
    zurück. Roosevelt versuchte ausserdem, durch eine Abwertung des Dollars, durch Schutzzölle und andere protektionistische Massnahmen die Wirtschaftskrise zu überwinden. \par

Um längerfristig die Situation der Farmer zu verbessern, mussten die Preise für ihre Produkte steigen. Erreicht wurde dies durch \textbf{Stilllegung} von Farmland (Angebotsverminderung) und
    \textbf{Vernichtungsaktionen} von Agrargütern (ebenfalls Angebotsverminderung) sowie durch Subventionspolitik auf bestimmte Agrargüter. Der experimentelle Charakter des \glqq New Deal\grqq{} kam bei der
    \textbf{Arbeitslosenhilfe} und den \textbf{Arbeitsbeschaffungsprogrammen} zum Ausdruck: Jugendliche Arbeitslose erhielten die Möglichkeit, bei staatlichen Projekten eine Arbeit zu finden oder sogar einen
    Beruf zu erlernen. Der Staat finanzierte den Bau von Infrastrukturprojekten. \par

Verbunden mit diesen Massnahmen zur Behebung der Not waren Reformgesetze, die die sozial Schwachen schützen und ihnen mehr Rechte geben sollten. So wurde der \textbf{Wettbewerb} auf dem Arbeitsmarkt eingeschränkt,
    erste Ansätze einer \textbf{staatlichen Arbeitslosenversicherung} geschaffen. Die Position der Gewerkschaften wurde gegenüber den Unternehmern gestärkt. Ferner wurden Höchstarbeitszeiten und Mindestlöhne
    festgelegt, die Kinderarbeit und 16 Jahren wurde verboten. \par

Zur Umsetzung seiner \glqq New Deal\grqq{}-Politik gründete Roosevelt viele \textbf{unabhängige Bundesbehörden}. Die bundesstaatliche \textbf{Bürokratie} erreichte ein nie gekanntes Ausmass; es fand eine
    Machtverschiebung vom Kongress zum Präsidentenamt und von den Einzelstaaten hin zur Zentralregierung statt. \par

Roosevelts Politik fand grosse Zustimmung bei der Masse der amerikanischen Bevölkerung, zumal er es verstand, seine Politik in der Öffentlichkeit erfolgreich darzustellen. Widerstand gegen den \glqq New Deal\grqq{}
    kam jedoch von der \textbf{Grossindustrie}, die ihre Schwierigkeiten den staatlichen Eingriffen anlastete und v.a. die Stärkung der Gewerkschaften und die zunehmende Zahl der Streiks nicht hinnehmen wollte. Vor
    dem Obersten Bundesgericht klagten sie und bekamen Recht: Das Gericht erklärte wichtige \textbf{\glqq New-Deal\grqq{}-Gesetze} für verfassungswidrig, weil sie nur einzelne soziale Gruppen begünstigen würden.
    Roosevelt behielt jedoch seinen Kurs bei. Er hatte mit seinem \glqq New-Deal\grqq{} ein wesentliches politisches Ziel erreicht: Viele AmerikanerInnen fassten neues Vertrauen in den demokratischen Staat, der
    sich für die Wohlfahrt seiner BürgerInnen verantwortlich fühlte. Nicht zuletzt deshalb hatten radikalere Lösungsversuche zur Überwindung der Wirtschaftskrise in den USA keine Chance.
    \newpage

\subsection{Amerikanische Aussenpolitik in den 1930er-Jahren: Die Überwindung der Neutralität}

Hitlers aggressive Aussenpolitik nach 1933 verstärkte zunächst die isolationistische Haltung der USA. Neutralitätsgesetze wurden erlassen und angewandt. Erst als Japan 1937 in Restchina einmarschierte, änderten
    die USA ihre Haltung. In der \textbf{Quarantäne-Rede} forderte Roosevelt die Amerikaner zu einer Abkehr der Neutralität auf. Um Frieden, Freiheit und Sicherheit zu sichern, müsse es den friedliebenden Völkern
    gelingen, die internationalen Rechtsbrecher gemeinsam in \glqq Quarantäne\grqq{} zu bringen. Als Deutschland den \glqq Anschluss\grqq{} Österreichs an das Deutsche Reich vollzog, bewilligte der Kongress
    die Aufrüstung. Mit dem Angriff auf Pearl Harbor am 7.12.1941 begann für die USA der Zweite Weltkrieg. \par

Die USA verfügten über gewaltige menschliche und materielle Ressourcen, die sie schnell mobilisieren konnten. Sechzig Prozent der Gesamtkriegskosten (370 Milliarden Dollar) wurden durch Kriegsanleihen finanziert.
    Man konnte auf Kapazitäten zurückgreifen, die wegen der grossen Depression ungenutzt waren. Grosse Forschungsaufträge wurden vergeben, die die Entwicklung der Computertechnologie oder der Atombombe
    vorantrieben.

\subsection{Der Krieg an der Heimatfront}

Die Ersten, die den Zorn über Pearl Harbor zu spüren bekamen, waren die rund 110'000 Amerikaner japanischer Abstammung. Sie wurden umgehend verhaftet und interniert. Die meisten mussten den ganzen Krieg in
    Internierungslagern verbringen und verloren ihren ganzen Besitz. Gegen Deutsch- und Italo-Amerikaner wurde weniger drastisch vorgegangen, aber auch sie wurden vorsorglich vom Geheimdienst überwacht. Gleichzeitig
    fanden mehr als 200'000 Deutsche in den USA Schutz vor dem Nationalsozialismus. Darunter befanden sich viele Intellektuelle, Wissenschaftler und Künstler wie Albert Einstein oder Bertolt Brecht. \par

Die USA verfügte 1941 nicht nur über gewaltige menschliche und materielle Ressourcen, sie konnte diese auch in relativ kurzer Zeit mobilisieren. 60 Prozent der \textbf{Gesamtkriegskosten} von
    ca. \textbf{370 Milliarden Dollar} wurden durch den Verkauf von Kriegsanleihen finanziert. Bei der industriellen Produktion konnte man auf die wegen der grossen Depression ungenutzten Kapazitäten zurückgreifen,
    ohne dass der private Konsum nennenswert reduziert werden musste. Nur die Produktion von Autos wurde zugunsten von Lastwagen und Panzern reduziert.  Die Zusammenarbeit der Regierung mit der Wirtschaft erfolgte
    nun weniger interventionistisch; die Streitkräfte konnten direkt Rüstungsaufträge an private Unternehmen vergeben. \par

Parallel zu den industriellen Anstrengungen wurden auch die wissenschaftlichen Ressourcen ausgeschöpft. Grosse Forschungsaufträge gingen an wichtige Institute, die den Grundstein für Technologien wie die des
    \textbf{Computers} und der \textbf{Atombombe} legten. Ab 1942 erholte sich die Wirtschaft und es kam zu einem selbsttragenden Wirtschaftsaufschwung mit \textbf{Vollbeschäftigung}. Auch die Landwirtschaft konnte
    nach der Aufhebung der Anbaubeschränkungen des \glqq New Deals\grqq{} ihre Produktion steigern, obwohl die Zahl der Beschäftigen in der Landwirtschaft abnahm. \par

Durch die Kriegswirtschaft stiegen die Unternehmensgewinne und die Einkommen der Privathaushalte. Die Zahl der werktätigen Frauen nahm um 60 Prozent zu. Der Krieg trug auch zu einer etwas gerechteren
    Einkommensverteilung bei.