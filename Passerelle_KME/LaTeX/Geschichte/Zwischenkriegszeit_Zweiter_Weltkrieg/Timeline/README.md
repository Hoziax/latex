# Anleitung

1. Laden Sie den Ordner wie folgt herunter:

![alt text](Screenshots/1_Download.png)

2. Extrahieren Sie die Dateien in einen Ordner. Wichtig ist, dass die Dateien *index.html* und *style.css* im selben Ordner sind.

3. Öffnen Sie die Datei *index.html* mit ihrem Browser (Firefox, Chrome, etc).