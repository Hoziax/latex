\clearpage % flushes out all floats
\newpage
\section{Gesellschaftliche und ökologische Folgen der Industrialisierung}
\subsection{Die soziale Frage}

Die Arbeit in den Fabriken war folgendermassen gekennzeichnet:
    \begin{itemize}
        \item Disziplinarische Vorschriften.
        \item Unfallgefahr.
        \item Unhygienische Verhältnisse.
        \item Lange Arbeitszeiten; am Anfang der Industrialisierung betrug sie bis zu 16 Stunden.
        \item Niedrige Löhne und die daraus folgende volle Berufstätigkeit beider Ehepartner.
    \end{itemize}
    Verdienstausfall hatte Not und Verelendung zur Folge, weshalb Alkoholismus unter Männern häufig war. Im Betrunkenen Zustand schlugen viele ihre Familie, verloren ihre Stelle; die Frauen verlangten v.a. im protestantisch-städtischen Umfeld die Scheidung. \par

Die Konzentration der Arbeitsplätze führte zu Grossstädten. Eine deutliche Trennung von bürgerlichen und Arbeiterquartieren entstand. Die \textbf{Arbeiterquartiere} hatten folgende Eigenschaften:
    \begin{itemize}
        \item Sie lagen in der Nähe der Fabriken.
        \item Sie waren nicht besonders gross und bescheiden ausgestattet.
        \item Nicht sehr günstig, weil die Nachfrage gross war. Häufige Überbelegung durch Untermiete waren häufig; Arbeiterfamilien erhofften sich dadurch, ihr Einkommen zu verbessern.
        \item Sie hatten dünne Wände.
        \item Die Abwasser- und Kanalisationssysteme waren ungenügend, Trinkwasser war verschmutzt. Cholera- und Typhusepidemien waren die Folge; Feuchtigkeit und schlechte Luft begünstigten Tuberkulose.
    \end{itemize}
    Diese desolate Lage in den Arbeiterquartieren beunruhigte Politiker, Sozialreformer, Pfarrer und kritische Zeitgenossen; sie wurde als \glqq\textbf{soziale Frage}\grqq{} definiert. Ende des 19. Jahrhunderts wurde viel dazu geschrieben; die Ideen von Marx fanden immer mehr Anklang.

\subsection{Der Marxismus}
\subsubsection{Grundlagen}

Als sich die \glqq soziale Frage\grqq{} verschärfte, war eine Antwort darauf der Marxismus. Er wurde von Karl Marx und Friedrich Engels im \textbf{19. Jahrhundert} entwickelt. Sie wird als \glqq wissenschaftlicher 
    Sozialismus\grqq{} bezeichnet. Sie basiert auf folgendes:
    \begin{itemize}
        \item Der Geschichtstheorie des Historischen Materialismus. Diese geht davon aus, dass die Entstehung einer sozialistischen Gesellschaft aufgrund der wirtschaftlichen und sozialen Entwicklung gesetzmässig 
                  ist.
        \item Der Politischen Ökonomie. Sie kritisiert die kapitalistische Produktionsweise.
    \end{itemize}

\subsubsection{Menschenbild}

Marx und Engels gingen von einem Menschenbild aus, in dem sich der Mensch vom Tier insofern unterscheidet, dass der Mensch sich durch seine Arbeit verwirklichen will. Erst kreative Arbeit, die frei von jedem 
    äusseren Zwang ist und nur der Selbstverwirklichung dient, macht den Menschen wirklich zum Menschen. Die industrialisierte Welt kann das dem Mensch nicht bieten; dies wegen der Arbeitsteilung, und dass das Endprodukt nicht dem Arbeiter, sondern dem Unternehmer gehört \textrightarrow{} es findet eine \textbf{Entfremdung} statt.

\subsubsection{Historischer Materialismus}

Hierbei handelt es sich um den Versuch, die Weltgeschichte als gesetzmässige Entwicklung zu erklären. Marx und Engels berufen sich auf die dialektische Geschichtsphilosophie Hegels, nach der Spannungen zwischen zwei 
    Ideen dadurch überwindet werden, indem sie in ein drittes Stadium übergehen \textrightarrow{} These und Antithese werden zur Synthese. Marx und Engels gehen aber nicht von Spannungen zwischen zwei Ideen aus, sondern von der in sich widersprüchlichen wirtschaftlichen Struktur einer Gesellschaft. \par

Die wirtschaftlichen Grundlagen bezeichnen sie als \glqq\textbf{Unterbau}\grqq{}. Sie besteht aus den Produktionsverhältnissen und den Produktionsmitteln. Der \glqq\textbf{Überbau}\grqq{} ist die mit dem Unterbau 
    verbundene Ideenwelt: Staatsform, Rechtssystem, Moralvorstellung, Kunst, Religion, Philosophie. Sie gehen davon aus, dass die Produktionsmittel im laufe der Zeit sich ändern und so im Gegensatz zu den unveränderten Produktionsverhältnissen stehen. Diese Spannung löst sich dann in einem revolutionären Schritt, indem sich die Produktionsverhältnisse an die Produktionsmittel angepasst werden \textrightarrow{} Überbau und Unterbau wandeln sich. \par

Dieser Kampf tragen die Klassen in \textbf{Klassenkämpfen} aus. Die Urgesellschaft, welche weder private Produktionsmitteln noch Privatbesitz kennt, wandelt sich mit der Zeit in eine Sklavenhaltergesellschaft 
    (Hauptgegensatz: Sklavenhalter -- Sklaven), dann in eine Feudalgesellschaft (Hauptgegensatz: Feudalherren -- Leibeigene) und schliesslich zur Industriegesellschaft bzw. dem Kapitalismus (Hauptgegensatz: Kapitalisten/Bourgeois -- Proletarier). In einem nächsten Schritt kommt es zu einer sozialistischen \textbf{Revolution}, wobei es vorübergehend zu einer Diktatur des Proletariats kommt, bis die Gesellschaft in einen klassenlosen Zustand und dem Kommunismus übergeht.

\subsubsection{Politische Ökonomie}

Von Karl Marx wird der \textbf{Mehrwert} als der eigentliche Antrieb der kapitalistischen Gesellschaft definiert. Darunter wird die Differenz zwischen dem Kaufbetrag der Wahre und dem Lohn, die der Arbeiter für die 
    Fertigung der Ware erhält. Im kapitalistischen System fällt dieser Mehrwert an den Kapitalisten, also dem Eigentümer Produktionsmittel, und nicht an den Arbeiter. Nach diesem Modell geschieht folgendes:
    \begin{itemize}
        \item Die Arbeitskraft spielt mit zunehmender Technisierung und Rationalisierung eine immer geringere Rolle.
        \item Dadurch werden weniger Löhne bezahlt, der Mehrwert sinkt.
        \item Der Unternehmer ist dadurch gezwungen, mehr zu produzieren.
        \item Dies führt zu einer Überproduktion, welche keinen Abnehmer finden und zum Ruin kleiner Unternehmen.
        \item Es führt zu einer \textbf{Konzentration} des Kapitals auf wenige Grosskapitalisten.
    \end{itemize}

Unter dem Strich hat dies zur Folge, dass die Zahl der ausgebeuteten Proletarier steigt; es kommt zu periodisch wiederkehrender Arbeitslosigkeit und damit zur Verelendung der Arbeiterklassen \textrightarrow{} 
    \textbf{Verelendungstheorie}. Diese Proletarier würden sich somit auflehnen und mit einer Revolution die Kapitalisten entmachten.

\subsection{Ökologische Folgen}
\subsubsection{Umweltverschmutzung}

Durch die Industrialisierung traten immer grösser werdende \textbf{Umweltprobleme} auf. In der ersten Hälfte des 19. Jahrhunderts war es v.a. die wandelnde Landwirtschaft, die die Umweltveränderung vorantrieb. Die 
    Flächen wurden intensiver genutzt, Sümpfe wurden trocken gelegt und Flüsse begradigt, um mehr Land zu gewinnen. Parallel dazu entstanden und wuchsen Städte, Verkehrswege begannen, die Landschaft zu durchschneiden. Luftverschmutzung, Verunreinigung von Flüssen durch Abwässer, Probleme bei der Abfallbeseitigung und der zunehmende Autoverkehr besorgten die Menschen. \par

Ab Mitte des 19. Jahrhunderts nahmen die Klagen diesbezüglich stark zu. FabrikanwohnerInnen waren v.a. betroffen, doch sie beklagten sich nicht, da sie ihre Stelle nicht verlieren wollten.

\subsubsection{Rauch und Russ}

Die schwerwiegendsten Umweltprobleme kamen vom Rauch und Russ, welcher aus den Kaminen der Privathäuser und Fabriken stammte. Die verstärkte Nutzung von Kohle und chemische Prozesse, die schädliche Substanzen 
    verursachten, führten zu mehr Klagen und einem Verschwinden der Vegetation. Viele Gegenmassnahmen wurden vorgeschlagen, doch keine griff. In Deutschland wollte man sogar an \glqq industrieresistente\grqq{} Pflanzen forschen.